<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class PreProvider.
 *
 * @package namespace App\Entities;
 */
class PreProvider extends AppEntity implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'token',
        'email',
        'register',
        'quotation_id',
        'expiration',
        'date_register',
    ];

    protected $dates = [
        'created',
        'modified',
        'deleted'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function quotation()
    {
        return $this->belongsTo(Quotation::class,'quotation_id')
                    ->joinWhere(Quotation::class,'quotation_id','=','id','inner');
    }
}
