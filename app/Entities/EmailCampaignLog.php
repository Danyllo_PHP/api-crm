<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class EmailCampaignLogService.
 *
 * @package namespace App\Entities;
 */
class EmailCampaignLog extends AppEntity implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'email_campaign_id',
        'email_email_id',
        'status'
    ];

    protected $dates = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function emailCampaign()
    {
        return $this->belongsTo(EmailCampaignLog::class, 'email_campaign_id')
                    ->joinWhere(EmailCampaignLog::class, 'email_campaign_id','=','id','inner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function emailEmail()
    {
        return $this->belongsTo(EmailEmails::class,'email_email_id')
                    ->joinWhere(EmailEmails::class, 'email_email_id', '=', 'id','inner');
    }
}
