<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class StockLog.
 *
 * @package namespace App\Entities;
 */
class StockLog extends Model implements Transformable
{
    use TransformableTrait;

    protected $connection = 'pgsql';

    protected $table = 'stock_logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'transaction_type',
        'price',
        'miles',
        'validity',
        'stock_id',
        'user_id',
        'payment_type',
        'wintour_sale',
        'refunded',
        'miles_remaining',
        'user_type',
        'emission_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function stock()
    {
        return $this->belongsTo(Stock::class,'stock_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
