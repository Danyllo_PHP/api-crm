<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class AuditDelta.
 *
 * @package namespace App\Entities;
 */
class AuditDelta extends AppEntity implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'audit_id',
        'property_name',
        'old_value',
        'new_value'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function audit()
    {
        return $this->belongsTo(Audit::class,'audit_id')
                    ->joinWhere(Audit::class,'audit_id','=','id','inner');
    }
}
