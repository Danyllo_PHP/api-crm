<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class RemittenceFile.
 *
 * @package namespace App\Entities;
 */
class RemittenceFile extends AppEntity implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'url',
        'remittence_banks_id',
        'created_by',
    ];

    protected $dates = [
        'created',
        'modified',
        'deleted',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function order()
    {
        return $this->belongsToMany(Order::class,'orders_remittence_files','remittence_file_id','order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function remittenceBank()
    {
        return $this->belongsTo(RemittenceBank::class,'remittence_banks_id')
                    ->joinWhere(RemittenceBank::class,'remittence_banks_id','=','id','inner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public  function creator()
    {
        return $this->belongsTo(User::class, 'created_by')
                    ->leftJoin('Users','created_by','=','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function remittenceReturn()
    {
        return $this->hasOne(RemittenceReturn::class,'remittence_file_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ordersRemittencesFile()
    {
        return $this->hasMany(OrdersRemittenceFile::class,'remittence_file_id');
    }
}
