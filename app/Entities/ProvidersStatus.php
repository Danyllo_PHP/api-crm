<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class ProvidersStatus.
 *
 * @package namespace App\Entities;
 */
class ProvidersStatus extends AppEntity implements Transformable
{
    use TransformableTrait;

    protected $table = 'providers_status';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'title',
        'provider_create',
        'has_comment',
        'class',
        'created_by',
        'modified_by',
    ];

    protected $dates = [
        'created',
        'modified',
        'deleted',
    ];

    const EM_ANALISE        = 1;
    const AGUARDANDO        = 2;
    const DADOS_PENDENTES   = 3;
    const APROVADO          = 4;
    const RECUSADO          = 5;
    const NAO_DEFINIDO      = 6;
    const FRAUDE            = 7;
    const POSTECIPADO       = 8;
    const DESISTENTE        = 9;
    const FINALIZADO        = 10;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public  function creator()
    {
        return $this->belongsTo(User::class, 'created_by')
                    ->leftJoin('Users','created_by','=','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function modifier(){
        return $this->belongsTo(User::class, 'modified_by')
                    ->leftJoin('Users','modified_by','=','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function provider()
    {
        return $this->hasMany(Provider::class,'provider_status_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function ordersStatus()
    {
        return $this->belongsToMany(OrdersStatus::class,'orders_status_providers_status1','providers_status_id','orders_status_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function paymentForm()
    {
        return $this->belongsToMany(PaymentForm::class,'providers_status_payment_forms','providers_status_id','payment_form_id');
    }
}
