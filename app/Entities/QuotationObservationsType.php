<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class QuotationObservationsType.
 *
 * @package namespace App\Entities;
 */
class QuotationObservationsType extends AppEntity implements Transformable
{
    use TransformableTrait;

    protected $table = 'quotations_observations_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'title',
    ];

    protected $dates = [
        'created',
        'modified',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function quotation()
    {
        return $this->hasMany(Quotation::class,'observations_type_id');
    }
}
