<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class StockProviderService.
 *
 * @package namespace App\Entities;
 */
class StockProvider extends Model implements Transformable
{
    use TransformableTrait;

    protected $connection = 'pgsql';

    protected $table = 'providers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'cpf',
        'phone',
        'cellphone',
        'company',
        'occupation',
        'company_phone',
        'observations',
        'wintour',
        'provider_in_use',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function stock()
    {
        return $this->hasMany(Stock::class, 'provider_id');
    }

}
