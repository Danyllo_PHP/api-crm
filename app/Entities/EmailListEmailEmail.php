<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class EmailListEmailEmail.
 *
 * @package namespace App\Entities;
 */
class EmailListEmailEmail extends AppEntity implements Transformable
{
    use TransformableTrait;

    protected $table = 'email_lists_email_emails';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'email_list_id',
        'email_email_id',
    ];

    protected $dates = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function emailList()
    {
        return $this->belongsTo(EmailList::class,'email_list_id')
                    ->joinWhere(EmailList::class,'email_list_id','=','id','inner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function emailEmail()
    {
        return $this->belongsTo(EmailEmails::class, 'email_email_id')
                    ->joinWhere(EmailEmails::class,'email_email_id','=','id','inner');
    }
}
