<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPasswordNotification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
/**
 * Class User.
 *
 * @package namespace App\Entities;
 */
class User extends Authenticatable implements Transformable
{
    use TransformableTrait, Notifiable, HasApiTokens;

    protected $table = 'users';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'username',
        'password',
        'created_by',
        'modified_by',
        'group_id',
        'branch',
        'photo',
        'photo_dir',
        'active',
        'activity_count',
        'comment_count',
        'quotation_count',
        'task_count',
        'group_manager',
        'last_access',
        'miles_goals',
        'email_verified_at',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = [
        'created',
        'modified',
        'deleted',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function actionsUser()
    {
        return $this->hasMany(ActionsUser::class, 'action_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function audit()
    {
        return $this->hasMany(Audit::class, 'source_id')
                ->where('source_model', '=', 'Users');
    }

    public function groups(){
        return $this->belongsTo(Group::class, 'group_id');
    }

    public  function creator(){
        return $this->belongsTo(User::class, 'created_by')->leftJoin('Users','created_by','=','id');
    }

    public function modifier(){
        return $this->belongsTo(User::class, 'modified_by')->leftJoin('Users','modified_by','=','id');
    }

    public function quotations(){
        return $this->hasMany(Quotation::class,'user_id');
    }

    public function tasks(){
        return $this->hasMany(Task::class,'user_id');
    }

    public function providers(){
        return $this->hasMany(Provider::class,'user_id');
    }

    public function emailTemplates(){
        return $this->hasMany(EmailTemplate::class,'user_id');
    }

    public function entitiesReleases(){
        return $this->hasMany(EntitiesRelease::class,'user_id');
    }

    public function orders(){
        return $this->hasMany(Order::class,'user_id');
    }

    public function phoneCallsLogs(){
        return $this->hasMany(PhoneCallsLog::class,'user_id');
    }

    public function sessions(){
        return $this->hasMany(Session::class,'user_id');
    }

//      'saveStrategy' => 'replace'
//    public function actions(){
//        return $this->belongsToMany()
//    }

    public function messages(){
        return $this->belongsToMany(Message::class,'messages_users','user_id','message_id');
    }

    /**
     * @param string $token
     * @return array
     */
    public function sendPasswordResetNotification($token)
    {
        $reset = $this->notify(new ResetPasswordNotification($token));
        DB::table('password_resets')->where('email', $this->email)->update(['token' => $token]);
        if ($reset) {
            return [
                'erro' => false,
                'message' => 'Email enviado!'
            ];
        }
        return [
            'erro' => true,
            'message' => 'Usuário bloqueado!'
        ];
    }
}
