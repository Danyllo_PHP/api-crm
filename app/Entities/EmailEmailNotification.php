<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class EmailEmailNotification.
 *
 * @package namespace App\Entities;
 */
class EmailEmailNotification extends AppEntity implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'email',
        'event',
        'source',
    ];

    protected $dates = [
        'created',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function quotation()//Qual a tabela do belongsToMany
    {
        return $this->belongsToMany(Quotation::class,null, 'email','email');
    }
}
