<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class MailBoxAttachment.
 *
 * @package namespace App\Entities;
 */
class MailBoxAttachment extends AppEntity implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'mail_box_id',
        'name',
        'file_path',
        'attachments_origin',
        'path_to_file',
    ];

    protected $dates = [
        'created',
        'modified',
        'deleted',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mailBox()
    {
        return $this->belongsTo(MailBox::class, 'mail_box_id');
    }
}
