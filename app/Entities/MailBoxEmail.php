<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class MailBoxEmail.
 *
 * @package namespace App\Entities;
 */
class MailBoxEmail extends AppEntity implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'description',
        'email',
        'mail_in_use',
        'name',
        'address',
    ];

    protected $dates = [
        'created',
        'modified',
        'deleted',
    ];

    protected $hidden = [
        'password',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mailBox()
    {
        return $this->hasMany(MailBox::class,'mail_box_email_id');
    }
}
