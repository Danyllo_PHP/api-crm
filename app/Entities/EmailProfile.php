<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class EmailProfile.
 *
 * @package namespace App\Entities;
 */
class EmailProfile extends AppEntity implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'email_transport_id',
        'name',
        'from',

    ];

    protected $dates = [
        'created',
        'modified',
        'deleted',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function emailTransport()
    {
        return $this->belongsTo(EmailTransport::class, 'email_transport_id')
                    ->joinWhere(EmailTransport::class,'email_transport_id','=','id','inner');
    }
}
