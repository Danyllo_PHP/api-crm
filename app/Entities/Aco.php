<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Aco.
 *
 * @package namespace App\Entities;
 */
class Aco extends AppEntity implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'parent_id',
        'model',
        'foreign_key',
        'alias',
        'lft',
        'rght',
        '_show'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function aro()
    {
        return $this->belongsToMany(Aro::class,'aros_acos','aco_id','aros_id');
    }
}
