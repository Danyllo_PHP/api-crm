<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class RemittenceReturnOrder.
 *
 * @package namespace App\Entities;
 */
class RemittenceReturnOrder extends AppEntity implements Transformable
{
    use TransformableTrait;

    protected $table = 'remittence_returns_orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'order_id',
        'remittence_return_id',
        'order_status_id',
        'provider_name',
        'provider_cpf',
        'order_price',
        'return_order_id',
        'return_codes',
        'protocol',
        'auth',
        'commitment',
        'payment_date',
        'payment_due_date',
        'created_by',
    ];

    protected $dates = [
        'created',
        'modified',
        'deleted',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public  function creator()
    {
        return $this->belongsTo(User::class, 'created_by')
                    ->leftJoin('Users','created_by','=','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function provider()
    {
        return $this->belongsTo(Provider::class,'provider_cpf','cpf');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function remittenceReturn()
    {
        return $this->belongsTo(RemittenceReturn::class,'remittence_return_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class,'order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ordersStatus()
    {
        return $this->belongsTo(OrdersStatus::class,'order_status_id');
    }
}
