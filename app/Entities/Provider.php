<?php

namespace App\Entities;

use function foo\func;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Provider.
 *
 * @package namespace App\Entities;
 */
class Provider extends AppEntity implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'provider_status_id',
        'user_id',
        'parent_id',
        'provider_occupation_id',
        'name',
        'email',
        'document',
        'cpf',
        'score_id',
        'score_value',
        'score_status',
        'score_updated',
        'phone',
        'cellphone',
        'company',
        'occupation',
        'company_email',
        'company_phone',
        'company_phone_branch',
        'comment',
        'payment_count',
        'quotation_count',
        'provider_file_count',
        'rg',
        'birthday',
        'gender',
        'street_view',
        'facebook',
        'linkedin',
        'restriction',
        'company_site',
        'orders_count',
        'order_status_id',
        'payment_form_id',
        'cnpj',
        'company_extra',
        'status_modified',
        'session_id',
        'was_consulted',
        'was_consulted_spc',
        'last_quotation_id',
        'created_by',
        'modified_by',
    ];

    protected $dates = [
        'created',
        'modified',
        'deleted',
    ];

    protected $hidden = [
        'password',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public  function creator()
    {
        return $this->belongsTo(User::class, 'created_by')
                    ->leftJoin('Users','created_by','=','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function modifier()
    {
        return $this->belongsTo(User::class, 'modified_by')
                    ->leftJoin('Users','modified_by','=','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parentProvider()
    {
        return $this->belongsTo(Provider::class,'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function providersOccupation()
    {
        return $this->belongsTo(ProvidersOccupation::class,'provider_occupation_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function ordersStatus()
    {
        return $this->belongsTo(OrdersStatus::class,'order_status_id')
                    ->joinWhere(OrdersStatus::class,'order_status_id','=','id','inner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lastQuotation()
    {
        return $this->belongsTo(Quotation::class,'last_quotation_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function providersStatus()
    {
        return $this->belongsTo(ProvidersStatus::class,'provider_status_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function paymentForm()
    {
        return $this->belongsTo(PaymentForm::class,'payment_form_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function session()
    {
        return $this->belongsTo(Session::class,'session_id')
                    ->joinWhere(Session::class,'session_id','=','id','inner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function banksProvidersSegments()
    {
        return $this->hasOne(BanksProvidersSegment::class,'provider_id')
                    ->where('main','=','1');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function campaignEmail()
    {
        return $this->hasOne(EmailEmails::class,'email');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function quotation()
    {
        return $this->hasMany(Quotation::class,'provider_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function note()
    {
        return $this->hasMany(Note::class,'provider_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function providersFile()
    {
        return $this->hasMany(ProvidersFile::class,'provider_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function address()
    {
        return $this->hasMany(Address::class,'parent_id')
                    ->where('model','=','Providers');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function email()
    {
        return $this->hasMany(Email::class,'provider_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fidelity(){
        return $this->hasMany(Fidelity::class,'provider_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mailBox()
    {
        return $this->hasMany(MailBox::class,'provider_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function order()
    {
        return $this->hasMany(Order::class,'provider_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ordersProgram()
    {
        return $this->hasMany(OrdersProgram::class,'provider_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payment()
    {
        return $this->hasMany(Payment::class,'provider_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function phone()
    {
        return $this->hasMany(Phone::class,'provider_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function  pendingEdition()//implementar condição 'PendingEditions.model' => 'Providers'
    {
        return $this->hasMany(PendingEdition::class,'primary_key')
                ->where('model', '=', 'Providers');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childProvider()
    {
        return $this->hasMany(Provider::class,'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sms()
    {
        return $this->hasMany(Sms::class,'provider_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function stock()
    {
        return $this->hasMany(Stock::class,'provider_id');
    }
}
