<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Payment.
 *
 * @package namespace App\Entities;
 */
class Payment extends AppEntity implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'payment_status_id',
        'provider_id',
        'quotation_id',
        'program_id',
        'bank_provider_id',
        'bank_id',
        'agency',
        'type',
        'account',
        'operation',
        'date',
        'price',
        'value',
        'comment',
        'status_modified',
        'order_id',
        'invoice_id',
        'stocked',
        'archived',
        'receipt_sent',
        'info',
        'created_by',
        'modified_by',
    ];

    protected $dates = [
        'created',
        'modified',
        'deleted',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function paymentsStatus()
    {
        return $this->belongsTo(PaymentsStatus::class,'payment_status_id')
                    ->joinWhere(PaymentsStatus::class,'payment_status_id','=','id','inner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function quotation(){
        return $this->belongsTo(Quotation::class,'quotation_id')
                    ->leftJoin(Quotation::class,'quotation_id','=','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function program(){
        return $this->belongsTo(Program::class,'program_id')
                    ->leftJoin(Program::class,'program_id','=','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function bank(){
        return $this->belongsTo(Bank::class,'bank_id')
                    ->joinWhere(Bank::class,'bank_id','=','id','inner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function provider(){
        return $this->belongsTo(Provider::class,'provider_id')
                    ->joinWhere(Provider::class,'provider_id','=','id','inner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function order(){
        return $this->belongsTo(Order::class,'order_id')
                    ->leftJoin(Order::class, 'order_id', '=','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function invoice(){
        return $this->belongsTo(Invoice::class,'invoice_id')
                    ->leftJoin(Invoice::class,'invoice_id','=','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public  function creator(){
        return $this->belongsTo(User::class, 'created_by')
                    ->leftJoin('Users','created_by','=','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function modifier(){
        return $this->belongsTo(User::class, 'modified_by')
                    ->leftJoin('Users','modified_by','=','id');
    }
}
