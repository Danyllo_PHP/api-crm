<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class EmailCampaign.
 *
 * @package namespace App\Entities;
 */
class EmailCampaign extends AppEntity implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'template_id',
        'email_transport_id',
        'email_profile_id',
        'sender',
        'paused',
        'ready',
        'started',
        'created_by',
        'modified_by',
    ];

    protected $dates = [
        'created',
        'modified',
        'deleted',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function emailTemplate()
    {
        return $this->belongsTo(EmailTemplate::class,'template_id')
                    ->joinWhere(EmailTemplate::class,'template_id','=', 'id', 'inner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function emailTransport()
    {
        return $this->belongsTo(EmailTransport::class,'email_transport_id')
                    ->joinWhere(EmailTransport::class, 'email_transport_id', '=','id', 'inner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function emailProfile()
    {
        return $this->belongsTo(EmailProfile::class,'email_profile_id')
                    ->joinWhere(EmailProfile::class, 'email_profile_id', '=', 'id','inner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public  function creator()
    {
        return $this->belongsTo(User::class, 'created_by')
                    ->leftJoin('Users','created_by','=','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function emailCampaignLog()
    {
        return $this->hasMany(EmailCampaignLog::class,'email_campaign_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function emailList()
    {
        return $this->belongsToMany(EmailList::class, 'email_campaigns_email_lists','email_campaign_id','email_list_id');
    }
}
