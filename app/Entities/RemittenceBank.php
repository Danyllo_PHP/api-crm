<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class RemittenceBank.
 *
 * @package namespace App\Entities;
 */
class RemittenceBank extends AppEntity implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'classname',
        'code',
        'agreement',
        'agency',
        'digit',
        'account',
        'created_by',
        'modified_by',
    ];

    protected $dates = [
        'created',
        'modified',
        'deleted',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public  function creator()
    {
        return $this->belongsTo(User::class, 'created_by')
                    ->leftJoin('Users','created_by','=','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function modifier()
    {
        return $this->belongsTo(User::class, 'modified_by')
                    ->leftJoin('Users','modified_by','=','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function remittenceConfig()
    {
        return $this->hasMany(RemittenceConfig::class,'remittence_banks_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function remittenceFile()
    {
        return $this->hasMany(RemittenceFile::class,'remittence_banks_id');
    }
}
