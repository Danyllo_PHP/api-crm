<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class InvoiceStatus.
 *
 * @package namespace App\Entities;
 */
class InvoiceStatus extends AppEntity implements Transformable
{
    use TransformableTrait;

    protected $table = 'invoices_status';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'title'
    ];

    protected $dates = [

    ];


}
