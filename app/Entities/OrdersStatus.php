<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class OrdersStatus.
 *
 * @package namespace App\Entities;
 */
class OrdersStatus extends AppEntity implements Transformable
{
    use TransformableTrait;

    protected $table = 'orders_status';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'title',
        'order_create',
        'parent_id',
        'created_by',
        'modified_by',
    ];

    protected $dates = [
        'created',
        'modified',
        'deleted',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ordersStatusProvidersStatus()
    {
        return $this->hasMany(OrdersStatusProvidersStatus1::class,'order_status_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subOrdersStatus()
    {
        return $this->hasMany(OrdersStatus::class,'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parentOrdersStatus()
    {
        return $this->belongsTo(OrdersStatus::class,'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public  function creator()
    {
        return $this->belongsTo(User::class, 'created_by')
                    ->leftJoin('Users','created_by','=','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function modifier()
    {
        return $this->belongsTo(User::class, 'modified_by')
                    ->leftJoin('Users','modified_by','=','id');
    }
}
