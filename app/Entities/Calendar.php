<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Calendar.
 *
 * @package namespace App\Entities;
 */
class Calendar extends AppEntity implements Transformable
{
    use TransformableTrait;

    protected $table = 'calendar';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'created_by',
        'modified_by',

    ];

    protected $dates = [
        'created',
        'modified',
        'deleted',
        'date',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public  function creator()
    {
        return $this->belongsTo(User::class, 'created_by')
                    ->leftJoin('Users','created_by','=','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function modifier()
    {
        return $this->belongsTo(User::class, 'modified_by')
                    ->leftJoin('Users','modified_by','=','id');
    }

}
