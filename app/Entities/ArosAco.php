<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class ArosAco.
 *
 * @package namespace App\Entities;
 */
class ArosAco extends AppEntity implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'aro_id',
        'aco_id',
        '_create',
        '_read',
        '_update',
        '_delete'
    ];
}
