<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class PendingEdition.
 *
 * @package namespace App\Entities;
 */
class PendingEdition extends AppEntity implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'model',
        'primary_key',
        'field',
        'value',
    ];

    protected $dates = [
        'created',
        'modified',
        'deleted',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function occupation()//Implementaçãoda condição de field == provider_occupation_id
    {
        return $this->belongsTo(ProvidersOccupation::class,'value')
                    ->leftJoin(ProvidersOccupation::class,'value','=','id')
                    ->whereHas(PendingEdition::class, function ($query){
                        $query->where('field', '=', 'provider_occupation_id');
                    });
    }
}
