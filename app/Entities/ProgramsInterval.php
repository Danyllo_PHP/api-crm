<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class ProgramsInterval.
 *
 * @package namespace App\Entities;
 */
class ProgramsInterval extends AppEntity implements Transformable
{
    use TransformableTrait;

    protected $table = 'programs_interval';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'parent_id',
        'program_id',
        'payment_form_id',
        'display_start',
        'display_end',
        'display_interval',
        'price',
        'expire',
        'created_by',
        'modified_by',
    ];

    protected $dates = [
        'created',
        'modified',
        'deleted',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public  function parentProgramsInterval()
    {
        return $this->belongsTo(ProgramsInterval::class, 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function program()
    {
        return $this->belongsTo(Program::class, 'program_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function paymentForm()
    {
        return $this->belongsTo(PaymentForm::class,'payment_form_id')
                    ->joinWhere(PaymentForm::class,'payment_form_id','=','id','inner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childProgramsInterval()
    {
        return $this->hasMany(ProgramsInterval::class,'parent_id');
    }
}
