<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class PhoneCall.
 *
 * @package namespace App\Entities;
 */
class PhoneCall extends AppEntity implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'type',
        'number',
        'branch',
        'date',
        'code',
        'path_to_file',
    ];

    protected $dates = [
        'created',
        'modified',
        'deleted',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function phonesCallsLog()
    {
        return $this->hasMany(PhoneCallsLog::class,'phone_call_id');
    }
}
