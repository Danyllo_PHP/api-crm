<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class FidelityExtract.
 *
 * @package namespace App\Entities;
 */
class FidelityExtract extends AppEntity implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'fidelity_id',
        'description',
        'transaction_type',
        'type',
        'value',
        'date',
        'locator',
        'stock_id',
    ];

    protected $dates = [
        'created',
        'modified',
        'deleted',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function fidelity()
    {
        return $this->belongsTo(Fidelity::class, 'fidelity_id')->joinWhere(Fidelity::class,'fidelity_id','=','id','inner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function stock()
    {
        return $this->belongsTo(Stock::class, 'stock_id');
    }
}
