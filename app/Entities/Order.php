<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Order.
 *
 * @package namespace App\Entities;
 */
class Order extends AppEntity implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'provider_id',
        'quotation_id',
        'program_id',
        'order_status_id',
        'order_type_id',
        'value',
        'value_paid',
        'value_payable',
        'price',
        'remittence_file_id',
        'observation',
        'origin',
        'system_creator',
        'department',
        'release_type_id',
        'banks_providers_segment_id',
        'payment_form_id',
        'receipt_sent',
        'stocked',
        'status_modified',
        'due_date',
        'session_id',
        'created_by',
        'modified_by',
    ];

    protected $dates = [
        'created',
        'modified',
        'deleted',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function providerLeft()
    {
        return $this->belongsTo(Provider::class, 'provider_id')
                    ->leftJoin(Provider::class,'provider_id', '=','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function provides()
    {
        return $this->belongsTo(Provider::class, 'provider_id')
                    ->joinWhere(Provider::class,'provider_id', '=', 'id','inner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function quotation()
    {
        return $this->belongsTo(Quotation::class,'quotation_id')
                    ->leftJoin(Quotation::class,'quotation_id', '=', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ordersProgram()
    {
        return $this->hasMany(OrdersProgram::class, 'order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function program()
    {
        return $this->belongsTo(Program::class, 'program_id')
                    ->leftJoin(Program::class,'program_id', '=', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function ordersStatus()
    {
        return $this->belongsTo(OrdersStatus::class,'order_status_id')
                    ->joinWhere(OrdersStatus::class, 'order_status_id', '=', 'id', 'inner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function ordersType()
    {
        return $this->belongsTo(OrdersType::class, 'order_type_id')
                    ->joinWhere(OrdersType::class, 'order_type_id', '=', 'id','inner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ordersFile()
    {
        return $this->hasMany(OrdersFile::class,'order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ordersNote()
    {
        return $this->hasMany(OrdersNote::class, 'order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function remittenceReturn()
    {
        return $this->belongsToMany(RemittenceReturn::class,'remittence_returns_orders','order_id','remittence_return_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function remittenceFile()
    {
        return $this->belongsToMany(RemittenceFile::class,'orders_remittence_files','order_id','remittence_file_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payment()
    {
        return $this->hasMany(Payment::class,'order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function banksProvidersSegment()
    {
        return $this->belongsTo(BanksProvidersSegment::class,'banks_providers_segment_id')
                    ->leftJoin(BanksProvidersSegment::class,'banks_providers_segment_id','=','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function paymentsForm()
    {
        return $this->belongsTo(PaymentForm::class,'payment_form_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function releaseType()
    {
        return $this->belongsTo(ReleaseType::class,'release_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public  function creator()
    {
        return $this->belongsTo(User::class, 'created_by')->leftJoin('Users','created_by','=','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function modifier()
    {
        return $this->belongsTo(User::class, 'modified_by')->leftJoin('Users','modified_by','=','id');
    }
}
