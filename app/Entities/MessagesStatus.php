<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class MessagesStatus.
 *
 * @package namespace App\Entities;
 */
class MessagesStatus extends AppEntity implements Transformable
{
    use TransformableTrait;

    protected $table = 'messages_status';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
    ];

    protected $dates = [
        'created',
        'modified',
        'deleted',
    ];

}
