<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Audit.
 *
 * @package namespace App\Entities;
 */
class Audit extends AppEntity implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'event',
        'model',
        'entity_id',
        'json_object',
        'description',
        'source_id',
        'delta_count',
        'source_ip',
        'source_url',
        'source_model',
        'audit_id',
    ];

    protected $dates = [
        'created',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function auditDelta()
    {
        return $this->hasMany(AuditDelta::class,'audit_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function relatedAudit()
    {
        return $this->hasMany(Audit::class,'audit_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()//Falta implemantar a condição do source_model == Users
    {
        return $this->belongsTo(User::class,'source_id')
                    ->whereHas(Audit::class, function ($query) {
                        $query->where('source_model', '=', 'Users');
                    });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function provider()//Falta implemantar a condição do source_model == Providers
    {
        return $this->belongsTo(Provider::class, 'source_id')
                    ->whereHas(Audit::class, function ($query) {
                        $query->where('source_model', '=', 'Providers');
                    });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function address()
    {
        return $this->belongsTo(Address::class, 'entity_id')
                    ->where('model', '=', 'Providers');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function phone()
    {
        return $this->belongsTo(Phone::class, 'entity_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function email()
    {
        return $this->belongsTo(Email::class, 'entity_id');
    }

}


