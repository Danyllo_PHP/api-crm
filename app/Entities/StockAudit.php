<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class StockAudit.
 *
 * @package namespace App\Entities;
 */
class StockAudit extends Model implements Transformable
{
    use TransformableTrait;

    protected $connection = 'pgsql';

    protected $table = 'stock_audits';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'logged_in',
        'miles_api',
        'stock_has_more',
        'status',
        'user_id',
        'is_credit',
        'company_initials',
        'stock_id',
        'miles_stock'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function stock()
    {
        return $this->belongsTo(Stock::class,'stock_id');
    }
}
