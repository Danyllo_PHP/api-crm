<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class EmailCampaignsEmailList.
 *
 * @package namespace App\Entities;
 */
class EmailCampaignsEmailList extends AppEntity implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'sent',
        'schedule',
        'email_campaign_id',
        'email_list_id',
    ];

    protected $dates = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function emailCampaign()
    {
        return $this->belongsTo(EmailCampaign::class,'email_campaign_id')
                    ->joinWhere(EmailCampaign::class,'email_campaign_id', '=', 'id','inner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function emailLists()
    {
        return $this->belongsTo(EmailList::class, 'email_list_id')
                    ->joinWhere(EmailList::class,'email_list_id', '=', 'id', 'inner');
    }
}
