<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class MailBox.
 *
 * @package namespace App\Entities;
 */
class MailBox extends AppEntity implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'message_id',
        'mail_box_email_id',
        'provider_id',
        'reference',
        'date',
        'subject',
        'from_name',
        'from_address',
        'to',
        'reply_to',
        'text_plain',
        'text_html',
        'email_origin',
        'attachments_origin',
    ];

    protected $dates = [
        'created',
        'modified',
        'deleted',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function MailBoxEmail()
    {
        return $this->belongsTo(MailBoxEmail::class, 'mail_box_email_id')
                    ->joinWhere(MailBoxEmail::class, 'mail_box_email_id', '=', 'id','inner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function provider()
    {
        return $this->belongsTo(Provider::class,'provider_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mailBoxAttachment()
    {
        return $this->hasMany(MailBoxAttachment::class, 'mail_box_id');
    }
}
