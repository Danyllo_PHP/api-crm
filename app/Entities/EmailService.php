<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class EmailService.
 *
 * @package namespace App\Entities;
 */
class EmailService extends AppEntity implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'email_profile_id',
        'name',
    ];

    protected $dates = [
        'created',
        'modified',
        'deleted',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function emailProfile()
    {
        return $this->belongsTo(EmailProfile::class,'email_profile_id')
                    ->joinWhere(EmailProfile::class, 'email_profile_id','=', 'id', 'inner');
    }
}
