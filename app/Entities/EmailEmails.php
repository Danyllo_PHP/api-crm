<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class EmailEmails.
 *
 * @package namespace App\Entities;
 */
class EmailEmails extends AppEntity implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'email',
        'unsubscribed',
        'blacklist_id',
    ];

    protected $dates = [
        'created',
        'modified',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function blacklist()
    {
        return $this->belongsTo(Blacklist::class, 'blacklist_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function emailList()
    {
        return $this->belongsToMany(EmailList::class,'email_lists_email_emails','email_email_id','email_list_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function emailCampaignLog()
    {
        return $this->hasMany(EmailCampaignLog::class,'email_email_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function emailNotification()
    {
        return $this->hasMany(EmailEmailNotification::class,'email')
                    ->where('email_email_notifications.source','=','campaign')
                    ->orderBy('created', 'desc');
    }
}
