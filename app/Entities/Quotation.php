<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Quotation.
 *
 * @package namespace App\Entities;
 */
class Quotation extends AppEntity implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'provider_id',
        'user_id',
        'quotation_status_id',
        'name',
        'email',
        'phone',
        'ip',
        'comment',
        'activity_count',
        'comment_count',
        'payment_count',
        'proposal_count',
        'quotation_file_count',
        'sms_count',
        'task_count',
        'archive',
        'email_send',
        'blacklist_id',
        'status_modified',
        'session_id',
        'observations_type_id',
        'observation',
        'device',
        'browser',
        'adw_campaign_id',
        'source',
        'id_reference_campaign',
        'created_by',
        'modified_by',
    ];

    protected $dates = [
        'created',
        'modified',
        'deleted',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function emailNotification()
    {
        return $this->hasMany(EmailEmailNotification::class,'email')
                    ->where('source','=','quotation')
                    ->orderBy('created', 'desc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function blacklist()
    {
        return $this->belongsTo(Blacklist::class,'blacklist_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function provider()
    {
        return $this->belongsTo(Provider::class,'provider_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function responsible()
    {
        return $this->belongsTo(User::class,'modified_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function quotationsStatus()
    {
        return $this->belongsTo(QuotationStatus::class,'quotation_status_id')
                    ->joinWhere(QuotationStatus::class,'quotation_status_id','=','id','inner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function quotationsObservationsType()
    {
        return $this->belongsTo(QuotationObservationsType::class,'observations_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public  function creator()
    {
        return $this->belongsTo(User::class, 'created_by')
                    ->leftJoin('Users','created_by','=','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function adWordsCampaigns()
    {
        return $this->belongsTo(AdwordsCampaign::class,'adw_campaign_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activity()
    {
        return $this->hasMany(Activity::class,'quotation_id')
                    ->orderBy('id', 'desc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function order()
    {
        return $this->hasMany(Order::class,'quotation_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comment()
    {
        return $this->hasMany(Comment::class,'quotation_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function proposal()
    {
        return $this->hasMany(Proposal::class,'quotation_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function task(){
        return $this->hasMany(Task::class,'quotation_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sms()
    {
        return $this->hasMany(Sms::class,'quotation_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function quotationsFile()
    {
        return $this->hasMany(QuotationFile::class,'quotation_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function program()
    {
        return $this->belongsToMany(Program::class,'programs_quotations','quotation_id','program_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function phoneCall()
    {
        return $this->hasMany(PhoneCall::class,'number','phone');
    }
}
