<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Aro.
 *
 * @package namespace App\Entities;
 */
class Aro extends AppEntity implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'parent_id',
        'model',
        'foreign_key',
        'alias',
        'lft',
        'rght'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function aco()
    {
        return $this->belongsToMany(Aco::class,'aros_acos','aro_id','acos_id');
    }
}
