<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Stock.
 *
 * @package namespace App\Entities;
 */
class Stock extends Model implements Transformable
{
    use TransformableTrait;

    protected $connection = 'pgsql';

    protected $table = 'stocks';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'company_initials',
        'miles',
        'card_number',
        'digital_signature',
        'provider_id',
        'payment_form',
        'last_lot_value',
        'category',
        'check_statements',
        'can_use_stock'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $hidden = [
        'access_password',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function provider()
    {
        return $this->belongsTo(StockProvider::class, 'provider_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function stockLog()
    {
        return $this->hasMany(StockLog::class,'stock_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function credit()
    {
        return $this->hasMany(StockLog::class,'stock_id')
                    ->where('transaction_type','credit')
                    ->orderBy('created_at', 'desc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lastCredit()
    {
        return $this->hasMany(StockLog::class,'stock_id')
                    ->where('transaction_type','credit')
                    ->orderBy('created_at', 'desc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lastDebit()
    {
        return $this->hasMany(StockLog::class,'stock_id')
                    ->where('transaction_type','debit')
                    ->orderBy('created_at', 'desc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function stockAudit()
    {
        return $this->hasMany(StockAudit::class,'stock_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fidelityExtract()
    {
        return $this->hasMany(FidelityExtract::class,'stock_id');
    }
}
