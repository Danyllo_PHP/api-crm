<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class RemittenceReturn.
 *
 * @package namespace App\Entities;
 */
class RemittenceReturn extends AppEntity implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'path',
        'status',
        'remittence_banks_id',
        'created_by',
    ];

    protected $dates = [
        'created',
        'modified',
        'deleted',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function remittenceReturnsOrder()
    {
        return $this->hasMany(RemittenceReturnOrder::class,'remittence_return_id')
                    ->orderBy('id', 'asc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function remittenceBank()
    {
        return $this->belongsTo(RemittenceBank::class,'remittence_banks_id')
                    ->joinWhere(RemittenceBank::class,'remittence_banks_id','=','id','inner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public  function creator()
    {
        return $this->belongsTo(User::class, 'created_by')
                    ->leftJoin('Users','created_by','=','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function order()
    {
        return $this->belongsToMany(Order::class,'remittence_returns_orders','remittence_return_id','order_id');
    }
}
