<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class EmailEmailsValidator.
 *
 * @package namespace App\Validators;
 */
class EmailEmailsValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                =>  'unique|integer',
            'email'             =>  'required|unique',
            'unsubscribed'      =>  'nullable|integer',
            'blacklist_id'      =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'email'             =>  'unique',
            'unsubscribed'      =>  'nullable|integer',
            'blacklist_id'      =>  'nullable|integer',
        ],
    ];
}
