<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class StockLogValidator.
 *
 * @package namespace App\Validators;
 */
class StockLogValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                    =>  'unique|integer',
            'transaction_type'      =>  'required|max:30',
            'price'                 =>  'nullable|numeric',
            'miles'                 =>  'required|integer',
            'validity'              =>  'nullable|date',
            'stock_id'              =>  'required|integer',
            'user_id'               =>  'nullable|integer',
            'payment_type'          =>  'nullable|integer',
            'wintour_sale'          =>  'required|integer',
            'refunded'              =>  'required|integer',
            'miles_remaining'       =>  'required|integer',
            'user_type'             =>  'nullable|integer',
            'emission_id'           =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'transaction_type'      =>  'max:30',
            'price'                 =>  'nullable|numeric',
            'miles'                 =>  'integer',
            'validity'              =>  'nullable|date',
            'stock_id'              =>  'integer',
            'user_id'               =>  'nullable|integer',
            'payment_type'          =>  'nullable|integer',
            'wintour_sale'          =>  'integer',
            'refunded'              =>  'integer',
            'miles_remaining'       =>  'integer',
            'user_type'             =>  'nullable|integer',
            'emission_id'           =>  'nullable|integer',
        ],
    ];
}
