<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class RemittenceConfigValidator.
 *
 * @package namespace App\Validators;
 */
class RemittenceConfigValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                        =>  'unique|integer',
            'name'                      =>  'required|max:30',
            'cnpj'                      =>  'required|max:14|cnpj',
            'address'                   =>  'required|max:30',
            'number'                    =>  'required|max:5',
            'complement'                =>  'required|max:15',
            'zip_code'                  =>  'required|max:5',
            'zip_code_complement'       =>  'required|max:3',
            'city'                      =>  'required|max:20',
            'uf'                        =>  'required|max:2',
            'payment_msg'               =>  'required|max:20',
            'remittence_banks_id'       =>  'required|integer',
            'created_by'                =>  'nullable|integer',
            'modified_by'               =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name'                      =>  'max:30',
            'cnpj'                      =>  'max:14|cnpj',
            'address'                   =>  'max:30',
            'number'                    =>  'max:5',
            'complement'                =>  'max:15',
            'zip_code'                  =>  'max:5',
            'zip_code_complement'       =>  'max:3',
            'city'                      =>  'max:20',
            'uf'                        =>  'max:2',
            'payment_msg'               =>  'max:20',
            'remittence_banks_id'       =>  'integer',
        ],
    ];
}
