<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class ActivityValidator.
 *
 * @package namespace App\Validators;
 */
class ActivityValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                    => 'unique|integer',
            'quotation_id'          =>  'required|integer',
            'title'                 =>  'required',
            'created_by'            =>  'nullable|integer',
            'modified_by'           =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'quotation_id'          =>  'integer',
            'title'                 =>  'min:3',
            'created_by'            =>  'nullable|integer',
            'modified_by'           =>  'nullable|integer',
        ],
    ];
}
