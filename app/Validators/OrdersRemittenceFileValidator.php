<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class OrdersRemittenceFileValidator.
 *
 * @package namespace App\Validators;
 */
class OrdersRemittenceFileValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                    =>  'unique|integer',
            'order_id'              =>  'required|integer',
            'remittence_file_id'    =>  'required|integer',
            'observation'           =>  'nullable|max:255',
            'created_by'            =>  'nullable|integer',
            'modified_by'           =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'order_id'              =>  'integer',
            'remittence_file_id'    =>  'integer',
            'observation'           =>  'nullable|max:255',
            'created_by'            =>  'nullable|integer',
            'modified_by'           =>  'nullable|integer',
        ],
    ];
}
