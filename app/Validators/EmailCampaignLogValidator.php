<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class EmailCampaignLogValidator.
 *
 * @package namespace App\Validators;
 */
class EmailCampaignLogValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                    =>  'unique|integer',
            'email_campaign_id'     =>  'required|integer',
            'email_email_id'        =>  'required|integer',
            'status'                =>  'required|max:255'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'email_campaign_id'     =>  'integer',
            'email_email_id'        =>  'integer',
            'status'                =>  'max:255',
        ],
    ];
}
