<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class ProvidersFileValidator.
 *
 * @package namespace App\Validators;
 */
class ProvidersFileValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'            =>  'unique|integer',
            'payment_id'    =>  'nullable',
            'program_id'    =>  'nullable',
            'provider_id'   =>  'nullable',
            'customer_id'   =>  'nullable',
            'created_by'    =>  'nullable|integer',
            'modified_by'   =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'payment_id'    =>  'nullable',
            'program_id'    =>  'nullable',
            'provider_id'   =>  'nullable',
            'customer_id'   =>  'nullable',
            'created_by'    =>  'nullable|integer',
            'modified_by'   =>  'nullable|integer',
        ],
    ];
}
