<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class ReleaseTypeValidator.
 *
 * @package namespace App\Validators;
 */
class ReleaseTypeValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                        =>  'unique|integer',
            'title'                     =>  'required|max:30',
            'selector_condition_1'      =>  'nullable|max:255',
            'value_condition_1'         =>  'nullable|max:255',
            'selector_condition_2'      =>  'nullable|max:255',
            'value_condition_2'         =>  'nullable|max:255',
            'created_by'                =>  'nullable|integer',
            'modified_by'               =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'title'                     =>  'max:30',
            'selector_condition_1'      =>  'nullable|max:255',
            'value_condition_1'         =>  'nullable|max:255',
            'selector_condition_2'      =>  'nullable|max:255',
            'value_condition_2'         =>  'nullable|max:255',
            'created_by'                =>  'nullable|integer',
            'modified_by'               =>  'nullable|integer',
        ],
    ];
}
