<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class RemittenceReturnValidator.
 *
 * @package namespace App\Validators;
 */
class RemittenceReturnValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                        =>  'unique|integer',
            'path'                      =>  'required|max:255',
            'status'                    =>  'required|boolean',
            'remittence_banks_id'       =>  'required|integer',
            'created_by'                =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'id'                        =>  'unique|integer',
            'path'                      =>  'max:255',
            'status'                    =>  'boolean',
            'remittence_banks_id'       =>  'integer',
            'created_by'                =>  'nullable|integer',
        ],
    ];
}
