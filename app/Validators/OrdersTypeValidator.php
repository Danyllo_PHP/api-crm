<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class OrdersTypeValidator.
 *
 * @package namespace App\Validators;
 */
class OrdersTypeValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'            =>  'unique|integer',
            'title'         =>  'required|max:30',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'title'         =>  'required|max:30',
        ],
    ];
}
