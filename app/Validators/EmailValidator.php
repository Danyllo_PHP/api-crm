<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class EmailValidator.
 *
 * @package namespace App\Validators;
 */
class EmailValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'            =>  'unique|integer',
            'email'         =>  'required|email|max:60|unique:email',
            'active'        =>  'nullable|boolean',
            'provider_id'   =>  'required|integer',
            'email_type'    =>  'nullable|boolean',
            'created_by'    =>  'nullable|integer',
            'modified_by'   =>  'nullable|string|max:45',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'email'         =>  'email|max:60|unique:email',
            'active'        =>  'nullable|boolean',
            'provider_id'   =>  'integer',
            'email_type'    =>  'nullable|boolean',
            'created_by'    =>  'nullable|integer',
            'modified_by'   =>  'nullable|string|max:45',
        ],
    ];
/**
     * Set data to validate
     *
     * @param array $data
     * @return $this
     */
    public function with(array $data)
    {
        if(!empty($data['id'])){
            $this->rules[ValidatorInterface::RULE_UPDATE]['email'] = "email|max:60|unique:email,".$data['id'];
        }
        $this->data = $data;
        return $this;
    }
}
