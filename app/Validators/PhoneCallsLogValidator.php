<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class PhoneCallsLogValidator.
 *
 * @package namespace App\Validators;
 */
class PhoneCallsLogValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                =>  'unique|integer',
            'phone_call_id'     =>  'nullable|integer',
            'user_id'           =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'phone_call_id'     =>  'nullable|integer',
            'user_id'           =>  'nullable|integer',
        ],
    ];
}
