<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class EmailListValidator.
 *
 * @package namespace App\Validators;
 */
class EmailListValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'            =>  'unique|integer',
            'name'          =>  'required|min:3|max:30',
            'description'   =>  'required|string',
            'created_by'    =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name'          =>  'min:3|max:30',
            'description'   =>  'string',
            'created_by'    =>  'nullable|integer',
        ],
    ];
}
