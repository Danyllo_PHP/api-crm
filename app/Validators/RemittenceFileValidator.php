<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class RemittenceFileValidator.
 *
 * @package namespace App\Validators;
 */
class RemittenceFileValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                    =>  'unique|integer',
            'url'                   =>  'nullable|max:255',
            'remittence_banks_id'   =>  'required|integer',
            'created_by'            =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'url'                   =>  'nullable|max:255',
            'remittence_banks_id'   =>  'integer',
            'created_by'            =>  'nullable|integer',
        ],
    ];
}
