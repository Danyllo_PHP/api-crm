<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class FidelityValidator.
 *
 * @package namespace App\Validators;
 */
class FidelityValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                    =>  'required|integer',
            'card_number'           =>  'required|max:45',
            'access_password'       =>  'nullable',
            'digital_signature'     =>  'nullable',
            'validity'              =>  'nullable|date_format:Y-m-d H:i:s',
            'program_id'            =>  'required|integer',
            'provider_id'           =>  'required|integer',
            'last_access'           =>  'nullable',
            'blocked_at'            =>  'nullable',
            'created_by'            =>  'nullable|integer',
            'modified_by'           =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'card_number'           =>  'max:45',
            'access_password'       =>  'nullable',
            'digital_signature'     =>  'nullable',
            'validity'              =>  'nullable|date_format:Y-m-d H:i:s',
            'program_id'            =>  'integer',
            'provider_id'           =>  'integer',
            'last_access'           =>  'nullable',
            'blocked_at'            =>  'nullable',
            'created_by'            =>  'nullable|integer',
            'modified_by'           =>  'nullable|integer',
        ],
    ];
}
