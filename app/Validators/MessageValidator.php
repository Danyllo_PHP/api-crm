<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class MessageValidator.
 *
 * @package namespace App\Validators;
 */
class MessageValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                    =>  'unique|integer',
            'parent_id'             =>  'required|integer',
            'parent_name'           =>  'required|max:255',
            'message_status_id'     =>  'required|integer',
            'text'                  =>  'required|string',
            'message_type_id'       =>  'required|integer',
            'schedule'              =>  'nullable|date_format:Y-m-d H:i:s',
            'show_for_creator'      =>  'required|boolean',
            'created_by'            =>  'nullable|integer',
            'modified_by'           =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'parent_id'             =>  'integer',
            'parent_name'           =>  'max:255',
            'message_status_id'     =>  'integer',
            'text'                  =>  'string',
            'message_type_id'       =>  'integer',
            'schedule'              =>  'nullable|date_format:Y-m-d H:i:s',
            'show_for_creator'      =>  'boolean',
            'created_by'            =>  'nullable|integer',
            'modified_by'           =>  'nullable|integer',
        ],
    ];
}
