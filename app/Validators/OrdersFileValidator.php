<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class OrdersFileValidator.
 *
 * @package namespace App\Validators;
 */
class OrdersFileValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'            =>  'unique|integer',
            'order_id'      =>  'required,integer',
            'title'         =>  'required,max:30',
            'dir'           =>  'nullable|max:255',
            'file'          =>  'required|max:255',
            'send'          =>  'nullable|boolean',
            'created_by'    =>  'nullable|integer',
            'modified_by'   =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'order_id'      =>  'integer',
            'title'         =>  'max:30',
            'dir'           =>  'nullable|max:255',
            'file'          =>  'max:255',
            'send'          =>  'nullable|boolean',
            'created_by'    =>  'nullable|integer',
            'modified_by'   =>  'nullable|integer',
        ],
    ];
}
