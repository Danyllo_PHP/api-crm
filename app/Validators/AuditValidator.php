<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class AuditValidator.
 *
 * @package namespace App\Validators;
 */
class AuditValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'event'         =>  'required|max:255',
            'model'         =>  'required|max:255',
            'entity_id'     =>  'required|max:255', 
            'json_object'   =>  'nullable|string',
            'description'   =>  'nullable|string',
            'source_id'     =>  'nullable|max:255',
            'delta_count'   =>  'required|integer',
            'source_ip'     =>  'nullable|max:255',
            'source_url'    =>  'nullable|max:255',
            'source_model'  =>  'required|max:255',
            'audit_id'      =>  'nullable|max:36',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'event'         =>  'max:255',
            'model'         =>  'max:255',
            'entity_id'     =>  'max:255',
            'json_object'   =>  'nullable|string',
            'description'   =>  'nullable|string',
            'source_id'     =>  'nullable|max:255',
            'delta_count'   =>  'integer',
            'source_ip'     =>  'nullable|max:255',
            'source_url'    =>  'nullable|max:255',
            'source_model'  =>  'max:255',
            'audit_id'      =>  'nullable|max:36',
        ],
    ];
}
