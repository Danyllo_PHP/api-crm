<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class MailBoxEmailValidator.
 *
 * @package namespace App\Validators;
 */
class MailBoxEmailValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'            =>  'unique|integer',
            'description'   =>  'required|max:255',
            'email'         =>  'required|email',
            'mail_in_use'   =>  'required|boolean',
            'name'          =>  'nullable|max:30',
            'password'      =>  'required|max:30',
            'address'       =>  'required|max:255',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'description'   =>  'max:255',
            'email'         =>  'email',
            'mail_in_use'   =>  'boolean',
            'name'          =>  'nullable|max:30',
            'password'      =>  'max:30',
            'address'       =>  'max:255',
        ],
    ];
}
