<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class GroupValidator.
 *
 * @package namespace App\Validators;
 */
class GroupValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'           =>  'unique|integer',
            'name'         =>  'required|max:30',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name'          =>  'required|max:30',
        ],
    ];
}
