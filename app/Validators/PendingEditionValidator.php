<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class PendingEditionValidator.
 *
 * @package namespace App\Validators;
 */
class PendingEditionValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'            =>  'unique|integer',
            'model'         =>  'required|max:255',
            'primary_key'   =>  'required|integer',
            'field'         =>  'required|max:255',
            'value'         =>  'required|string',
            
        ],
        ValidatorInterface::RULE_UPDATE => [
            'model'         =>  'max:255',
            'primary_key'   =>  'integer',
            'field'         =>  'max:255',
            'value'         =>  'string',
        ],
    ];
}
