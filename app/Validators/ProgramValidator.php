<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class ProgramValidator.
 *
 * @package namespace App\Validators;
 */
class ProgramValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                            =>  'unique|integer',
            'title'                         =>  'required|max:30',
            'code'                          =>  'required|max:3',
            'program_quotation_count'       =>  'required|integer',
            'miles_in_stock'                =>  'required|numeric',
            'created_by'                    =>  'nullable|integer',
            'modified_by'                   =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'title'                         =>  'max:30',
            'code'                          =>  'max:3',
            'program_quotation_count'       =>  'integer',
            'miles_in_stock'                =>  'numeric',
            'created_by'                    =>  'nullable|integer',
            'modified_by'                   =>  'nullable|integer',
        ],
    ];
}
