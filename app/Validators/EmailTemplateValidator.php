<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class EmailTemplateValidator.
 *
 * @package namespace App\Validators;
 */
class EmailTemplateValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'        =>  'unique|integer',
            'name'      =>  'required|string|max:30',
            'body'      =>  'required|string',
            'type'      =>  'nullable',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name'      =>  'string|max:30',
            'body'      =>  'string',
            'type'      =>  'nullable',
        ],
    ];
}
