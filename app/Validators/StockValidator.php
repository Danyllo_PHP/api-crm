<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class StockValidator.
 *
 * @package namespace App\Validators;
 */
class StockValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                    =>  'unique|integer',
            'company_initials'      =>  'required|max:3',
            'miles'                 =>  'required|integer',
            'card_number'           =>  'required|max:25',
            'digital_signature'     =>  'required|max:25',
            'provider_id'           =>  'required|integer',
            'payment_form'          =>  'required|max:50',
            'last_lot_value'        =>  'required|numeric',
            'category'              =>  'nullable|max:50',
            'check_statements'      =>  'nullable|boolean',
            'can_use_stock'         =>  'nullable|boolean'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'company_initials'      =>  'unique|integer',
            'miles'                 =>  'max:3',
            'card_number'           =>  'integer',
            'digital_signature'     =>  'max:25',
            'provider_id'           =>  'max:25',
            'payment_form'          =>  'integer',
            'last_lot_value'        =>  'max:50',
            'category'              =>  'numeric',
            'check_statements'      =>  'nullable|max:50',
            'can_use_stock'         =>  'nullable|boolean',
        ],
    ];
}
