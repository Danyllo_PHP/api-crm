<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class ProviderValidator.
 *
 * @package namespace App\Validators;
 */
class ProviderValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                            =>  'unique|integer',
            'provider_status_id'            =>  'required|integer',
            'user_id'                       =>  'nullable|integer',
            'parent_id'                     =>  'nullable|integer',
            'provider_occupation_id'        =>  'nullable|integer',
            'name'                          =>  'required|max:30',
            'password'                      =>  'required|max:30',
            'email'                         =>  'required|email|max:255',
            'document'                      =>  'nullable|max:255',
            'cpf'                           =>  'required|unique',
            'score_id'                      =>  'nullable|max:255',
            'score_value'                   =>  'nullable|numeric',
            'score_status'                  =>  'nullable|max:255',
            'score_updated'                 =>  'nullable|date_format:Y-m-d H:i:s',
            'phone'                         =>  'nullable|max:255',
            'cellphone'                     =>  'nullable|max:255',
            'company'                       =>  'nullable|max:255',
            'occupation'                    =>  'nullable|max:255',
            'company_email'                 =>  'nullable|max:255',
            'company_phone'                 =>  'nullable|max:255',
            'company_phone_branch'          =>  'nullable|max:255',
            'comment'                       =>  'nullable|string',
            'payment_count'                 =>  'required|integer',
            'quotation_count'               =>  'required|integer',
            'provider_file_count'           =>  'required|integer',
            'rg'                            =>  'nullable|max:255',
            'birthday'                      =>  'required|date',
            'gender'                        =>  'required|max:255',
            'street_view'                   =>  'nullable|max:255',
            'facebook'                      =>  'nullable|max:255',
            'linkedin'                      =>  'nullable|max:255',
            'restriction'                   =>  'required|boolean',
            'company_site'                  =>  'nullable|max:255',
            'orders_count'                  =>  'nullable|integer',
            'order_status_id'               =>  'required|integer',
            'payment_form_id'               =>  'required|integer',
            'cnpj'                          =>  'nullable|max:255',
            'company_extra'                 =>  'nullable|string',
            'status_modified'               =>  'required|date_format:Y-m-d H:i:s',
            'session_id'                    =>  'required|max:255',
            'was_consulted'                 =>  'required|boolean',
            'was_consulted_spc'             =>  'required|boolean',
            'last_quotation_id'             =>  'nullable|integer',
            'created_by'                    =>  'nullable|integer',
            'modified_by'                   =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'provider_status_id'            =>  'integer',
            'user_id'                       =>  'nullable|integer',
            'parent_id'                     =>  'nullable|integer',
            'provider_occupation_id'        =>  'nullable|integer',
            'name'                          =>  'max:30',
            'password'                      =>  'max:30',
            'email'                         =>  'email|max:255',
            'document'                      =>  'nullable|max:255',
            'cpf'                           =>  'required|unique',
            'score_id'                      =>  'nullable|max:255',
            'score_value'                   =>  'nullable|numeric',
            'score_status'                  =>  'nullable|max:255',
            'score_updated'                 =>  'nullable|date_format:Y-m-d H:i:s',
            'phone'                         =>  'nullable|max:255',
            'cellphone'                     =>  'nullable|max:255',
            'company'                       =>  'nullable|max:255',
            'occupation'                    =>  'nullable|max:255',
            'company_email'                 =>  'nullable|max:255',
            'company_phone'                 =>  'nullable|max:255',
            'company_phone_branch'          =>  'nullable|max:255',
            'comment'                       =>  'nullable|string',
            'payment_count'                 =>  'integer',
            'quotation_count'               =>  'integer',
            'provider_file_count'           =>  'integer',
            'rg'                            =>  'nullable|max:255',
            'birthday'                      =>  'date',
            'gender'                        =>  'max:255',
            'street_view'                   =>  'nullable|max:255',
            'facebook'                      =>  'nullable|max:255',
            'linkedin'                      =>  'nullable|max:255',
            'restriction'                   =>  'boolean',
            'company_site'                  =>  'nullable|max:255',
            'orders_count'                  =>  'nullable|integer',
            'order_status_id'               =>  'integer',
            'payment_form_id'               =>  'integer',
            'cnpj'                          =>  'nullable|max:255',
            'company_extra'                 =>  'nullable|string',
            'status_modified'               =>  'date_format:Y-m-d H:i:s',
            'session_id'                    =>  'max:255',
            'was_consulted'                 =>  'boolean',
            'was_consulted_spc'             =>  'boolean',
            'last_quotation_id'             =>  'nullable|integer',
            'created_by'                    =>  'nullable|integer',
            'modified_by'                   =>  'nullable|integer',
        ],
    ];
}
