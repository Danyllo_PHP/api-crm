<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class QuotationValidator.
 *
 * @package namespace App\Validators;
 */
class QuotationValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                        =>  'unique|integer',
            'provider_id'               =>  'nullable|integer',
            'user_id'                   =>  'nullable|integer',
            'quotation_status_id'       =>  'required|integer',
            'name'                      =>  'required|max:30',
            'email'                     =>  'required|email',
            'phone'                     =>  'required|max:20',
            'ip'                        =>  'required|ip',
            'comment'                   =>  'nullable|string',
            'activity_count'            =>  'required|integer',
            'comment_count'             =>  'required|integer',
            'payment_count'             =>  'required|integer',
            'proposal_count'            =>  'required|integer',
            'quotation_file_count'      =>  'required|integer',
            'sms_count'                 =>  'required|integer',
            'task_count'                =>  'required|integer',
            'archive'                   =>  'required|boolean',
            'email_send'                =>  'nullable|boolean',
            'blacklist_id'              =>  'nullable|integer',
            'status_modified'           =>  'required|date_format:Y-m-d H:i:s',
            'session_id'                =>  'required|max:255',
            'observations_type_id'      =>  'nullable|integer',
            'observation'               =>  'nullable|max:255',
            'device'                    =>  'nullable|max:255',
            'browser'                   =>  'nullable|max:255',
            'adw_campaign_id'           =>  'nullable|integer',
            'created_by'                =>  'nullable|integer',
            'modified_by'               =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'provider_id'               =>  'nullable|integer',
            'user_id'                   =>  'nullable|integer',
            'quotation_status_id'       =>  'integer',
            'name'                      =>  'max:30',
            'email'                     =>  'email',
            'phone'                     =>  'max:20',
            'ip'                        =>  'ip',
            'comment'                   =>  'nullable|string',
            'activity_count'            =>  'integer',
            'comment_count'             =>  'integer',
            'payment_count'             =>  'integer',
            'proposal_count'            =>  'integer',
            'quotation_file_count'      =>  'integer',
            'sms_count'                 =>  'integer',
            'task_count'                =>  'integer',
            'archive'                   =>  'boolean',
            'email_send'                =>  'nullable|boolean',
            'blacklist_id'              =>  'nullable|integer',
            'status_modified'           =>  'required|date_format:Y-m-d H:i:s',
            'session_id'                =>  'max:255',
            'observations_type_id'      =>  'nullable|integer',
            'observation'               =>  'nullable|max:255',
            'device'                    =>  'nullable|max:255',
            'browser'                   =>  'nullable|max:255',
            'adw_campaign_id'           =>  'nullable|integer',
            'created_by'                =>  'nullable|integer',
            'modified_by'               =>  'nullable|integer',
        ],
    ];
}
