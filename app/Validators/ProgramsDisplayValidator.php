<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class ProgramsDisplayValidator.
 *
 * @package namespace App\Validators;
 */
class ProgramsDisplayValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'            =>  'unique|integer',
            'program_id'    =>  'required|integer',
            'value'         =>  'required|integer',
            'price'         =>  'nullable|numeric',
            'created_by'    =>  'nullable|integer',
            'modified_by'   =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'program_id'    =>  'integer',
            'value'         =>  'integer',
            'price'         =>  'nullable|numeric',
            'created_by'    =>  'nullable|integer',
            'modified_by'   =>  'nullable|integer',
        ],
    ];
}
