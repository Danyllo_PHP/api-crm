<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class SessionValidator.
 *
 * @package namespace App\Validators;
 */
class SessionValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'        =>  'unique|integer',
            'data'      =>  'nullable',
            'user_id'   =>  'nullable|integer',
            'expires'   =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'data'          =>  'nullable',
            'user_id'       =>  'nullable|integer',
            'expires'       =>  'nullable|integer',
        ],
    ];
}
