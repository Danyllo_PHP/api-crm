<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class PaymentsProgramValidator.
 *
 * @package namespace App\Validators;
 */
class PaymentsProgramValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'            =>  'unique|integer',
            'payment_id'    =>  'required|integer',
            'program_id'    =>  'required|integer',
            'value'         =>  'required|integer',
            'price'         =>  'required|numeric',
            'created_by'    =>  'nullable|integer',
            'modified_by'   =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'payment_id'    =>  'integer',
            'program_id'    =>  'integer',
            'value'         =>  'integer',
            'price'         =>  'numeric',
            'created_by'    =>  'nullable|integer',
            'modified_by'   =>  'nullable|integer',
        ],
    ];
}
