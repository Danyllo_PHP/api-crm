<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class FidelityExtractValidator.
 *
 * @package namespace App\Validators;
 */
class FidelityExtractValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                    =>  'unique|integer',
            'fidelity_id'           =>  'required|integer',
            'description'           =>  'nullable',
            'transaction_type'      =>  'required|max:255',
            'type'                  =>  'required|max:255',
            'value'                 =>  'required|numeric',
            'locator'               =>  'nullable|string',
            'stock_id'              =>  'required|integer',
            'date'                  =>  'required|date_format:Y-m-d H:i:s',

        ],
        ValidatorInterface::RULE_UPDATE => [
            'fidelity_id'           =>  'integer',
            'description'           =>  'nullable',
            'transaction_type'      =>  'max:255',
            'type'                  =>  'max:255',
            'value'                 =>  'numeric',
            'locator'               =>  'nullable|string',
            'stock_id'              =>  'integer',
            'date'                  =>  'date_format:Y-m-d H:i:s',
        ],
    ];
}
