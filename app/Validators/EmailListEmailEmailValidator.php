<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class EmailListEmailEmailValidator.
 *
 * @package namespace App\Validators;
 */
class EmailListEmailEmailValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                =>  'unique|integer',
            'email_list_id'     =>  'required|integer',
            'email_email_id'    =>  'required|integer',

        ],
        ValidatorInterface::RULE_UPDATE => [
            'email_list_id'     =>  'integer',
            'email_email_id'    =>  'integer',
        ],
    ];
}
