<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class EmailProfileValidator.
 *
 * @package namespace App\Validators;
 */
class EmailProfileValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                    =>  'unique|integer',
            'email_transport_id'    =>  'required|integer',
            'name'                  =>  'required|string|max:30',
            'from'                  =>  'required|string|max:30',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'email_transport_id'    =>  'integer',
            'name'                  =>  'string|max:30',
            'from'                  =>  'string|max:30',
        ],
    ];
}
