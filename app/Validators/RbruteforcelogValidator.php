<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class RbruteforcelogValidator.
 *
 * @package namespace App\Validators;
 */
class RbruteforcelogValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'    =>  'unique|integer',
            'data'  =>  'nullable|string'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'data'          =>  'nullable|string',
        ],
    ];
}
