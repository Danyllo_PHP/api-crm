<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class AuditDeltaValidator.
 *
 * @package namespace App\Validators;
 */
class AuditDeltaValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'audit_id'          =>  'required|max:36',
            'property_name'     =>  'required|max:255',
            'old_value'         =>  'nullable|string',
            'new_value'         =>  'nullable|string'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'audit_id'          =>  'max:36',
            'property_name'     =>  'max:255',
            'old_value'         =>  'nullable|string',
            'new_value'         =>  'nullable|string',
        ],
    ];
}
