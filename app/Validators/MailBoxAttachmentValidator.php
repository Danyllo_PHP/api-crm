<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class MailBoxAttachmentValidator.
 *
 * @package namespace App\Validators;
 */
class MailBoxAttachmentValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                    =>  'unique|integer',
            'mail_box_id'           =>  'required|integer',
            'name'                  =>  'required|max:255',
            'file_path'             =>  'nullable|string',
            'attachments_origin'    =>  'nullable|string',
            'path_to_file'          =>  'required|string',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'mail_box_id'           =>  'integer',
            'name'                  =>  'max:255',
            'file_path'             =>  'nullable|string',
            'attachments_origin'    =>  'nullable|string',
            'path_to_file'          =>  'string',
        ],
    ];
}
