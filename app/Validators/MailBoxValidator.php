<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class MailBoxValidator.
 *
 * @package namespace App\Validators;
 */
class MailBoxValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                    =>  'unique|integer',
            'message_id'            =>  'nullable|string',
            'mail_box_email_id'     =>  'required|integer',
            'provider_id'           =>  'nullable|integer',
            'reference'             =>  'nullable|max:65',
            'date'                  =>  'required|date_format:Y-m-d H:i:s',
            'subject'               =>  'nullable|max:255',
            'from_name'             =>  'nullable|max:255',
            'from_address'          =>  'nullable|max:255',
            'to'                    =>  'nullable|string',
            'reply_to'              =>  'nullable|string',
            'text_plain'            =>  'nullable|string',
            'text_html'             =>  'nullable|string',
            'email_origin'          =>  'nullable|string',
            'attachments_origin'    =>  'nullable|string',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'message_id'            =>  'nullable|string',
            'mail_box_email_id'     =>  'integer',
            'provider_id'           =>  'nullable|integer',
            'reference'             =>  'nullable|max:65',
            'date'                  =>  'date_format:Y-m-d H:i:s',
            'subject'               =>  'nullable|max:255',
            'from_name'             =>  'nullable|max:255',
            'from_address'          =>  'nullable|max:255',
            'to'                    =>  'nullable|string',
            'reply_to'              =>  'nullable|string',
            'text_plain'            =>  'nullable|string',
            'text_html'             =>  'nullable|string',
            'email_origin'          =>  'nullable|string',
            'attachments_origin'    =>  'nullable|string',
        ],
    ];
}
