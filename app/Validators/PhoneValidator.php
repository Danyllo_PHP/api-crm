<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class PhoneValidator.
 *
 * @package namespace App\Validators;
 */
class PhoneValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'            =>  'unique|integer',
            'number'        =>  'required|max:45',
            'branch'        =>  'nullable|max:10',
            'phone_type'    =>  'nullable|boolean',
            'provider_id'   =>  'required|integer',
            'created_by'    =>  'nullable|integer',
            'modified_by'   =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'number'        =>  'max:45',
            'branch'        =>  'nullable|max:10',
            'phone_type'    =>  'nullable|boolean',
            'provider_id'   =>  'integer',
            'created_by'    =>  'nullable|integer',
            'modified_by'   =>  'nullable|integer',
        ],
    ];
}
