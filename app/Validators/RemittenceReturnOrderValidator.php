<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class RemittenceReturnOrderValidator.
 *
 * @package namespace App\Validators;
 */
class RemittenceReturnOrderValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                        =>  'unique|integer',
            'order_id'                  =>  'nullable|integer',
            'remittence_return_id'      =>  'required|integer',
            'order_status_id'           =>  'nullable|integer',
            'provider_name'             =>  'required|max:255',
            'provider_cpf'              =>  'required|max:20',
            'order_price'               =>  'required|numeric',
            'return_order_id'           =>  'nullable|integer',
            'return_codes'              =>  'nullable|max:255',
            'protocol'                  =>  'nullable|max:255',
            'auth'                      =>  'nullable|max:255',
            'commitment'                =>  'nullable|max:255',
            'payment_date'              =>  'nullable|data',
            'payment_due_date'          =>  'nullable|data',
            'created_by'                =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'order_id'                  =>  'nullable|integer',
            'remittence_return_id'      =>  'integer',
            'order_status_id'           =>  'nullable|integer',
            'provider_name'             =>  'max:255',
            'provider_cpf'              =>  'max:20',
            'order_price'               =>  'numeric',
            'return_order_id'           =>  'nullable|integer',
            'return_codes'              =>  'nullable|max:255',
            'protocol'                  =>  'nullable|max:255',
            'auth'                      =>  'nullable|max:255',
            'commitment'                =>  'nullable|max:255',
            'payment_date'              =>  'nullable|data',
            'payment_due_date'          =>  'nullable|data',
            'created_by'                =>  'nullable|integer',
        ],
    ];
}
