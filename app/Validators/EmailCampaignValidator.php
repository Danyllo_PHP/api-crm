<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class EmailCampaignValidator.
 *
 * @package namespace App\Validators;
 */
class EmailCampaignValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                    =>  'unique|integer',
            'name'                  =>  'required|string|max:255|min:3',
            'template_id'           =>  'required|integer',
            'email_transport_id'    =>  'required|integer',
            'email_profile_id'      =>  'required|integer',
            'sender'                =>  'nullable|max:255',
            'paused'                =>  'nullable|integer',
            'ready'                 =>  'required|boolean',
            'started'               =>  'required|boolean',
            'created_by'            =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name'                  =>  'string|max:255|min:3',
            'template_id'           =>  'integer',
            'email_transport_id'    =>  'integer',
            'email_profile_id'      =>  'integer',
            'sender'                =>  'nullable|max:255',
            'paused'                =>  'nullable|integer',
            'ready'                 =>  'boolean',
            'started'               =>  'boolean',
            'created_by'            =>  'nullable|integer',
        ],
    ];
}
