<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class RbruteforceValidator.
 *
 * @package namespace App\Validators;
 */
class RbruteforceValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'ip'        =>  'unique|integer',
            'url'       =>  'required|max:255',
            'expire'    =>  'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'ip'            =>  'unique|integer',
            'url'           =>  'max:255',
        ],
    ];
}
