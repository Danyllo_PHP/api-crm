<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class PhoneCallValidator.
 *
 * @package namespace App\Validators;
 */
class PhoneCallValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'            =>  'unique|integer',
            'type'          =>  'required|max:10',
            'number'        =>  'required|max:20',
            'branch'        =>  'required|max:20',
            'date'          =>  'required|date_format:Y-m-d H:i:s',
            'code'          =>  'required|max:20',
            'path_to_file'  =>  'required|max:255',
            
        ],
        ValidatorInterface::RULE_UPDATE => [
            'type'          =>  'max:10',
            'number'        =>  'max:20',
            'branch'        =>  'max:20',
            'date'          =>  'date_format:Y-m-d H:i:s',
            'code'          =>  'max:20',
            'path_to_file'  =>  'max:255',
            'modified'      =>  'required|date_format:Y-m-d H:i:s',
            'deleted'       =>  'nullable|date_format:Y-m-d H:i:s',
        ],
    ];
}
