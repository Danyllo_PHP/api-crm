<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class EmailTransportValidator.
 *
 * @package namespace App\Validators;
 */
class EmailTransportValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'            =>  'unique|integer',
            'name'          =>  'required|max:3',
            'class_name'    =>  'nullable|max:255',
            'host'          =>  'required|max:255',
            'port'          =>  'nullable|integer',
            'username'      =>  'required|max:30',
            'password'      =>  'required|max:30',
            'client'        =>  'nullable|max:255',
            'tls'           =>  'nullable|boolean',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name'          =>  'max:3',
            'class_name'    =>  'nullable|max:255',
            'host'          =>  'max:255',
            'port'          =>  'nullable|integer',
            'username'      =>  'max:30',
            'password'      =>  'max:30',
            'client'        =>  'nullable|max:255',
            'tls'           =>  'nullable|boolean',
        ],
    ];
}
