<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class PaymentsStatusValidator.
 *
 * @package namespace App\Validators;
 */
class PaymentsStatusValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'            =>  'unique|integer',
            'title'         =>  'required|max:30',
            'created_by'    =>  'nullable|integer',
            'modified_by'   =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'title'         =>  'required|max:30',
            'created_by'    =>  'nullable|integer',
            'modified_by'   =>  'nullable|integer',
        ],
    ];
}
