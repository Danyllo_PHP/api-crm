<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class ProgramsQuotationValidator.
 *
 * @package namespace App\Validators;
 */
class ProgramsQuotationValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                =>  'unique|integer',
            'program_id'        =>  'required|integer',
            'quotation_id'      =>  'required|integer',
            'payment_form_id'   =>  'required|integer',
            'username'          =>  'nullable|max:30',
            'password'          =>  'nullable|max:30',
            'value'             =>  'required|integer',
            'price'             =>  'required|numeric',
            'created_by'        =>  'nullable|integer',
            'modified_by'       =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'program_id'        =>  'integer',
            'quotation_id'      =>  'integer',
            'payment_form_id'   =>  'integer',
            'username'          =>  'nullable|max:30',
            'password'          =>  'nullable|max:30',
            'value'             =>  'integer',
            'price'             =>  'numeric',
            'created_by'        =>  'nullable|integer',
            'modified_by'       =>  'nullable|integer',
        ],
    ];
}
