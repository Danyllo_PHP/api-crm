<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class BankValidator.
 *
 * @package namespace App\Validators;
 */
class BankValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                =>  'unique|integer',
            'title'             =>  'required|max:255',
            'code'              =>  'required|integer',
            'payment_count'     =>  'required|integer',
            'created_by'        =>  'nullable|integer',
            'modified_by'       =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'title'             =>  'max:255',
            'code'              =>  'integer',
            'payment_count'     =>  'integer',
            'created_by'        =>  'nullable|integer',
            'modified_by'       =>  'nullable|integer',
        ],
    ];
}
