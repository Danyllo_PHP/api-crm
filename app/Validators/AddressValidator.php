<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class AddressValidator.
 *
 * @package namespace App\Validators;
 */
class AddressValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'            => 'unique|integer',
            'parent_id'     => 'required|integer',
            'model'         => 'required|string',
            'zip_code'      => 'required|string',
            'address'       => 'required|min:3|max:255',
            'number'        => 'required|min:1',
            'complement'    => 'nullable|min:3|max:255',
            'neighborhood'  => 'nullable|min:3|max:255',
            'city'          => 'nullable|min:3|max:255',
            'state'         => 'nullable|min:3|max:255',
            'address_type'  => 'nullable|integer',
            'street_view'   => 'nullable|min:3|max:255',
            'extra'         => 'nullable|string|max:255',
            'created_by'    => 'nullable|integer',
            'modified_by'   => 'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'parent_id'     => 'integer',
            'model'         => 'string',
            'zip_code'      => 'string',
            'address'       => 'min:3|max:255',
            'number'        => 'min:1',
            'complement'    => 'nullable|min:3|max:255',
            'neighborhood'  => 'nullable|min:3|max:255',
            'city'          => 'nullable|min:3|max:255',
            'state'         => 'nullable|min:3|max:255',
            'address_type'  => 'nullable|integer',
            'street_view'   => 'nullable|min:3|max:255',
            'extra'         => 'nullable|string|max:255',
            'created_by'    => 'nullable|integer',
            'modified_by'   => 'nullable|integer',

        ],
    ];
}
