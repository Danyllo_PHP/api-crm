<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class SmsValidator.
 *
 * @package namespace App\Validators;
 */
class SmsValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                =>  'unique|integer',
            'dongle'            =>  'required|string',
            'dongle_number'     =>  'nullable|max:255',
            'number'            =>  'required|max:255',
            'provider_id'       =>  'nullable|integer',
            'msg'               =>  'required|max:255',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'dongle'            =>  'string',
            'dongle_number'     =>  'nullable|max:255',
            'number'            =>  'max:255',
            'provider_id'       =>  'nullable|integer',
            'msg'               =>  'max:255',
        ],
    ];
}
