<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class ProgramsIntervalValidator.
 *
 * @package namespace App\Validators;
 */
class ProgramsIntervalValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                =>  'unique|integer',
            'parent_id'         =>  'nullable|integer',
            'program_id'        =>  'nullable|integer',
            'payment_form_id'   =>  'required|integer',
            'display_start'     =>  'required|integer',
            'display_end'       =>  'nullable|integer',
            'display_interval'  =>  'required|integer',
            'price'             =>  'required|numeric',
            'expire'            =>  'nullable|date_format:Y-m-d H:i:s',
            'created_by'        =>  'nullable|integer',
            'modified_by'       =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'parent_id'         =>  'nullable|integer',
            'program_id'        =>  'nullable|integer',
            'payment_form_id'   =>  'integer',
            'display_start'     =>  'integer',
            'display_end'       =>  'nullable|integer',
            'display_interval'  =>  'integer',
            'price'             =>  'numeric',
            'expire'            =>  'nullable|date_format:Y-m-d H:i:s',
            'created_by'        =>  'nullable|integer',
            'modified_by'       =>  'nullable|integer',
        ],
    ];
}
