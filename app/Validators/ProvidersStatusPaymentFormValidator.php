<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class ProvidersStatusPaymentFormValidator.
 *
 * @package namespace App\Validators;
 */
class ProvidersStatusPaymentFormValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                    =>  'unique|integer',
            'provider_status_id'    =>  'required|integer',
            'payment_form_id'       =>  'required|integer',
            'created_by'            =>  'nullable|integer',
            'modified_by'           =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'provider_status_id'    =>  'integer',
            'payment_form_id'       =>  'integer',
            'created_by'            =>  'nullable|integer',
            'modified_by'           =>  'nullable|integer',
        ],
    ];
}
