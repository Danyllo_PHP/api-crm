<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class EmailServiceValidator.
 *
 * @package namespace App\Validators;
 */
class EmailServiceValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                    =>  'unique|integer',
            'email_profile_id'      =>  'required|integer',
            'name'                  =>  'required|string|max:30',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'email_profile_id'      =>  'integer',
            'name'                  =>  'string|max:30',
        ],
    ];
}
