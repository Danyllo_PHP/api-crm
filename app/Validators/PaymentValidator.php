<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class PaymentValidator.
 *
 * @package namespace App\Validators;
 */
class PaymentValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                    =>  'unique|integer',
            'payment_status_id'     =>  'required|integer',
            'provider_id'           =>  'nullable|integer',
            'quotation_id'          =>  'nullable|integer',
            'program_id'            =>  'nullable|integer',
            'bank_provider_id'      =>  'nullable|integer',
            'bank_id'               =>  'nullable|integer',
            'agency'                =>  'required|max:255',
            'type'                  =>  'required|max:255',
            'account'               =>  'required|max:255',
            'operation'             =>  'nullable|max:255',
            'date'                  =>  'nullable|date',
            'price'                 =>  'required|numeric',
            'value'                 =>  'nullable|numeric',
            'comment'               =>  'nullable|string',
            'status_modified'       =>  'nullable|date_format:Y-m-d H:i:s',
            'order_id'              =>  'nullable|integer',
            'invoice_id'            =>  'nullable|integer',
            'stocked'               =>  'required|integer',
            'archived'              =>  'required|integer',
            'receipt_sent'          =>  'nullable|integer',
            'info'                  =>  'nullable|string',
            'created_by'            =>  'nullable|integer',
            'modified_by'           =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'payment_status_id'     =>  'integer',
            'provider_id'           =>  'nullable|integer',
            'quotation_id'          =>  'nullable|integer',
            'program_id'            =>  'nullable|integer',
            'bank_provider_id'      =>  'nullable|integer',
            'bank_id'               =>  'nullable|integer',
            'agency'                =>  'max:255',
            'type'                  =>  'max:255',
            'account'               =>  'max:255',
            'operation'             =>  'nullable|max:255',
            'date'                  =>  'nullable|date',
            'price'                 =>  'numeric',
            'value'                 =>  'nullable|numeric',
            'comment'               =>  'nullable|string',
            'status_modified'       =>  'nullable|date_format:Y-m-d H:i:s',
            'order_id'              =>  'nullable|integer',
            'invoice_id'            =>  'nullable|integer',
            'stocked'               =>  'integer',
            'archived'              =>  'integer',
            'receipt_sent'          =>  'nullable|integer',
            'info'                  =>  'nullable|string',
            'created_by'            =>  'nullable|integer',
            'modified_by'           =>  'nullable|integer',
        ],
    ];
}
