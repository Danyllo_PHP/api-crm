<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class QuotationObservationsTypeValidator.
 *
 * @package namespace App\Validators;
 */
class QuotationObservationsTypeValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'        =>  'unique|integer',
            'title'     =>  'required|max:30',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'title'         =>  'required|max:30',
        ],
    ];
}
