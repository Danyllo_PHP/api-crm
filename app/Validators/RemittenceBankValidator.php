<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class RemittenceBankValidator.
 *
 * @package namespace App\Validators;
 */
class RemittenceBankValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'            =>  'unique|integer',
            'name'          =>  'required|max:30',
            'classname'     =>  'required|max:30',
            'code'          =>  'required|max:3',
            'agreement'     =>  'nullable|max:20',
            'agency'        =>  'required|max:5',
            'digit'         =>  'nullable|max:1',
            'account'       =>  'required|max:12',
            'created_by'    =>  'nullable|integer',
            'modified_by'   =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name'          =>  'max:30',
            'classname'     =>  'max:30',
            'code'          =>  'max:3',
            'agreement'     =>  'nullable|max:20',
            'agency'        =>  'max:5',
            'digit'         =>  'nullable|max:1',
            'account'       =>  'max:12',
            'created_by'    =>  'nullable|integer',
            'modified_by'   =>  'nullable|integer',
        ],
    ];
}
