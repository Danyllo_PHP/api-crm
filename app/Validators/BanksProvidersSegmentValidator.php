<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class BanksProvidersSegmentValidator.
 *
 * @package namespace App\Validators;
 */
class BanksProvidersSegmentValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                =>  'unique|integer',
            'bank_id'           =>  'required|integer',
            'segment_id'        =>  'required|integer',
            'provider_id'       =>  'required|integer', 
            'agency'            =>  'required|max:255',
            'type'              =>  'nullable|max:255',
            'account'           =>  'required|max:255',
            'operation'         =>  'nullable|max:255',
            'account_digit'     =>  'required|string|max:2',
            'agency_digit'      =>  'required|string|max:2',
            'main'              =>  'required|integer',
            'created_by'        =>  'nullable|integer',
            'modified_by'       =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'bank_id'           =>  'integer',
            'segment_id'        =>  'integer',
            'provider_id'       =>  'integer',
            'agency'            =>  'max:255',
            'type'              =>  'nullable|max:255',
            'account'           =>  'max:255',
            'operation'         =>  'nullable|max:255',
            'account_digit'     =>  'string|max:2',
            'agency_digit'      =>  'string|max:2',
            'main'              =>  'integer',
            'created_by'        =>  'nullable|integer',
            'modified_by'       =>  'nullable|integer',
        ],
    ];
}
