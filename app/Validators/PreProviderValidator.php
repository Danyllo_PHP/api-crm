<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class PreProviderValidator.
 *
 * @package namespace App\Validators;
 */
class PreProviderValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'            =>  'unique|integer',
            'token'         =>  'required|max:255',
            'email'         =>  'required|max:255',
            'register'      =>  'required|boolean',
            'quotation_id'  =>  'required|integer',
            'expiration'    =>  'required|date_format:Y-m-d H:i:s',
            'date_register' =>  'nullable|date_format:Y-m-d H:i:s',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'token'         =>  'max:255',
            'email'         =>  'max:255',
            'register'      =>  'boolean',
            'quotation_id'  =>  'integer',
            'expiration'    =>  'date_format:Y-m-d H:i:s',
            'date_register' =>  'nullable|date_format:Y-m-d H:i:s',
        ],
    ];
}
