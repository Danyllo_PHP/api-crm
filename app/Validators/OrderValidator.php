<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class OrderValidator.
 *
 * @package namespace App\Validators;
 */
class OrderValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                                =>  'unique|integer',
            'user_id'                           =>  'nullable|integer',
            'provider_id'                       =>  'required|integer',
            'quotation_id'                      =>  'nullable|integer',
            'program_id'                        =>  'nullable|integer',
            'order_status_id'                   =>  'required|integer',
            'order_type_id'                     =>  'required|integer',
            'value'                             =>  'required|numeric',
            'value_paid'                        =>  'nullable|numeric',
            'value_payable'                     =>  'nullable|numeric',
            'price'                             =>  'required|numeric',
            'remittence_file_id'                =>  'nullable|integer',
            'observation'                       =>  'nullable|string',
            'origin'                            =>  'required|max:45',
            'system_creator'                    =>  'required|integer',
            'department'                        =>  'required|integer',
            'release_type_id'                   =>  'nullable|integer',
            'banks_providers_segment_id'        =>  'nullable|integer',
            'payment_form_id'                   =>  'required|integer',
            'receipt_sent'                      =>  'nullable|integer',
            'stocked'                           =>  'nullable|integer',
            'status_modified'                   =>  'required|date_format:Y-m-d H:i:s',
            'due_date'                          =>  'nullable|date_format:Y-m-d H:i:s',
            'session_id'                        =>  'required|max:255',
            'created_by'                        =>  'nullable|integer',
            'modified_by'                       =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'user_id'                           =>  'nullable|integer',
            'provider_id'                       =>  'integer',
            'quotation_id'                      =>  'nullable|integer',
            'program_id'                        =>  'nullable|integer',
            'order_status_id'                   =>  'integer',
            'order_type_id'                     =>  'integer',
            'value'                             =>  'numeric',
            'value_paid'                        =>  'nullable|numeric',
            'value_payable'                     =>  'nullable|numeric',
            'price'                             =>  'numeric',
            'remittence_file_id'                =>  'nullable|integer',
            'observation'                       =>  'nullable|string',
            'origin'                            =>  'max:45',
            'system_creator'                    =>  'integer',
            'department'                        =>  'integer',
            'release_type_id'                   =>  'nullable|integer',
            'banks_providers_segment_id'        =>  'nullable|integer',
            'payment_form_id'                   =>  'integer',
            'receipt_sent'                      =>  'nullable|integer',
            'stocked'                           =>  'nullable|integer',
            'status_modified'                   =>  'required|date_format:Y-m-d H:i:s',
            'due_date'                          =>  'nullable|date_format:Y-m-d H:i:s',
            'session_id'                        =>  'max:255',
            'created_by'                        =>  'nullable|integer',
            'modified_by'                       =>  'nullable|integer',
        ],
    ];
}
