<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class OrdersProgramValidator.
 *
 * @package namespace App\Validators;
 */
class OrdersProgramValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                =>  'unique|integer',
            'order_id'          =>  'nullable|integer',
            'program_id'        =>  'nullable|integer',
            'number'            =>  'required|max:255',
            'file'              =>  'nullable|max:255',
            'file_dir'          =>  'nullable|max:255',
            'access_password'   =>  'nullable|max:30',
            'created_by'        =>  'nullable|integer',
            'modified_by'       =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'order_id'          =>  'nullable|integer',
            'program_id'        =>  'nullable|integer',
            'number'            =>  'max:255',
            'file'              =>  'nullable|max:255',
            'file_dir'          =>  'nullable|max:255',
            'access_password'   =>  'nullable|max:30',
            'created_by'        =>  'nullable|integer',
            'modified_by'       =>  'nullable|integer',
        ],
    ];
}
