<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class InvoiceStatusValidator.
 *
 * @package namespace App\Validators;
 */
class InvoiceStatusValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'    =>  'unique|integer',
            'title' =>  'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'title'         =>  'required',
        ],
    ];
}
