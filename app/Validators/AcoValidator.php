<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class AcoValidator.
 *
 * @package namespace App\Validators;
 */
class AcoValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'            =>  'unique|integer',
            'parent_id'     =>  'nullable|integer',
            'model'         =>  'nullable',
            'foreign_key'   =>  'nullable|integer',
            'alias'         =>  'nullable',
            'lft'           =>  'nullable|integer',
            'rght'          =>  'nullable|integer',
            '_show'         =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'parent_id'     =>  'nullable|integer',
            'model'         =>  'nullable|string',
            'foreign_key'   =>  'nullable|integer',
            'alias'         =>  'nullable|string',
            'lft'           =>  'nullable|integer',
            'rght'          =>  'nullable|integer',
            '_show'         =>  'nullable|integer',
        ],
    ];
}
