<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class CommentValidator.
 *
 * @package namespace App\Validators;
 */
class CommentValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                =>  'unique|integer',
            'quotation_id'      =>  'required|integer',
            'body'              =>  'required|string',
            'comment_type_id'   =>  'required|integer',
            'created_by'        =>  'nullable|integer',
            'modified_by'       =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'quotation_id'      =>  'integer',
            'body'              =>  'required|string',
            'comment_type_id'   =>  'integer',
            'created_by'        =>  'nullable|integer',
            'modified_by'       =>  'nullable|integer',
        ],
    ];
}
