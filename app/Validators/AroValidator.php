<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class AroValidator.
 *
 * @package namespace App\Validators;
 */
class AroValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'               =>   'unique|integer',
            'parent_id'        =>   'nullable|integer',
            'model'            =>   'nullable|max:255',
            'foreign_key'      =>   'nullable|integer',
            'alias'            =>   'nullable|max:255',
            'lft'              =>   'nullable|integer',
            'rght'             =>   'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'parent_id'         =>  'nullable|integer',
            'model'             =>  'nullable|max:255',
            'foreign_key'       =>  'nullable|integer',
            'alias'             =>  'nullable|max:255',
            'lft'               =>  'nullable|integer',
            'rght'              =>  'nullable|integer',
        ],
    ];
}
