<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class InvoiceValidator.
 *
 * @package namespace App\Validators;
 */
class InvoiceValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                    =>  'unique|integer',
            'price'                 =>  'required|numeric',
            'invoice_status_id'     =>  'nullable|integer',
            'archived'              =>  'nullable|integer',
            'receipt_sent'          =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'price'                 =>  'numeric',
            'invoice_status_id'     =>  'nullable|integer',
            'archived'              =>  'nullable|integer',
            'receipt_sent'          =>  'nullable|integer',
        ],
    ];
}
