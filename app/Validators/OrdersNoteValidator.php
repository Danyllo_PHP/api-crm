<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class OrdersNoteValidator.
 *
 * @package namespace App\Validators;
 */
class OrdersNoteValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'            =>  'unique|integer',
            'order_id'      =>  'required|integer',
            'body'          =>  'required|string',
            'created_by'    =>  'nullable|integer',
            'modified_by'   =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'order_id'      =>  'integer',
            'body'          =>  'string',
            'created_by'    =>  'nullable|integer',
            'modified_by'   =>  'nullable|integer',
        ],
    ];
}
