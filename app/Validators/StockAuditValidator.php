<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class StockAuditValidator.
 *
 * @package namespace App\Validators;
 */
class StockAuditValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                =>  'unique|integer',
            'logged_in'         =>  'required|integer',
            'miles_api'         =>  'required|integer',
            'stock_has_more'    =>  'required|integer',
            'status'            =>  'required|integer',
            'user_id'           =>  'required|integer',
            'is_credit'         =>  'required|integer',
            'company_initials'  =>  'required|max:255',
            'stock_id'          =>  'required|integer',
            'miles_stock'       =>  'required|integer'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'logged_in'         =>  'integer',
            'miles_api'         =>  'integer',
            'stock_has_more'    =>  'integer',
            'status'            =>  'integer',
            'user_id'           =>  'integer',
            'is_credit'         =>  'integer',
            'company_initials'  =>  'max:255',
            'stock_id'          =>  'integer',
            'miles_stock'       =>  'integer',
        ],
    ];
}
