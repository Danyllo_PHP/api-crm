<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class ActionValidator.
 *
 * @package namespace App\Validators;
 */
class ActionValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'            =>  'unique|integer',
            'parent_id'     =>  'nullable|integer',
            'title'         =>  'required|max:30',
            'lft'           =>  'nullable|integer',
            'rght'          =>  'nullable|integer',
            'created_by'    =>  'nullable|integer',
            'modified_by'   =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'parent_id'     =>  'nullable|integer',
            'title'         =>  'max:30',
            'lft'           =>  'nullable|integer',
            'rght'          =>  'nullable|integer',
            'created_by'    =>  'nullable|integer',
            'modified_by'   =>  'nullable|integer',
        ],
    ];
}
