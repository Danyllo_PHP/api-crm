<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class ProvidersStatusValidator.
 *
 * @package namespace App\Validators;
 */
class ProvidersStatusValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                =>  'unique|integer',
            'title'             =>  'required|max:30',
            'provider_create'   =>  'required|boolean',
            'has_comment'       =>  'required|boolean',
            'class'             =>  'required|max:255',
            'created_by'        =>  'nullable|integer',
            'modified_by'       =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'title'             =>  'max:30',
            'provider_create'   =>  'boolean',
            'has_comment'       =>  'boolean',
            'class'             =>  'max:255',
            'created_by'        =>  'nullable|integer',
            'modified_by'       =>  'nullable|integer',
        ],
    ];
}
