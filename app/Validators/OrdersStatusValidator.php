<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class OrdersStatusValidator.
 *
 * @package namespace App\Validators;
 */
class OrdersStatusValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'            =>  'unique|integer',
            'title'         =>  'required|max:30',
            'order_create'  =>  'required|boolean',
            'parent_id'     =>  'nullable|integer',
            'created_by'    =>  'nullable|integer',
            'modified_by'   =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'title'         =>  'max:30',
            'order_create'  =>  'boolean',
            'parent_id'     =>  'nullable|integer',
            'created_by'    =>  'nullable|integer',
            'modified_by'   =>  'nullable|integer',
        ],
    ];
}
