<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class OrdersStatusProvidersStatus1Validator.
 *
 * @package namespace App\Validators;
 */
class OrdersStatusProvidersStatus1Validator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                    =>  'unique|integer',
            'order_status_id'       =>  'required|integer',
            'provider_status_id'    =>  'required|integer',
            'created_by'            =>  'nullable|integer',
            'modified_by'           =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'order_status_id'       =>  'integer',
            'provider_status_id'    =>  'integer',
            'created_by'            =>  'nullable|integer',
            'modified_by'           =>  'nullable|integer',
        ],
    ];
}
