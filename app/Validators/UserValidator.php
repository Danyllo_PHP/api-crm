<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class UserValidator.
 *
 * @package namespace App\Validators;
 */
class UserValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                =>  'unique|integer',
            'name'              =>  'required|max:30',
            'email'             =>  'required|email',
            'username'          =>  'required|max:30',
            'password'          =>  'required|max:100',
            'group_id'          =>  'nullable|integer',
            'branch'            =>  'nullable|max:255',
            'photo'             =>  'nullable|max:255',
            'photo_dir'         =>  'nullable|max:255',
            'active'            =>  'required|boolean',
            'activity_count'    =>  'required|integer',
            'comment_count'     =>  'required|integer',
            'quotation_count'   =>  'required|integer',
            'task_count'        =>  'required|integer',
            'group_manager'     =>  'required|boolean',
            'last_access'       =>  'nullable|date_format:Y-m-d H:i:s',
            'miles_goals'       =>  'nullable|numeric',
            'email_verified_at' =>  'nullable|date_format:Y-m-d H:i:s',
            'created_by'        =>  'nullable|integer',
            'modified_by'       =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name'              =>  'max:30',
            'email'             =>  'email',
            'username'          =>  'max:30',
            'password'          =>  'max:30',
            'group_id'          =>  'nullable|integer',
            'branch'            =>  'nullable|max:255',
            'photo'             =>  'nullable|max:255',
            'photo_dir'         =>  'nullable|max:255',
            'active'            =>  'boolean',
            'activity_count'    =>  'integer',
            'comment_count'     =>  'integer',
            'quotation_count'   =>  'integer',
            'task_count'        =>  'integer',
            'group_manager'     =>  'boolean',
            'last_access'       =>  'nullable|date_format:Y-m-d H:i:s',
            'miles_goals'       =>  'nullable|numeric',
            'email_verified_at' =>  'nullable|date_format:Y-m-d H:i:s',
            'created_by'        =>  'nullable|integer',
            'modified_by'       =>  'nullable|integer',
        ],
    ];
}
