<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class ProposalValidator.
 *
 * @package namespace App\Validators;
 */
class ProposalValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                =>  'unique|integer',
            'quotation_id'      =>  'required|integer',
            'dir'               =>  'required|max:255',
            'file'              =>  'required|max:255',
            'seen'              =>  'nullable|date_format:Y-m-d H:i:s',
            'created_by'        =>  'nullable|integer',
            'modified_by'       =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'quotation_id'      =>  'integer',
            'dir'               =>  'max:255',
            'file'              =>  'max:255',
            'seen'              =>  'nullable|date_format:Y-m-d H:i:s',
            'created_by'        =>  'nullable|integer',
            'modified_by'       =>  'nullable|integer',
        ],
    ];
}
