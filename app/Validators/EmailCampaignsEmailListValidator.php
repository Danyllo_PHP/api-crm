<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class EmailCampaignsEmailListValidator.
 *
 * @package namespace App\Validators;
 */
class EmailCampaignsEmailListValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                    =>  'unique|integer',
            'sent'                  =>  'nullable',
            'schedule'              =>  'required|date_format:Y-m-d H:i:s',
            'email_campaign_id'     =>  'required|integer',
            'email_list_id'         =>  'required|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'sent'                  =>  'nullable',
            'schedule'              =>  'date_format:Y-m-d H:i:s',
            'email_campaign_id'     =>  'integer',
            'email_list_id'         =>  'integer',
        ],
    ];
}
