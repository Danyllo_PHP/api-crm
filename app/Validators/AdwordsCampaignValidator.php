<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class AdwordsCampaignValidator.
 *
 * @package namespace App\Validators;
 */
class AdwordsCampaignValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                =>  'unique|integer',
            'name'              =>  'required|max:30',
            'code'              =>  'required|max:255',
            'count_quotations'  =>  'required|integer',
            'description'       =>  'nullable|max:255'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name'              =>  'max:30',
            'code'              =>  'max:255',
            'count_quotations'  =>  'integer',
            'description'       =>  'nullable|max:255',
        ],
    ];
}
