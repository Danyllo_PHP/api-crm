<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class MessagesUserValidator.
 *
 * @package namespace App\Validators;
 */
class MessagesUserValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'            =>  'unique|integer',
            'message_id'    =>  'required|integer',
            'user_id'       =>  'required|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'message_id'    =>  'required|integer',
            'user_id'       =>  'required|integer',
        ],
    ];
}
