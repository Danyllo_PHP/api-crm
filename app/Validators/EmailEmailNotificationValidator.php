<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class EmailEmailNotificationValidator.
 *
 * @package namespace App\Validators;
 */
class EmailEmailNotificationValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'        =>  'unique|integer',
            'email'     =>  'required|max:255',
            'event'     =>  'required|max:255',
            'source'    =>  'in:campaign,quotation',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'email'         =>  'max:255',
            'event'         =>  'max:255',
            'source'        =>  'in:campaign,quotation',
        ],
    ];
}
