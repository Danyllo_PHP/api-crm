<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class QuotationStatusValidator.
 *
 * @package namespace App\Validators;
 */
class QuotationStatusValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'                =>  'unique|integer',
            'title'             =>  'required|max:30',
            'quotation_create'  =>  'required|boolean',
            'has_comment'       =>  'required|boolean',
            'class'             =>  'required|max255',
            'created_by'        =>  'nullable|integer',
            'modified_by'       =>  'nullable|integer',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'title'             =>  'max:30',
            'quotation_create'  =>  'boolean',
            'has_comment'       =>  'boolean',
            'class'             =>  'max255',
            'created_by'        =>  'nullable|integer',
            'modified_by'       =>  'nullable|integer',
        ],
    ];
}
