<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class EntitiesReleaseValidator.
 *
 * @package namespace App\Validators;
 */
class EntitiesReleaseValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'            =>  'unique|integer',
            'parent_id'     =>  'required|integer',
            'parent_name'   =>  'required|max:255',
            'user_id'       =>  'required|integer',
            'approved'      =>  'required|boolean',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'parent_id'     =>  'integer',
            'parent_name'   =>  'max:255',
            'user_id'       =>  'integer',
            'approved'      =>  'boolean',
        ],
    ];
}
