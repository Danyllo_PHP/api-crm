<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class ArosAcoValidator.
 *
 * @package namespace App\Validators;
 */
class ArosAcoValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id'        =>  'unique|integer',
            'aro_id'    =>  'required|integer',
            'aco_id'    =>  'required|integer',
            '_create'   =>  'required|max:2',
            '_read'     =>  'required|max:2',
            '_update'   =>  'required|max:2',
            '_delete'   =>  'required|max:2'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'aro_id'        =>  'integer',
            'aco_id'        =>  'integer',
            '_create'       =>  'max:2',
            '_read'         =>  'max:2',
            '_update'       =>  'max:2',
            '_delete'       =>  'max:2',
        ],
    ];
}
