<?php

namespace App\Services;


use App\Repositories\UserRepository;
use App\Services\Traits\CrudMethods;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;


/**
 * Class BankAccountService
 * @package App\Services
 */
class UserService extends AppService
{
    use CrudMethods;
    
    /**
     * SendsPasswordResetEmails
     */
    use SendsPasswordResetEmails {
        sendResetLinkEmail as public reset;
    }

    /**
     * @var UserRepository
     */
    protected $repository;

    /**
     * UserService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository  = $repository;
    }

    /**
     * @param $data
     * @param bool $skipPresenter
     * @return \Illuminate\Http\JsonResponse
     */
    public function create($data, $skipPresenter = false)
    {
        $data['password'] = bcrypt($data['password'] ?? '');
        $data['email'] = strtolower($data['email'] ?? '');

        $response = $skipPresenter ? $this->repository->skipPresenter()->create($data) : $this->repository->create($data);

        $response = [
            'error' => $response['error'] ?? false,
            'user' => $response['data'] ?? [],
            'message' => !isset($response['error']) ? "Usuário criado!" : "Falha na criação do usuário",
        ];

        return response()->json($response, 200);

    }

}
