<?php

namespace App\Services;

use App\Repositories\ProviderRepository;
use App\Repositories\ProvidersStatusRepository;
use App\Services\Traits\CrudMethods;

/**
 * Class ProviderService
 * @package App\Services
 */
class ProviderService extends AppService
{
    use CrudMethods{
        all as public processAll;
    }

    /**
     * @var ProviderRepository
     */
    protected $repository;

    /**
     * @var ProvidersStatusRepository
     */
    protected $providersStatus;

    /**
     * ProviderService constructor.
     * @param ProviderRepository $repository
     * @param ProvidersStatusRepository $providersStatus
     */
    public function __construct(ProviderRepository $repository, ProvidersStatusRepository $providersStatus)
    {
        $this->repository      = $repository;
        $this->providersStatus = $providersStatus;
    }

    /**
     * @return mixed
     */
    public function reportProvidersStatus()
    {
        $this->repository
            ->resetCriteria()
            ->pushCriteria(app('App\Criterias\FilterByStatusCriteria'))
            ->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        return $this->processAll(1);
    }

}