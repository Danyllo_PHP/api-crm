<?php

namespace App\Services;

use App\Repositories\ActionsAcoRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class ActionsAcoService
 * @package App\Services
 */
class ActionsAcoService extends AppService
{
    use CrudMethods;

    /**
     * @var ActionsAcoRepository
     */
    protected $repository;

    /**
     * ActionsAcosService constructor.
     *
     * @param ActionsAcoRepository $repository
     */
    public function __construct(ActionsAcoRepository $repository)
    {
        $this->repository = $repository;
    }
}
