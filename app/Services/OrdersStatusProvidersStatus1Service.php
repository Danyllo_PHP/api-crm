<?php

namespace App\Services;

use App\Repositories\OrdersStatusProvidersStatus1Repository;
use App\Services\Traits\CrudMethods;


/**
 * Class OrdersStatusProvidersStatus1Service
 * @package App\Services
 */
class OrdersStatusProvidersStatus1Service extends AppService
{
    use CrudMethods;

    /**
     * @var OrdersStatusProvidersStatus1Repository
     */
    protected $repository;

    /**
     * OrdersStatusProvidersStatus1Service constructor.
     * @param OrdersStatusProvidersStatus1Repository $repository
     */
    public function __construct(OrdersStatusProvidersStatus1Repository $repository)
    {
        $this->repository  = $repository;
    }

   
}