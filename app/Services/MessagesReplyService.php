<?php

namespace App\Services;

use App\Repositories\MessagesReplyRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class MessagesReplyService
 * @package App\Services
 */
class MessagesReplyService extends AppService
{
    use CrudMethods;

    /**
     * @var MessagesReplyRepository
     */
    protected $repository;

    /**
     * MessagesReplyService constructor.
     * @param MessagesReplyRepository $repository
     */
    public function __construct(MessagesReplyRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}