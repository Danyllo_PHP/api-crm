<?php

namespace App\Services;

use App\Repositories\RemittenceConfigRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class RemittenceConfigService
 * @package App\Services
 */
class RemittenceConfigService extends AppService
{
    use CrudMethods;

    /**
     * @var RemittenceConfigRepository
     */
    protected $repository;

    /**
     * RemittenceConfigService constructor.
     * @param RemittenceConfigRepository $repository
     */
    public function __construct(RemittenceConfigRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}