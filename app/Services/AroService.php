<?php

namespace App\Services;

use App\Repositories\AroRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class AroService
 * @package App\Services
 */
class AroService extends AppService
{
    use CrudMethods;

    /**
     * @var AroRepository
     */
    protected $repository;

    /**
     * AroService constructor.
     * @param AroRepository $repository
     */
    public function __construct(AroRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}