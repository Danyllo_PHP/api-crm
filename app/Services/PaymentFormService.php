<?php

namespace App\Services;

use App\Repositories\PaymentFormRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class PaymentFormService
 * @package App\Services
 */
class PaymentFormService extends AppService
{
    use CrudMethods;

    /**
     * @var PaymentFormRepository
     */
    protected $repository;

    /**
     * PaymentFormService constructor.
     * @param PaymentFormRepository $repository
     */
    public function __construct(PaymentFormRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}