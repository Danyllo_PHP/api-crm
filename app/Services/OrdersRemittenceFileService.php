<?php

namespace App\Services;

use App\Repositories\OrdersRemittenceFileRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class OrdersRemittenceFileService
 * @package App\Services
 */
class OrdersRemittenceFileService extends AppService
{
    use CrudMethods;

    /**
     * @var OrdersRemittenceFileRepository
     */
    protected $repository;

    /**
     * OrdersRemittenceFileService constructor.
     * @param OrdersRemittenceFileRepository $repository
     */
    public function __construct(OrdersRemittenceFileRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}