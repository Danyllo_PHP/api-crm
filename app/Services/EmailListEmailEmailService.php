<?php

namespace App\Services;

use App\Repositories\EmailListEmailEmailRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class EmailListEmailEmailService
 * @package App\Services
 */
class EmailListEmailEmailService extends AppService
{
    use CrudMethods;

    /**
     * @var EmailListEmailEmailRepository
     */
    protected $repository;

    /**
     * EmailListEmailEmailService constructor.
     * @param EmailListEmailEmailRepository $repository
     */
    public function __construct(EmailListEmailEmailRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}