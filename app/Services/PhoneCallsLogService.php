<?php

namespace App\Services;

use App\Repositories\PhoneCallsLogRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class PhoneCallsLogService
 * @package App\Services
 */
class PhoneCallsLogService extends AppService
{
    use CrudMethods;

    /**
     * @var PhoneCallsLogRepository
     */
    protected $repository;

    /**
     * PhoneCallsLogService constructor.
     * @param PhoneCallsLogRepository $repository
     */
    public function __construct(PhoneCallsLogRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}