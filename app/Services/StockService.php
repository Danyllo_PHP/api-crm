<?php

namespace App\Services;

use App\Repositories\StockRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class StockService
 * @package App\Services
 */
class StockService extends AppService
{
    use CrudMethods;

    /**
     * @var StockRepository
     */
    protected $repository;

    /**
     * StockService constructor.
     * @param StockRepository $repository
     */
    public function __construct(StockRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}