<?php

namespace App\Services;

use App\Repositories\ProgramsQuotationRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class ProgramsQuotationService
 * @package App\Services
 */
class ProgramsQuotationService extends AppService
{
    use CrudMethods;

    /**
     * @var ProgramsQuotationRepository
     */
    protected $repository;

    /**
     * AcoService constructor.
     * @param ProgramsQuotationRepository $repository
     */
    public function __construct(ProgramsQuotationRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}