<?php

namespace App\Services;

use App\Repositories\OrderRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class OrderService
 * @package App\Services
 */
class OrderService extends AppService
{
    use CrudMethods;

    /**
     * @var OrderRepository
     */
    protected $repository;

    /**
     * OrderService constructor.
     * @param OrderRepository $repository
     */
    public function __construct(OrderRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}