<?php

namespace App\Services;

use App\Repositories\EmailTransportRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class EmailTransportService
 * @package App\Services
 */
class EmailTransportService extends AppService
{
    use CrudMethods;

    /**
     * @var EmailTransportRepository
     */
    protected $repository;

    /**
     * EmailTransportService constructor.
     * @param EmailTransportRepository $repository
     */
    public function __construct(EmailTransportRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}