<?php

namespace App\Services;

use App\Repositories\PendingEditionRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class PendingEditionService
 * @package App\Services
 */
class PendingEditionService extends AppService
{
    use CrudMethods;

    /**
     * @var PendingEditionRepository
     */
    protected $repository;

    /**
     * PendingEditionService constructor.
     * @param PendingEditionRepository $repository
     */
    public function __construct(PendingEditionRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}