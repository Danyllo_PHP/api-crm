<?php

namespace App\Services;

use App\Repositories\PaymentsStatusRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class PaymentsStatusService
 * @package App\Services
 */
class PaymentsStatusService extends AppService
{
    use CrudMethods;

    /**
     * @var PaymentsStatusRepository
     */
    protected $repository;

    /**
     * PaymentsStatusService constructor.
     * @param PaymentsStatusRepository $repository
     */
    public function __construct(PaymentsStatusRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}