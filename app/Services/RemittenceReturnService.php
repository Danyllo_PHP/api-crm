<?php

namespace App\Services;

use App\Repositories\RemittenceReturnRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class RemittenceReturnService
 * @package App\Services
 */
class RemittenceReturnService extends AppService
{
    use CrudMethods;

    /**
     * @var RemittenceReturnRepository
     */
    protected $repository;

    /**
     * RemittenceReturnService constructor.
     * @param RemittenceReturnRepository $repository
     */
    public function __construct(RemittenceReturnRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}