<?php

namespace App\Services;


use App\Repositories\AcoRepository;
use App\Repositories\TaskTypeRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class TaskTypeService
 * @package App\Services
 */
class TaskTypeService extends AppService
{
    use CrudMethods;

    /**
     * @var TaskTypeRepository
     */
    protected $repository;

    /**
     * TaskTypeService constructor.
     * @param TaskTypeRepository $repository
     */
    public function __construct(TaskTypeRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}