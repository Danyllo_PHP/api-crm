<?php

namespace App\Services;

use App\Repositories\CommentTypeRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class CommentTypeService
 * @package App\Services
 */
class CommentTypeService extends AppService
{
    use CrudMethods;

    /**
     * @var CommentTypeRepository
     */
    protected $repository;

    /**
     * CommentTypeService constructor.
     * @param CommentTypeRepository $repository
     */
    public function __construct(CommentTypeRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}