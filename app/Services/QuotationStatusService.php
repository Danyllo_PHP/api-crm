<?php

namespace App\Services;

use App\Repositories\QuotationStatusRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class QuotationStatusService
 * @package App\Services
 */
class QuotationStatusService extends AppService
{
    use CrudMethods;

    /**
     * @var QuotationStatusRepository
     */
    protected $repository;

    /**
     * QuotationStatusService constructor.
     * @param QuotationStatusRepository $repository
     */
    public function __construct(QuotationStatusRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}