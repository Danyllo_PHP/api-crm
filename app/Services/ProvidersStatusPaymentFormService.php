<?php

namespace App\Services;

use App\Repositories\ProvidersStatusPaymentFormRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class ProvidersStatusPaymentFormService
 * @package App\Services
 */
class ProvidersStatusPaymentFormService extends AppService
{
    use CrudMethods;

    /**
     * @var ProvidersStatusPaymentFormRepository
     */
    protected $repository;

    /**
     * ProvidersStatusPaymentFormService constructor.
     * @param ProvidersStatusPaymentFormRepository $repository
     */
    public function __construct(ProvidersStatusPaymentFormRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}