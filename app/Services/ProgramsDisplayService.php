<?php

namespace App\Services;

use App\Repositories\ProgramsDisplayRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class ProgramsDisplayService
 * @package App\Services
 */
class ProgramsDisplayService extends AppService
{
    use CrudMethods;

    /**
     * @var ProgramsDisplayRepository
     */
    protected $repository;

    /**
     * ProgramsDisplayService constructor.
     * @param ProgramsDisplayRepository $repository
     */
    public function __construct(ProgramsDisplayRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}