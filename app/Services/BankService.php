<?php

namespace App\Services;

use App\Repositories\BankRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class BankService
 * @package App\Services
 */
class BankService extends AppService
{
    use CrudMethods;

    /**
     * @var BankRepository
     */
    protected $repository;

    /**
     * BankService constructor.
     * @param BankRepository $repository
     */
    public function __construct(BankRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}