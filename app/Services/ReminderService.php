<?php

namespace App\Services;

use App\Repositories\ReminderRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class ReminderService
 * @package App\Services
 */
class ReminderService extends AppService
{
    use CrudMethods;

    /**
     * @var ReminderRepository
     */
    protected $repository;

    /**
     * ReminderService constructor.
     * @param ReminderRepository $repository
     */
    public function __construct(ReminderRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}