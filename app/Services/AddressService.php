<?php

namespace App\Services;

use App\Repositories\AddressRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class AddressService
 * @package App\Services
 */
class AddressService extends AppService
{
    use CrudMethods;

    /**
     * @var AddressRepository
     */
    protected $repository;

    /**
     * AddressesService constructor.
     *
     * @param AddressRepository $repository
     */
    public function __construct(AddressRepository $repository)
    {
        $this->repository = $repository;
    }
}
