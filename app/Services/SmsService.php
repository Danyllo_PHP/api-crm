<?php

namespace App\Services;

use App\Repositories\SmsRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class SmsService
 * @package App\Services
 */
class SmsService extends AppService
{
    use CrudMethods;

    /**
     * @var SmsRepository
     */
    protected $repository;

    /**
     * SmsService constructor.
     * @param SmsRepository $repository
     */
    public function __construct(SmsRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}