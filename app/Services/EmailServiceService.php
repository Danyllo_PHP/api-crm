<?php

namespace App\Services;

use App\Repositories\EmailServiceRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class EmailServiceService
 * @package App\Services
 */
class EmailServiceService extends AppService
{
    use CrudMethods;

    /**
     * @var EmailServiceRepository
     */
    protected $repository;

    /**
     * EmailServiceService constructor.
     * @param EmailServiceRepository $repository
     */
    public function __construct(EmailServiceRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}