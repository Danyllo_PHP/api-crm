<?php

namespace App\Services;

use App\Repositories\EmailEmailsRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class EmailEmailService
 * @package App\Services
 */
class EmailEmailService extends AppService
{
    use CrudMethods;

    /**
     * @var EmailEmailsRepository
     */
    protected $repository;

    /**
     * EmailEmailService constructor.
     * @param EmailEmailsRepository $repository
     */
    public function __construct(EmailEmailsRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}