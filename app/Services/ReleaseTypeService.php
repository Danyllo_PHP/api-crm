<?php

namespace App\Services;

use App\Repositories\ReleaseTypeRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class ReleaseTypeService
 * @package App\Services
 */
class ReleaseTypeService extends AppService
{
    use CrudMethods;

    /**
     * @var ReleaseTypeRepository
     */
    protected $repository;

    /**
     * ReleaseTypeService constructor.
     * @param ReleaseTypeRepository $repository
     */
    public function __construct(ReleaseTypeRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}