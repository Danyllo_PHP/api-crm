<?php

namespace App\Services;

use App\Repositories\FidelityRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class FidelityService
 * @package App\Services
 */
class FidelityService extends AppService
{
    use CrudMethods;

    /**
     * @var FidelityRepository
     */
    protected $repository;

    /**
     * FidelityService constructor.
     * @param FidelityRepository $repository
     */
    public function __construct(FidelityRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}