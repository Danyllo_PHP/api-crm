<?php

namespace App\Services;

use App\Repositories\StockLogRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class StockLogService
 * @package App\Services
 */
class StockLogService extends AppService
{
    use CrudMethods;

    /**
     * @var StockLogRepository
     */
    protected $repository;

    /**
     * StockLogService constructor.
     * @param StockLogRepository $repository
     */
    public function __construct(StockLogRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}