<?php

namespace App\Services;

use App\Repositories\MessagesUserRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class MessagesUserService
 * @package App\Services
 */
class MessagesUserService extends AppService
{
    use CrudMethods;

    /**
     * @var MessagesUserRepository
     */
    protected $repository;

    /**
     * MessagesUserService constructor.
     * @param MessagesUserRepository $repository
     */
    public function __construct(MessagesUserRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}