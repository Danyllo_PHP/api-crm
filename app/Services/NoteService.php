<?php

namespace App\Services;

use App\Repositories\NoteRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class NoteService
 * @package App\Services
 */
class NoteService extends AppService
{
    use CrudMethods;

    /**
     * @var NoteRepository
     */
    protected $repository;

    /**
     * NoteService constructor.
     * @param NoteRepository $repository
     */
    public function __construct(NoteRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}