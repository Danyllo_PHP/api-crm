<?php

namespace App\Services;

use App\Repositories\PhoneCallRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class PhoneCallService
 * @package App\Services
 */
class PhoneCallService extends AppService
{
    use CrudMethods;

    /**
     * @var PhoneCallRepository
     */
    protected $repository;

    /**
     * PhoneCallService constructor.
     * @param PhoneCallRepository $repository
     */
    public function __construct(PhoneCallRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}