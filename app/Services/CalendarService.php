<?php

namespace App\Services;

use App\Repositories\CalendarRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class CalendarService
 * @package App\Services
 */
class CalendarService extends AppService
{
    use CrudMethods;

    /**
     * @var CalendarRepository
     */
    protected $repository;

    /**
     * CalendarService constructor.
     * @param CalendarRepository $repository
     */
    public function __construct(CalendarRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}