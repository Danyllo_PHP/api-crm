<?php

namespace App\Services;

use App\Repositories\RemittenceReturnOrderRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class RemittenceReturnOrderService
 * @package App\Services
 */
class RemittenceReturnOrderService extends AppService
{
    use CrudMethods;

    /**
     * @var RemittenceReturnOrderRepository
     */
    protected $repository;

    /**
     * RemittenceReturnOrderService constructor.
     * @param RemittenceReturnOrderRepository $repository
     */
    public function __construct(RemittenceReturnOrderRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}