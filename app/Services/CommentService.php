<?php

namespace App\Services;

use App\Repositories\CommentRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class CommentService
 * @package App\Services
 */
class CommentService extends AppService
{
    use CrudMethods;

    /**
     * @var CommentRepository
     */
    protected $repository;

    /**
     * CommentService constructor.
     * @param CommentRepository $repository
     */
    public function __construct(CommentRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}