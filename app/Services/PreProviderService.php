<?php

namespace App\Services;

use App\Repositories\PreProviderRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class PreProviderService
 * @package App\Services
 */
class PreProviderService extends AppService
{
    use CrudMethods;

    /**
     * @var PreProviderRepository
     */
    protected $repository;

    /**
     * PreProviderService constructor.
     * @param PreProviderRepository $repository
     */
    public function __construct(PreProviderRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}