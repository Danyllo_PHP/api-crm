<?php

namespace App\Services;

use App\Repositories\ActionRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class ActionService
 * @package App\Services
 */
class ActionService extends AppService
{
    use CrudMethods;

    /**
     * @var ActionRepository
     */
    protected $repository;


    /**
     * ActionsService constructor.
     *
     * @param ActionRepository $repository
     */
    public function __construct(ActionRepository $repository)
    {
        $this->repository = $repository;
    }
}
