<?php

namespace App\Services;

use App\Repositories\EmailListRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class EmailListService
 * @package App\Services
 */
class EmailListService extends AppService
{
    use CrudMethods;

    /**
     * @var EmailListRepository
     */
    protected $repository;

    /**
     * EmailListService constructor.
     * @param EmailListRepository $repository
     */
    public function __construct(EmailListRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}