<?php

namespace App\Services;

use App\Repositories\PaymentRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class PaymentService
 * @package App\Services
 */
class PaymentService extends AppService
{
    use CrudMethods;

    /**
     * @var PaymentRepository
     */
    protected $repository;

    /**
     * PaymentService constructor.
     * @param PaymentRepository $repository
     */
    public function __construct(PaymentRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}