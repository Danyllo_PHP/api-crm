<?php

namespace App\Services;

use App\Repositories\StockRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class AcoService
 * @package App\Services
 */
class StockProviderService extends AppService
{
    use CrudMethods;

    /**
     * @var StockRepository
     */
    protected $repository;

    /**
     * AcoService constructor.
     * @param StockRepository $repository
     */
    public function __construct(StockRepository $repository)
    {
        $this->repository  = $repository;
    }
}
