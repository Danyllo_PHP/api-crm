<?php

namespace App\Services;

use App\Repositories\MessageRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class MessageService
 * @package App\Services
 */
class MessageService extends AppService
{
    use CrudMethods;

    /**
     * @var MessageRepository
     */
    protected $repository;

    /**
     * MessageService constructor.
     * @param MessageRepository $repository
     */
    public function __construct(MessageRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}