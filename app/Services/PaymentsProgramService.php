<?php

namespace App\Services;

use App\Repositories\PaymentsProgramRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class PaymentsProgramService
 * @package App\Services
 */
class PaymentsProgramService extends AppService
{
    use CrudMethods;

    /**
     * @var PaymentsProgramRepository
     */
    protected $repository;

    /**
     * PaymentsProgramService constructor.
     * @param PaymentsProgramRepository $repository
     */
    public function __construct(PaymentsProgramRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}