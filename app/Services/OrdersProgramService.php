<?php

namespace App\Services;

use App\Repositories\OrdersProgramRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class OrdersProgramService
 * @package App\Services
 */
class OrdersProgramService extends AppService
{
    use CrudMethods;

    /**
     * @var OrdersProgramRepository
     */
    protected $repository;

    /**
     * OrdersProgramService constructor.
     * @param OrdersProgramRepository $repository
     */
    public function __construct(OrdersProgramRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}