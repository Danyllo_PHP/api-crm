<?php

namespace App\Services;

use App\Repositories\MailBoxRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class MailBoxService
 * @package App\Services
 */
class MailBoxService extends AppService
{
    use CrudMethods;

    /**
     * @var MailBoxRepository
     */
    protected $repository;

    /**
     * MailBoxService constructor.
     * @param MailBoxRepository $repository
     */
    public function __construct(MailBoxRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}