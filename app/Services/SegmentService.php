<?php

namespace App\Services;

use App\Repositories\SegmentRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class SegmentService
 * @package App\Services
 */
class SegmentService extends AppService
{
    use CrudMethods;

    /**
     * @var SegmentRepository
     */
    protected $repository;

    /**
     * SegmentService constructor.
     * @param SegmentRepository $repository
     */
    public function __construct(SegmentRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}