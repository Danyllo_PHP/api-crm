<?php

namespace App\Services;

use App\Repositories\EmailTemplateRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class EmailTemplateService
 * @package App\Services
 */
class EmailTemplateService extends AppService
{
    use CrudMethods;

    /**
     * @var EmailTemplateRepository
     */
    protected $repository;

    /**
     * EmailTemplateService constructor.
     * @param EmailTemplateRepository $repository
     */
    public function __construct(EmailTemplateRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}