<?php

namespace App\Services;

use App\Repositories\AcoRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class AcoService
 * @package App\Services
 */
class AcoService extends AppService
{
    use CrudMethods;

    /**
     * @var AcoRepository
     */
    protected $repository;

    /**
     * AcoService constructor.
     * @param AcoRepository $repository
     */
    public function __construct(AcoRepository $repository)
    {
        $this->repository  = $repository;
    }
}
