<?php

namespace App\Services;

use App\Repositories\TaskRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class TaskService
 * @package App\Services
 */
class TaskService extends AppService
{
    use CrudMethods;

    /**
     * @var TaskRepository
     */
    protected $repository;

    /**
     * TaskService constructor.
     * @param TaskRepository $repository
     */
    public function __construct(TaskRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}