<?php

namespace App\Services;

use App\Repositories\QuotationRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class QuotationService
 * @package App\Services
 */
class QuotationService extends AppService
{
    use CrudMethods;

    /**
     * @var QuotationRepository
     */
    protected $repository;

    /**
     * QuotationService constructor.
     * @param QuotationRepository $repository
     */
    public function __construct(QuotationRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}