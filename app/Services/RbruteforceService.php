<?php

namespace App\Services;

use App\Repositories\RbruteforceRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class RbruteforceService
 * @package App\Services
 */
class RbruteforceService extends AppService
{
    use CrudMethods;

    /**
     * @var RbruteforceRepository
     */
    protected $repository;

    /**
     * RbruteforceService constructor.
     * @param RbruteforceRepository $repository
     */
    public function __construct(RbruteforceRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}