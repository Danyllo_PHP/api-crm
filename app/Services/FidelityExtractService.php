<?php

namespace App\Services;

use App\Repositories\FidelityExtractRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class FidelityExtractService
 * @package App\Services
 */
class FidelityExtractService extends AppService
{
    use CrudMethods;

    /**
     * @var FidelityExtractRepository
     */
    protected $repository;

    /**
     * FidelityExtractService constructor.
     * @param FidelityExtractRepository $repository
     */
    public function __construct(FidelityExtractRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}