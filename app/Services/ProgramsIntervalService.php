<?php

namespace App\Services;

use App\Repositories\ProgramsIntervalRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class ProgramsIntervalService
 * @package App\Services
 */
class ProgramsIntervalService extends AppService
{
    use CrudMethods;

    /**
     * @var ProgramsIntervalRepository
     */
    protected $repository;

    /**
     * ProgramsIntervalService constructor.
     * @param ProgramsIntervalRepository $repository
     */
    public function __construct(ProgramsIntervalRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}