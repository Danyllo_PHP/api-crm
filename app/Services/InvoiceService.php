<?php

namespace App\Services;

use App\Repositories\InvoiceRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class InvoiceService
 * @package App\Services
 */
class InvoiceService extends AppService
{
    use CrudMethods;

    /**
     * @var InvoiceRepository
     */
    protected $repository;

    /**
     * InvoiceService constructor.
     * @param InvoiceRepository $repository
     */
    public function __construct(InvoiceRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}