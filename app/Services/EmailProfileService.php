<?php

namespace App\Services;

use App\Repositories\EmailProfileRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class EmailProfileService
 * @package App\Services
 */
class EmailProfileService extends AppService
{
    use CrudMethods;

    /**
     * @var EmailProfileRepository
     */
    protected $repository;

    /**
     * EmailProfileService constructor.
     * @param EmailProfileRepository $repository
     */
    public function __construct(EmailProfileRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}