<?php

namespace App\Services;

use App\Repositories\StockAuditRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class StockAuditService
 * @package App\Services
 */
class StockAuditService extends AppService
{
    use CrudMethods;

    /**
     * @var StockAuditRepository
     */
    protected $repository;

    /**
     * StockAuditService constructor.
     * @param StockAuditRepository $repository
     */
    public function __construct(StockAuditRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}