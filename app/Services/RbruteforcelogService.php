<?php

namespace App\Services;

use App\Repositories\RbruteforcelogRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class RbruteforcelogService
 * @package App\Services
 */
class RbruteforcelogService extends AppService
{
    use CrudMethods;

    /**
     * @var RbruteforcelogRepository
     */
    protected $repository;

    /**
     * AcoService constructor.
     * @param RbruteforcelogRepository $repository
     */
    public function __construct(RbruteforcelogRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}