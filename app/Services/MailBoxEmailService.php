<?php

namespace App\Services;

use App\Repositories\MailBoxEmailRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class MailBoxEmailService
 * @package App\Services
 */
class MailBoxEmailService extends AppService
{
    use CrudMethods;

    /**
     * @var MailBoxEmailRepository
     */
    protected $repository;

    /**
     * MailBoxEmailService constructor.
     * @param MailBoxEmailRepository $repository
     */
    public function __construct(MailBoxEmailRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}