<?php

namespace App\Services;

use App\Repositories\BanksProvidersSegmentRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class BanksProvidersSegmentService
 * @package App\Services
 */
class BanksProvidersSegmentService extends AppService
{
    use CrudMethods;

    /**
     * @var BanksProvidersSegmentRepository
     */
    protected $repository;

    /**
     * BanksProvidersSegmentService constructor.
     * @param BanksProvidersSegmentRepository $repository
     */
    public function __construct(BanksProvidersSegmentRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}