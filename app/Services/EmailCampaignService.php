<?php

namespace App\Services;

use App\Repositories\EmailCampaignRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class EmailCampaignService
 * @package App\Services
 */
class EmailCampaignService extends AppService
{
    use CrudMethods;

    /**
     * @var EmailCampaignRepository
     */
    protected $repository;

    /**
     * EmailCampaignService constructor.
     * @param EmailCampaignRepository $repository
     */
    public function __construct(EmailCampaignRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}