<?php

namespace App\Services;

use App\Repositories\EntitiesReleaseRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class EntitiesReleaseService
 * @package App\Services
 */
class EntitiesReleaseService extends AppService
{
    use CrudMethods;

    /**
     * @var EntitiesReleaseRepository
     */
    protected $repository;

    /**
     * EntitiesReleaseService constructor.
     * @param EntitiesReleaseRepository $repository
     */
    public function __construct(EntitiesReleaseRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}