<?php

namespace App\Services;


use App\Repositories\RemittenceBankRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class RemittenceBankService
 * @package App\Services
 */
class RemittenceBankService extends AppService
{
    use CrudMethods;

    /**
     * @var RemittenceBankRepository
     */
    protected $repository;

    /**
     * RemittenceBankService constructor.
     * @param RemittenceBankRepository $repository
     */
    public function __construct(RemittenceBankRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}