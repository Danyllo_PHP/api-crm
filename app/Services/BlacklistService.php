<?php

namespace App\Services;

use App\Repositories\BlacklistRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class BlacklistService
 * @package App\Services
 */
class BlacklistService extends AppService
{
    use CrudMethods;

    /**
     * @var BlacklistRepository
     */
    protected $repository;

    /**
     * BlacklistService constructor.
     * @param BlacklistRepository $repository
     */
    public function __construct(BlacklistRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}