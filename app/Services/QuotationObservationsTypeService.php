<?php

namespace App\Services;

use App\Repositories\QuotationObservationsTypeRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class QuotationObservationsTypeService
 * @package App\Services
 */
class QuotationObservationsTypeService extends AppService
{
    use CrudMethods;

    /**
     * @var QuotationObservationsTypeRepository
     */
    protected $repository;

    /**
     * QuotationObservationsTypeService constructor.
     * @param QuotationObservationsTypeRepository $repository
     */
    public function __construct(QuotationObservationsTypeRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}