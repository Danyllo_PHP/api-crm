<?php

namespace App\Services;

use App\Repositories\PhoneRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class PhoneService
 * @package App\Services
 */
class PhoneService extends AppService
{
    use CrudMethods;

    /**
     * @var PhoneRepository
     */
    protected $repository;

    /**
     * PhoneService constructor.
     * @param PhoneRepository $repository
     */
    public function __construct(PhoneRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}