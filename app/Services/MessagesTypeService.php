<?php

namespace App\Services;

use App\Repositories\MessagesTypeRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class MessagesTypeService
 * @package App\Services
 */
class MessagesTypeService extends AppService
{
    use CrudMethods;

    /**
     * @var MessagesTypeRepository
     */
    protected $repository;

    /**
     * MessagesTypeService constructor.
     * @param MessagesTypeRepository $repository
     */
    public function __construct(MessagesTypeRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}