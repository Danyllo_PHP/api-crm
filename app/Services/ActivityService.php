<?php

namespace App\Services;

use App\Repositories\ActivityRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class ActivityService
 * @package App\Services
 */
class ActivityService extends AppService
{
    use CrudMethods;

    /**
     * @var ActivityRepository
     */
    protected $repository;

    /**
     * ActivitiesService constructor.
     *
     * @param ActivityRepository $repository
     */
    public function __construct(ActivityRepository $repository)
    {
        $this->repository = $repository;
    }
}
