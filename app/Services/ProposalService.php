<?php

namespace App\Services;

use App\Repositories\ProposalRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class ProposalService
 * @package App\Services
 */
class ProposalService extends AppService
{
    use CrudMethods;

    /**
     * @var ProposalRepository
     */
    protected $repository;

    /**
     * ProposalService constructor.
     * @param ProposalRepository $repository
     */
    public function __construct(ProposalRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}