<?php

namespace App\Services;

use App\Repositories\ActionsUserRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class ActionsUserService
 * @package App\Services
 */
class ActionsUserService extends AppService
{
    use CrudMethods;

    /**
     * @var ActionsUserRepository
     */
    protected $repository;

    /**
     * ActionsUserService constructor.
     *
     * @param ActionsUserRepository $repository
     */
    public function __construct(ActionsUserRepository $repository)
    {
        $this->repository = $repository;
    }
}
