<?php

namespace App\Services;

use App\Repositories\SessionRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class SessionService
 * @package App\Services
 */
class SessionService extends AppService
{
    use CrudMethods;

    /**
     * @var SessionRepository
     */
    protected $repository;

    /**
     * SessionService constructor.
     * @param SessionRepository $repository
     */
    public function __construct(SessionRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}