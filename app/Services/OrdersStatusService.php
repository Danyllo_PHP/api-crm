<?php

namespace App\Services;

use App\Repositories\OrdersStatusRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class OrdersStatusService
 * @package App\Services
 */
class OrdersStatusService extends AppService
{
    use CrudMethods;

    /**
     * @var OrdersStatusRepository
     */
    protected $repository;

    /**
     * OrdersStatusService constructor.
     * @param OrdersStatusRepository $repository
     */
    public function __construct(OrdersStatusRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}