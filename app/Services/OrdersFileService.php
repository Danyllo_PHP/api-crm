<?php

namespace App\Services;

use App\Repositories\OrdersFileRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class OrdersFileService
 * @package App\Services
 */
class OrdersFileService extends AppService
{
    use CrudMethods;

    /**
     * @var OrdersFileRepository
     */
    protected $repository;

    /**
     * OrdersFileService constructor.
     * @param OrdersFileRepository $repository
     */
    public function __construct(OrdersFileRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}