<?php

namespace App\Services;

use App\Repositories\GroupRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class GroupService
 * @package App\Services
 */
class GroupService extends AppService
{
    use CrudMethods;

    /**
     * @var GroupRepository
     */
    protected $repository;

    /**
     * GroupService constructor.
     * @param GroupRepository $repository
     */
    public function __construct(GroupRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}