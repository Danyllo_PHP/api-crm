<?php
/**
 * Created by PhpStorm.
 * User: raylison
 * Date: 07/01/19
 * Time: 11:59
 */

namespace App\Services;

use App\Repositories\ProvidersStatusRepository;
use App\Services\Traits\CrudMethods;

/**
 * Class ProviderStatusService
 * @package App\Services
 */class ProviderStatusService extends AppService
{
    use CrudMethods;

    /**
     * @var ProvidersStatusRepository
     */
    protected $repository;

    /**
     * ProviderStatusService constructor.
     * @param ProvidersStatusRepository $repository
     */
    public function __construct(ProvidersStatusRepository $repository)
    {
        $this->repository  = $repository;
    }

}
