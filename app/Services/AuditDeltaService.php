<?php

namespace App\Services;

use App\Repositories\AuditDeltaRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class AuditDeltaService
 * @package App\Services
 */
class AuditDeltaService extends AppService
{
    use CrudMethods;

    /**
     * @var AuditDeltaRepository
     */
    protected $repository;

    /**
     * AuditDeltaService constructor.
     * @param AuditDeltaRepository $repository
     */
    public function __construct(AuditDeltaRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}