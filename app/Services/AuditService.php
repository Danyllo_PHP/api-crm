<?php

namespace App\Services;

use App\Repositories\AuditRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class AuditService
 * @package App\Services
 */
class AuditService extends AppService
{
    use CrudMethods;

    /**
     * @var AuditRepository
     */
    protected $repository;

    /**
     * AuditService constructor.
     * @param AuditRepository $repository
     */
    public function __construct(AuditRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}