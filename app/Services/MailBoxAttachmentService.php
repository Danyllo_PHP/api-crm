<?php

namespace App\Services;

use App\Repositories\MailBoxAttachmentRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class MailBoxAttachmentService
 * @package App\Services
 */
class MailBoxAttachmentService extends AppService
{
    use CrudMethods;

    /**
     * @var MailBoxAttachmentRepository
     */
    protected $repository;

    /**
     * MailBoxAttachmentService constructor.
     * @param MailBoxAttachmentRepository $repository
     */
    public function __construct(MailBoxAttachmentRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}