<?php

namespace App\Services;

use App\Repositories\ProvidersFileRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class ProvidersFileService
 * @package App\Services
 */
class ProvidersFileService extends AppService
{
    use CrudMethods;

    /**
     * @var ProvidersFileRepository
     */
    protected $repository;

    /**
     * ProvidersFileService constructor.
     * @param ProvidersFileRepository $repository
     */
    public function __construct(ProvidersFileRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}