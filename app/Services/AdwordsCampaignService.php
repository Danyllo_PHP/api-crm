<?php

namespace App\Services;

use App\Repositories\AdwordsCampaignRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class AdwordsCampaignService
 * @package App\Services
 */
class AdwordsCampaignService extends AppService
{
    use CrudMethods;

    /**
     * @var AdwordsCampaignRepository
     */
    protected $repository;

    /**
     * AddressService constructor.
     * @param AdwordsCampaignRepository $repository
     */
    public function __construct(AdwordsCampaignRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}