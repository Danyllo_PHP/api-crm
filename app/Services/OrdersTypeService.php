<?php

namespace App\Services;

use App\Repositories\OrdersTypeRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class OrdersTypeService
 * @package App\Services
 */
class OrdersTypeService extends AppService
{
    use CrudMethods;

    /**
     * @var OrdersTypeRepository
     */
    protected $repository;

    /**
     * OrdersTypeService constructor.
     * @param OrdersTypeRepository $repository
     */
    public function __construct(OrdersTypeRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}