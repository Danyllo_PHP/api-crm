<?php

namespace App\Services;

use App\Repositories\EmailEmailNotificationRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class EmailEmailNotificationService
 * @package App\Services
 */
class EmailEmailNotificationService extends AppService
{
    use CrudMethods;

    /**
     * @var EmailEmailNotificationRepository
     */
    protected $repository;

    /**
     * EmailEmailNotificationService constructor.
     * @param EmailEmailNotificationRepository $repository
     */
    public function __construct(EmailEmailNotificationRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}