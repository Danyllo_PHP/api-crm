<?php

namespace App\Services;

use App\Repositories\RemittenceFileRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class RemittenceFileService
 * @package App\Services
 */
class RemittenceFileService extends AppService
{
    use CrudMethods;

    /**
     * @var RemittenceFileRepository
     */
    protected $repository;

    /**
     * RemittenceFileService constructor.
     * @param RemittenceFileRepository $repository
     */
    public function __construct(RemittenceFileRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}