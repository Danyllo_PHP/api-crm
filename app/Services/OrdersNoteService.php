<?php

namespace App\Services;

use App\Repositories\OrdersNoteRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class OrdersNoteService
 * @package App\Services
 */
class OrdersNoteService extends AppService
{
    use CrudMethods;

    /**
     * @var OrdersNoteRepository
     */
    protected $repository;

    /**
     * OrdersNoteService constructor.
     * @param OrdersNoteRepository $repository
     */
    public function __construct(OrdersNoteRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}