<?php

namespace App\Services;

use App\Repositories\EmailCampaignsEmailListRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class EmailCampaignsEmailListService
 * @package App\Services
 */
class EmailCampaignsEmailListService extends AppService
{
    use CrudMethods;

    /**
     * @var EmailCampaignsEmailListRepository
     */
    protected $repository;

    /**
     * EmailCampaignsEmailListService constructor.
     * @param EmailCampaignsEmailListRepository $repository
     */
    public function __construct(EmailCampaignsEmailListRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}