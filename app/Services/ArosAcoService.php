<?php

namespace App\Services;

use App\Repositories\ArosAcoRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class ArosAcoService
 * @package App\Services
 */
class ArosAcoService extends AppService
{
    use CrudMethods;

    /**
     * @var ArosAcoRepository
     */
    protected $repository;

    /**
     * ArosAcoService constructor.
     * @param ArosAcoRepository $repository
     */
    public function __construct(ArosAcoRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}