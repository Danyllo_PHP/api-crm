<?php

namespace App\Services;

use App\Repositories\ProvidersOccupationRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class ProvidersOccupationService
 * @package App\Services
 */
class ProvidersOccupationService extends AppService
{
    use CrudMethods;

    /**
     * @var ProvidersOccupationRepository
     */
    protected $repository;

    /**
     * ProvidersOccupationService constructor.
     * @param ProvidersOccupationRepository $repository
     */
    public function __construct(ProvidersOccupationRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}