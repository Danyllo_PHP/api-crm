<?php

namespace App\Services;

use App\Repositories\InvoiceStatusRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class InvoiceStatusService
 * @package App\Services
 */
class InvoiceStatusService extends AppService
{
    use CrudMethods;

    /**
     * @var InvoiceStatusRepository
     */
    protected $repository;

    /**
     * InvoiceStatusService constructor.
     * @param InvoiceStatusRepository $repository
     */
    public function __construct(InvoiceStatusRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}