<?php

namespace App\Services;

use App\Repositories\DepartmentRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class DepartmentService
 * @package App\Services
 */
class DepartmentService extends AppService
{
    use CrudMethods;

    /**
     * @var DepartmentRepository
     */
    protected $repository;

    /**
     * DepartmentService constructor.
     * @param DepartmentRepository $repository
     */
    public function __construct(DepartmentRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}