<?php

namespace App\Services;

use App\Repositories\NoteTypeRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class NoteTypeService
 * @package App\Services
 */
class NoteTypeService extends AppService
{
    use CrudMethods;

    /**
     * @var NoteTypeRepository
     */
    protected $repository;

    /**
     * NoteTypeService constructor.
     * @param NoteTypeRepository $repository
     */
    public function __construct(NoteTypeRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}