<?php

namespace App\Services;

use App\Repositories\EmailCampaignLogRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class EmailCampaignLogService
 * @package App\Services
 */
class EmailCampaignLogService extends AppService
{
    use CrudMethods;

    /**
     * @var EmailCampaignLogRepository
     */
    protected $repository;

    /**
     * EmailCampaignLogService constructor.
     * @param EmailCampaignLogRepository $repository
     */
    public function __construct(EmailCampaignLogRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}