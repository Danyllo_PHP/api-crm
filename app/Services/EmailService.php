<?php

namespace App\Services;

use App\Repositories\EmailRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class EmailService
 * @package App\Services
 */
class EmailService extends AppService
{
    use CrudMethods;

    /**
     * @var EmailRepository
     */
    protected $repository;

    /**
     * EmailService constructor.
     * @param EmailRepository $repository
     */
    public function __construct(EmailRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}