<?php

namespace App\Services;

use App\Repositories\MessagesStatusRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class MessagesStatusService
 * @package App\Services
 */
class MessagesStatusService extends AppService
{
    use CrudMethods;

    /**
     * @var MessagesStatusRepository
     */
    protected $repository;

    /**
     * MessagesStatusService constructor.
     * @param MessagesStatusRepository $repository
     */
    public function __construct(MessagesStatusRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}