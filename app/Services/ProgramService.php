<?php

namespace App\Services;


use App\Repositories\ProgramRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class ProgramService
 * @package App\Services
 */
class ProgramService extends AppService
{
    use CrudMethods;

    /**
     * @var ProgramRepository
     */
    protected $repository;

    /**
     * ProgramService constructor.
     * @param ProgramRepository $repository
     */
    public function __construct(ProgramRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}