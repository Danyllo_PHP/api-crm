<?php

namespace App\Services;

use App\Repositories\QuotationFileRepository;
use App\Services\Traits\CrudMethods;


/**
 * Class QuotationFileService
 * @package App\Services
 */
class QuotationFileService extends AppService
{
    use CrudMethods;

    /**
     * @var QuotationFileRepository
     */
    protected $repository;

    /**
     * QuotationFileService constructor.
     * @param QuotationFileRepository $repository
     */
    public function __construct(QuotationFileRepository $repository)
    {
        $this->repository  = $repository;
    }

   
}