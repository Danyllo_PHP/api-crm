<?php

namespace App\Criterias;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;


/**
 * Class FilterReceiptByDateCriteria
 * @package App\Criterias
 */
class FilterByDateCriteria implements CriteriaInterface
{
    /**
     * @var \Illuminate\Http\Request
     */
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if($this->request->query->has('start_date') && $this->request->query->has('end_date')){
            $start = Carbon::createFromFormat('Y-m-d', $this->request->query->get('start_date'))->startOfDay();
            $end   = Carbon::createFromFormat('Y-m-d', $this->request->query->get('end_date'))->endOfDay();

            return $model->whereBetween('created_at', [$start, $end]);
        }

        return $model;
    }
}
