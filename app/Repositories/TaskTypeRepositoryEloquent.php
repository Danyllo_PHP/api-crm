<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\TaskTypeRepository;
use App\Entities\TaskType;
use App\Validators\TaskTypeValidator;

/**
 * Class TaskTypeRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class TaskTypeRepositoryEloquent extends BaseRepository implements TaskTypeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return TaskType::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return TaskTypeValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
