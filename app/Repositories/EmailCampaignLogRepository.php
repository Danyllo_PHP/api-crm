<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EmailCampaignLogRepository.
 *
 * @package namespace App\Repositories;
 */
interface EmailCampaignLogRepository extends RepositoryInterface
{
    //
}
