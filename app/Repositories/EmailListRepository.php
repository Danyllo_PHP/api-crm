<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EmailListRepository.
 *
 * @package namespace App\Repositories;
 */
interface EmailListRepository extends RepositoryInterface
{
    //
}
