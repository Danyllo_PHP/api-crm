<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PaymentsProgramRepository.
 *
 * @package namespace App\Repositories;
 */
interface PaymentsProgramRepository extends RepositoryInterface
{
    //
}
