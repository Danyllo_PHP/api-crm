<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MailBoxAttachmentRepository.
 *
 * @package namespace App\Repositories;
 */
interface MailBoxAttachmentRepository extends RepositoryInterface
{
    //
}
