<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ProvidersFileRepository;
use App\Entities\ProvidersFile;
use App\Validators\ProvidersFileValidator;

/**
 * Class ProvidersFileRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ProvidersFileRepositoryEloquent extends BaseRepository implements ProvidersFileRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProvidersFile::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return ProvidersFileValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
