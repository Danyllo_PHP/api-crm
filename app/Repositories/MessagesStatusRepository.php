<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MessagesStatusRepository.
 *
 * @package namespace App\Repositories;
 */
interface MessagesStatusRepository extends RepositoryInterface
{
    //
}
