<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EmailEmailNotificationRepository.
 *
 * @package namespace App\Repositories;
 */
interface EmailEmailNotificationRepository extends RepositoryInterface
{
    //
}
