<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CommentTypeRepository.
 *
 * @package namespace App\Repositories;
 */
interface CommentTypeRepository extends RepositoryInterface
{
    //
}
