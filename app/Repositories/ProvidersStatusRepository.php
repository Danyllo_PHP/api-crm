<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProvidersStatusRepository.
 *
 * @package namespace App\Repositories;
 */
interface ProvidersStatusRepository extends RepositoryInterface
{
    //
}
