<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MailBoxEmailRepository;
use App\Entities\MailBoxEmail;
use App\Validators\MailBoxEmailValidator;

/**
 * Class MailBoxEmailRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class MailBoxEmailRepositoryEloquent extends BaseRepository implements MailBoxEmailRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MailBoxEmail::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return MailBoxEmailValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
