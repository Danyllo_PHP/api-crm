<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrdersRemittenceFileRepository.
 *
 * @package namespace App\Repositories;
 */
interface OrdersRemittenceFileRepository extends RepositoryInterface
{
    //
}
