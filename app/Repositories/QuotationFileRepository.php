<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface QuotationFileRepository.
 *
 * @package namespace App\Repositories;
 */
interface QuotationFileRepository extends RepositoryInterface
{
    //
}
