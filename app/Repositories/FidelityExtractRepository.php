<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface FidelityExtractRepository.
 *
 * @package namespace App\Repositories;
 */
interface FidelityExtractRepository extends RepositoryInterface
{
    //
}
