<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PhoneCallRepository;
use App\Entities\PhoneCall;
use App\Validators\PhoneCallValidator;

/**
 * Class PhoneCallRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class PhoneCallRepositoryEloquent extends BaseRepository implements PhoneCallRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PhoneCall::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return PhoneCallValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
