<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RbruteforcelogRepository.
 *
 * @package namespace App\Repositories;
 */
interface RbruteforcelogRepository extends RepositoryInterface
{
    //
}
