<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface StockAuditRepository.
 *
 * @package namespace App\Repositories;
 */
interface StockAuditRepository extends RepositoryInterface
{
    //
}
