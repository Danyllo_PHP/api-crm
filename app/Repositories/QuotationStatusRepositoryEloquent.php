<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\QuotationStatusRepository;
use App\Entities\QuotationStatus;
use App\Validators\QuotationStatusValidator;

/**
 * Class QuotationStatusRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class QuotationStatusRepositoryEloquent extends BaseRepository implements QuotationStatusRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return QuotationStatus::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return QuotationStatusValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
