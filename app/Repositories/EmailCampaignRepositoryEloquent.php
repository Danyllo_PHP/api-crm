<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\EmailCampaignRepository;
use App\Entities\EmailCampaign;
use App\Validators\EmailCampaignValidator;

/**
 * Class EmailCampaignRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class EmailCampaignRepositoryEloquent extends BaseRepository implements EmailCampaignRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return EmailCampaign::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return EmailCampaignValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
