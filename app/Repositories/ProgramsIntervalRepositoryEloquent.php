<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ProgramsIntervalRepository;
use App\Entities\ProgramsInterval;
use App\Validators\ProgramsIntervalValidator;

/**
 * Class ProgramsIntervalRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ProgramsIntervalRepositoryEloquent extends BaseRepository implements ProgramsIntervalRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProgramsInterval::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return ProgramsIntervalValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
