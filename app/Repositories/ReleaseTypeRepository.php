<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ReleaseTypeRepository.
 *
 * @package namespace App\Repositories;
 */
interface ReleaseTypeRepository extends RepositoryInterface
{
    //
}
