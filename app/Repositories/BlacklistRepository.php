<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BlacklistRepository.
 *
 * @package namespace App\Repositories;
 */
interface BlacklistRepository extends RepositoryInterface
{
    //
}
