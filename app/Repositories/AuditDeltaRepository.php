<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AuditDeltaRepository.
 *
 * @package namespace App\Repositories;
 */
interface AuditDeltaRepository extends RepositoryInterface
{
    //
}
