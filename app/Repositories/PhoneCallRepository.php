<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PhoneCallRepository.
 *
 * @package namespace App\Repositories;
 */
interface PhoneCallRepository extends RepositoryInterface
{
    //
}
