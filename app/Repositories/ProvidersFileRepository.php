<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProvidersFileRepository.
 *
 * @package namespace App\Repositories;
 */
interface ProvidersFileRepository extends RepositoryInterface
{
    //
}
