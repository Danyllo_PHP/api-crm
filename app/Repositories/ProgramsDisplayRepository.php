<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProgramsDisplayRepository.
 *
 * @package namespace App\Repositories;
 */
interface ProgramsDisplayRepository extends RepositoryInterface
{
    //
}
