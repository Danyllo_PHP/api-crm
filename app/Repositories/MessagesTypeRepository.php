<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MessagesTypeRepository.
 *
 * @package namespace App\Repositories;
 */
interface MessagesTypeRepository extends RepositoryInterface
{
    //
}
