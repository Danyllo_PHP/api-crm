<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProvidersStatusPaymentFormRepository.
 *
 * @package namespace App\Repositories;
 */
interface ProvidersStatusPaymentFormRepository extends RepositoryInterface
{
    //
}
