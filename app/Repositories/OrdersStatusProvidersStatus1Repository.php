<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrdersStatusProvidersStatus1Repository.
 *
 * @package namespace App\Repositories;
 */
interface OrdersStatusProvidersStatus1Repository extends RepositoryInterface
{
    //
}
