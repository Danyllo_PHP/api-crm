<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ReleaseTypeRepository;
use App\Entities\ReleaseType;
use App\Validators\ReleaseTypeValidator;

/**
 * Class ReleaseTypeRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ReleaseTypeRepositoryEloquent extends BaseRepository implements ReleaseTypeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ReleaseType::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return ReleaseTypeValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
