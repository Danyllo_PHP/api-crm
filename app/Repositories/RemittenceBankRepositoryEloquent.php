<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\RemittenceBankRepository;
use App\Entities\RemittenceBank;
use App\Validators\RemittenceBankValidator;

/**
 * Class RemittenceBankRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class RemittenceBankRepositoryEloquent extends BaseRepository implements RemittenceBankRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RemittenceBank::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return RemittenceBankValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
