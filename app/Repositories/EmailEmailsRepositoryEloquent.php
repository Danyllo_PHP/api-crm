<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\EmailEmailsRepository;
use App\Entities\EmailEmails;
use App\Validators\EmailEmailsValidator;

/**
 * Class EmailEmailsRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class EmailEmailsRepositoryEloquent extends BaseRepository implements EmailEmailsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return EmailEmails::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return EmailEmailsValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
