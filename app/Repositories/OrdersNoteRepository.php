<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrdersNoteRepository.
 *
 * @package namespace App\Repositories;
 */
interface OrdersNoteRepository extends RepositoryInterface
{
    //
}
