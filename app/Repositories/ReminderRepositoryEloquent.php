<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ReminderRepository;
use App\Entities\Reminder;
use App\Validators\ReminderValidator;

/**
 * Class ReminderRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ReminderRepositoryEloquent extends BaseRepository implements ReminderRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Reminder::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return ReminderValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
