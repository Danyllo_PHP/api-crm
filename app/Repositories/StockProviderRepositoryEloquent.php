<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\StockProviderRepository;
use App\Entities\StockProvider;
use App\Validators\StockProviderValidator;

/**
 * Class StockProviderRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class StockProviderRepositoryEloquent extends BaseRepository implements StockProviderRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return StockProvider::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return StockProviderValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
