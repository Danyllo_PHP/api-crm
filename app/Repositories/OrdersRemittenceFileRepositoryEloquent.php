<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\OrdersRemittenceFileRepository;
use App\Entities\OrdersRemittenceFile;
use App\Validators\OrdersRemittenceFileValidator;

/**
 * Class OrdersRemittenceFileRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class OrdersRemittenceFileRepositoryEloquent extends BaseRepository implements OrdersRemittenceFileRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return OrdersRemittenceFile::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return OrdersRemittenceFileValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
