<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\RemittenceConfigRepository;
use App\Entities\RemittenceConfig;
use App\Validators\RemittenceConfigValidator;

/**
 * Class RemittenceConfigRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class RemittenceConfigRepositoryEloquent extends BaseRepository implements RemittenceConfigRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RemittenceConfig::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return RemittenceConfigValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
