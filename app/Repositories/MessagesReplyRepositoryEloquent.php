<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MessagesReplyRepository;
use App\Entities\MessagesReply;
use App\Validators\MessagesReplyValidator;

/**
 * Class MessagesReplyRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class MessagesReplyRepositoryEloquent extends BaseRepository implements MessagesReplyRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MessagesReply::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return MessagesReplyValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
