<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\RemittenceReturnRepository;
use App\Entities\RemittenceReturn;
use App\Validators\RemittenceReturnValidator;

/**
 * Class RemittenceReturnRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class RemittenceReturnRepositoryEloquent extends BaseRepository implements RemittenceReturnRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RemittenceReturn::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return RemittenceReturnValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
