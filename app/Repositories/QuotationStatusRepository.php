<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface QuotationStatusRepository.
 *
 * @package namespace App\Repositories;
 */
interface QuotationStatusRepository extends RepositoryInterface
{
    //
}
