<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PaymentsProgramRepository;
use App\Entities\PaymentsProgram;
use App\Validators\PaymentsProgramValidator;

/**
 * Class PaymentsProgramRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class PaymentsProgramRepositoryEloquent extends BaseRepository implements PaymentsProgramRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PaymentsProgram::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return PaymentsProgramValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
