<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\BlacklistRepository;
use App\Entities\Blacklist;
use App\Validators\BlacklistValidator;

/**
 * Class BlacklistRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class BlacklistRepositoryEloquent extends BaseRepository implements BlacklistRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Blacklist::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return BlacklistValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
