<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\EntitiesReleaseRepository;
use App\Entities\EntitiesRelease;
use App\Validators\EntitiesReleaseValidator;

/**
 * Class EntitiesReleaseRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class EntitiesReleaseRepositoryEloquent extends BaseRepository implements EntitiesReleaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return EntitiesRelease::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return EntitiesReleaseValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
