<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\QuotationFileRepository;
use App\Entities\QuotationFile;
use App\Validators\QuotationFileValidator;

/**
 * Class QuotationFileRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class QuotationFileRepositoryEloquent extends BaseRepository implements QuotationFileRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return QuotationFile::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return QuotationFileValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
