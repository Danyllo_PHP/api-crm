<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CalendarRepository.
 *
 * @package namespace App\Repositories;
 */
interface CalendarRepository extends RepositoryInterface
{
    //
}
