<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RemittenceReturnOrderRepository.
 *
 * @package namespace App\Repositories;
 */
interface RemittenceReturnOrderRepository extends RepositoryInterface
{
    //
}
