<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface StockProviderRepository.
 *
 * @package namespace App\Repositories;
 */
interface StockProviderRepository extends RepositoryInterface
{
    //
}
