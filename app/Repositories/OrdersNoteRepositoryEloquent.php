<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\OrdersNoteRepository;
use App\Entities\OrdersNote;
use App\Validators\OrdersNoteValidator;

/**
 * Class OrdersNoteRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class OrdersNoteRepositoryEloquent extends BaseRepository implements OrdersNoteRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return OrdersNote::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return OrdersNoteValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
