<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ActionsUserRepository.
 *
 * @package namespace App\Repositories;
 */
interface ActionsUserRepository extends RepositoryInterface
{
    //
}
