<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ArosAcoRepository;
use App\Entities\ArosAco;
use App\Validators\ArosAcoValidator;

/**
 * Class ArosAcoRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ArosAcoRepositoryEloquent extends BaseRepository implements ArosAcoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ArosAco::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return ArosAcoValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
