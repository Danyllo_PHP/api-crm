<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\OrdersTypeRepository;
use App\Entities\OrdersType;
use App\Validators\OrdersTypeValidator;

/**
 * Class OrdersTypeRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class OrdersTypeRepositoryEloquent extends BaseRepository implements OrdersTypeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return OrdersType::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return OrdersTypeValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
