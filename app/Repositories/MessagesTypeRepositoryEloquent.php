<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MessagesTypeRepository;
use App\Entities\MessagesType;
use App\Validators\MessagesTypeValidator;

/**
 * Class MessagesTypeRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class MessagesTypeRepositoryEloquent extends BaseRepository implements MessagesTypeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MessagesType::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return MessagesTypeValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
