<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RemittenceBankRepository.
 *
 * @package namespace App\Repositories;
 */
interface RemittenceBankRepository extends RepositoryInterface
{
    //
}
