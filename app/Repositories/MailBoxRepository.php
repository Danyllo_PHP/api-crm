<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MailBoxRepository.
 *
 * @package namespace App\Repositories;
 */
interface MailBoxRepository extends RepositoryInterface
{
    //
}
