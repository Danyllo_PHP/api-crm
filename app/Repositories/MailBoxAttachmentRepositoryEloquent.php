<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MailBoxAttachmentRepository;
use App\Entities\MailBoxAttachment;
use App\Validators\MailBoxAttachmentValidator;

/**
 * Class MailBoxAttachmentRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class MailBoxAttachmentRepositoryEloquent extends BaseRepository implements MailBoxAttachmentRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MailBoxAttachment::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return MailBoxAttachmentValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
