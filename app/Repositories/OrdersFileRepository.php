<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrdersFileRepository.
 *
 * @package namespace App\Repositories;
 */
interface OrdersFileRepository extends RepositoryInterface
{
    //
}
