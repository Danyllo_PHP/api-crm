<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\CommentTypeRepository;
use App\Entities\CommentType;
use App\Validators\CommentTypeValidator;

/**
 * Class CommentTypeRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class CommentTypeRepositoryEloquent extends BaseRepository implements CommentTypeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CommentType::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return CommentTypeValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
