<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AroRepository.
 *
 * @package namespace App\Repositories;
 */
interface AroRepository extends RepositoryInterface
{
    //
}
