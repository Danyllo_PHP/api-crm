<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PhoneCallsLogRepository;
use App\Entities\PhoneCallsLog;
use App\Validators\PhoneCallsLogValidator;

/**
 * Class PhoneCallsLogRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class PhoneCallsLogRepositoryEloquent extends BaseRepository implements PhoneCallsLogRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PhoneCallsLog::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return PhoneCallsLogValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
