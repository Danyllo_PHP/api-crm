<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface NoteTypeRepository.
 *
 * @package namespace App\Repositories;
 */
interface NoteTypeRepository extends RepositoryInterface
{
    //
}
