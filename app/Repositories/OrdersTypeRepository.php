<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrdersTypeRepository.
 *
 * @package namespace App\Repositories;
 */
interface OrdersTypeRepository extends RepositoryInterface
{
    //
}
