<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface QuotationObservationsTypeRepository.
 *
 * @package namespace App\Repositories;
 */
interface QuotationObservationsTypeRepository extends RepositoryInterface
{
    //
}
