<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProvidersOccupationRepository.
 *
 * @package namespace App\Repositories;
 */
interface ProvidersOccupationRepository extends RepositoryInterface
{
    //
}
