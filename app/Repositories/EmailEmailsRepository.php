<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EmailEmailsRepository.
 *
 * @package namespace App\Repositories;
 */
interface EmailEmailsRepository extends RepositoryInterface
{
    //
}
