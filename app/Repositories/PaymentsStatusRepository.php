<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PaymentsStatusRepository.
 *
 * @package namespace App\Repositories;
 */
interface PaymentsStatusRepository extends RepositoryInterface
{
    //
}
