<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RemittenceConfigRepository.
 *
 * @package namespace App\Repositories;
 */
interface RemittenceConfigRepository extends RepositoryInterface
{
    //
}
