<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\RemittenceReturnOrderRepository;
use App\Entities\RemittenceReturnOrder;
use App\Validators\RemittenceReturnOrderValidator;

/**
 * Class RemittenceReturnOrderRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class RemittenceReturnOrderRepositoryEloquent extends BaseRepository implements RemittenceReturnOrderRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RemittenceReturnOrder::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return RemittenceReturnOrderValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
