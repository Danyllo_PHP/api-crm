<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\InvoiceStatusRepository;
use App\Entities\InvoiceStatus;
use App\Validators\InvoiceStatusValidator;

/**
 * Class InvoiceStatusRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class InvoiceStatusRepositoryEloquent extends BaseRepository implements InvoiceStatusRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return InvoiceStatus::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return InvoiceStatusValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
