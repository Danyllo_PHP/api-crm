<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ActionsAcoRepository.
 *
 * @package namespace App\Repositories;
 */
interface ActionsAcoRepository extends RepositoryInterface
{
    //
}
