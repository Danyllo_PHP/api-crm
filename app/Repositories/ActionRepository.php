<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ActionRepository.
 *
 * @package namespace App\Repositories;
 */
interface ActionRepository extends RepositoryInterface
{
    //
}
