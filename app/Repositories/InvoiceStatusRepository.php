<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface InvoiceStatusRepository.
 *
 * @package namespace App\Repositories;
 */
interface InvoiceStatusRepository extends RepositoryInterface
{
    //
}
