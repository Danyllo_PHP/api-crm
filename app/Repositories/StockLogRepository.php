<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface StockLogRepository.
 *
 * @package namespace App\Repositories;
 */
interface StockLogRepository extends RepositoryInterface
{
    //
}
