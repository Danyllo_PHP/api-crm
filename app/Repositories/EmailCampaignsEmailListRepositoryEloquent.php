<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\EmailCampaignsEmailListRepository;
use App\Entities\EmailCampaignsEmailList;
use App\Validators\EmailCampaignsEmailListValidator;

/**
 * Class EmailCampaignsEmailListRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class EmailCampaignsEmailListRepositoryEloquent extends BaseRepository implements EmailCampaignsEmailListRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return EmailCampaignsEmailList::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return EmailCampaignsEmailListValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
