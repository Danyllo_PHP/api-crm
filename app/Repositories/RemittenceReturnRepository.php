<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RemittenceReturnRepository.
 *
 * @package namespace App\Repositories;
 */
interface RemittenceReturnRepository extends RepositoryInterface
{
    //
}
