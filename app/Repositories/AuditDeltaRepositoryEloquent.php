<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\AuditDeltaRepository;
use App\Entities\AuditDelta;
use App\Validators\AuditDeltaValidator;

/**
 * Class AuditDeltaRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class AuditDeltaRepositoryEloquent extends BaseRepository implements AuditDeltaRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AuditDelta::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return AuditDeltaValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
