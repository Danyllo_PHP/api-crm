<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RemittenceFileRepository.
 *
 * @package namespace App\Repositories;
 */
interface RemittenceFileRepository extends RepositoryInterface
{
    //
}
