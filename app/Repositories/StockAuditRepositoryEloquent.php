<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\StockAuditRepository;
use App\Entities\StockAudit;
use App\Validators\StockAuditValidator;

/**
 * Class StockAuditRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class StockAuditRepositoryEloquent extends BaseRepository implements StockAuditRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return StockAudit::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return StockAuditValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
