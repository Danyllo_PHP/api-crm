<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\QuotationObservationsTypeRepository;
use App\Entities\QuotationObservationsType;
use App\Validators\QuotationObservationsTypeValidator;

/**
 * Class QuotationObservationsTypeRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class QuotationObservationsTypeRepositoryEloquent extends BaseRepository implements QuotationObservationsTypeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return QuotationObservationsType::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return QuotationObservationsTypeValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
