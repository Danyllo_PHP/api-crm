<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\EmailListEmailEmailRepository;
use App\Entities\EmailListEmailEmail;
use App\Validators\EmailListEmailEmailValidator;

/**
 * Class EmailListEmailEmailRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class EmailListEmailEmailRepositoryEloquent extends BaseRepository implements EmailListEmailEmailRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return EmailListEmailEmail::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return EmailListEmailEmailValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
