<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PhoneCallsLogRepository.
 *
 * @package namespace App\Repositories;
 */
interface PhoneCallsLogRepository extends RepositoryInterface
{
    //
}
