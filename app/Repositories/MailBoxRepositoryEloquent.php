<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MailBoxRepository;
use App\Entities\MailBox;
use App\Validators\MailBoxValidator;

/**
 * Class MailBoxRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class MailBoxRepositoryEloquent extends BaseRepository implements MailBoxRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MailBox::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return MailBoxValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
