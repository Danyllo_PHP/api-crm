<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\NoteTypeRepository;
use App\Entities\NoteType;
use App\Validators\NoteTypeValidator;

/**
 * Class NoteTypeRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class NoteTypeRepositoryEloquent extends BaseRepository implements NoteTypeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return NoteType::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return NoteTypeValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
