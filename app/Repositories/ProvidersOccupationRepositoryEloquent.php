<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ProvidersOccupationRepository;
use App\Entities\ProvidersOccupation;
use App\Validators\ProvidersOccupationValidator;

/**
 * Class ProvidersOccupationRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ProvidersOccupationRepositoryEloquent extends BaseRepository implements ProvidersOccupationRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProvidersOccupation::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return ProvidersOccupationValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
