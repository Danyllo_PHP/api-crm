<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MessagesStatusRepository;
use App\Entities\MessagesStatus;
use App\Validators\MessagesStatusValidator;

/**
 * Class MessagesStatusRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class MessagesStatusRepositoryEloquent extends BaseRepository implements MessagesStatusRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MessagesStatus::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return MessagesStatusValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
