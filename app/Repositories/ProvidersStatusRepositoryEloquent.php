<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ProvidersStatusRepository;
use App\Entities\ProvidersStatus;
use App\Validators\ProvidersStatusValidator;

/**
 * Class ProvidersStatusRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ProvidersStatusRepositoryEloquent extends BaseRepository implements ProvidersStatusRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProvidersStatus::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return ProvidersStatusValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
