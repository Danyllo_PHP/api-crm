<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\RemittenceFileRepository;
use App\Entities\RemittenceFile;
use App\Validators\RemittenceFileValidator;

/**
 * Class RemittenceFileRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class RemittenceFileRepositoryEloquent extends BaseRepository implements RemittenceFileRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RemittenceFile::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return RemittenceFileValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
