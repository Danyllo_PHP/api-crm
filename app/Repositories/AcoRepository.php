<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AcoRepository.
 *
 * @package namespace App\Repositories;
 */
interface AcoRepository extends RepositoryInterface
{
    //
}
