<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MessagesReplyRepository.
 *
 * @package namespace App\Repositories;
 */
interface MessagesReplyRepository extends RepositoryInterface
{
    //
}
