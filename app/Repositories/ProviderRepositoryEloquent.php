<?php

namespace App\Repositories;

use App\Presenters\ProviderPresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ProviderRepository;
use App\Entities\Provider;
use App\Validators\ProviderValidator;

/**
 * Class ProviderRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ProviderRepositoryEloquent extends AppRepository implements ProviderRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Provider::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {
        return ProviderValidator::class;
    }

    /**
     * @return mixed
     */
   public function presenter()
   {
       return ProviderPresenter::class;
   }


}
