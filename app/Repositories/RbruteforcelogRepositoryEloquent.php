<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\RbruteforcelogRepository;
use App\Entities\Rbruteforcelog;
use App\Validators\RbruteforcelogValidator;

/**
 * Class RbruteforcelogRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class RbruteforcelogRepositoryEloquent extends BaseRepository implements RbruteforcelogRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Rbruteforcelog::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return RbruteforcelogValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
