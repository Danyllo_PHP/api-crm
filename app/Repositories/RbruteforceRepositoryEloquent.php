<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\RbruteforceRepository;
use App\Entities\Rbruteforce;
use App\Validators\RbruteforceValidator;

/**
 * Class RbruteforceRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class RbruteforceRepositoryEloquent extends BaseRepository implements RbruteforceRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Rbruteforce::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return RbruteforceValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
