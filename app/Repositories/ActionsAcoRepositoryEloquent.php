<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ActionsAcoRepository;
use App\Entities\ActionsAco;
use App\Validators\ActionsAcoValidator;

/**
 * Class ActionsAcoRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ActionsAcoRepositoryEloquent extends BaseRepository implements ActionsAcoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ActionsAco::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return ActionsAcoValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
