<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\OrdersStatusProvidersStatus1Repository;
use App\Entities\OrdersStatusProvidersStatus1;
use App\Validators\OrdersStatusProvidersStatus1Validator;

/**
 * Class OrdersStatusProvidersStatus1RepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class OrdersStatusProvidersStatus1RepositoryEloquent extends BaseRepository implements OrdersStatusProvidersStatus1Repository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return OrdersStatusProvidersStatus1::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return OrdersStatusProvidersStatus1Validator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
