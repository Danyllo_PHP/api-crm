<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AdwordsCampaignRepository.
 *
 * @package namespace App\Repositories;
 */
interface AdwordsCampaignRepository extends RepositoryInterface
{
    //
}
