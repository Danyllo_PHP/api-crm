<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EmailCampaignRepository.
 *
 * @package namespace App\Repositories;
 */
interface EmailCampaignRepository extends RepositoryInterface
{
    //
}
