<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ProgramsDisplayRepository;
use App\Entities\ProgramsDisplay;
use App\Validators\ProgramsDisplayValidator;

/**
 * Class ProgramsDisplayRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ProgramsDisplayRepositoryEloquent extends BaseRepository implements ProgramsDisplayRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProgramsDisplay::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return ProgramsDisplayValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
