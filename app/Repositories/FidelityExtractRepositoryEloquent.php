<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\FidelityExtractRepository;
use App\Entities\FidelityExtract;
use App\Validators\FidelityExtractValidator;

/**
 * Class FidelityExtractRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class FidelityExtractRepositoryEloquent extends BaseRepository implements FidelityExtractRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return FidelityExtract::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return FidelityExtractValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
