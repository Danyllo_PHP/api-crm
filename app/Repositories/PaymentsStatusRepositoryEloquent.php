<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PaymentsStatusRepository;
use App\Entities\PaymentsStatus;
use App\Validators\PaymentsStatusValidator;

/**
 * Class PaymentsStatusRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class PaymentsStatusRepositoryEloquent extends BaseRepository implements PaymentsStatusRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PaymentsStatus::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return PaymentsStatusValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
