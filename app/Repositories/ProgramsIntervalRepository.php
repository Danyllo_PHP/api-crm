<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProgramsIntervalRepository.
 *
 * @package namespace App\Repositories;
 */
interface ProgramsIntervalRepository extends RepositoryInterface
{
    //
}
