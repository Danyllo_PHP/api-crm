<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ProvidersStatusPaymentFormRepository;
use App\Entities\ProvidersStatusPaymentForm;
use App\Validators\ProvidersStatusPaymentFormValidator;

/**
 * Class ProvidersStatusPaymentFormRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ProvidersStatusPaymentFormRepositoryEloquent extends BaseRepository implements ProvidersStatusPaymentFormRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProvidersStatusPaymentForm::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return ProvidersStatusPaymentFormValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
