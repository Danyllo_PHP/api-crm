<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\EmailServiceRepository;
use App\Entities\EmailService;
use App\Validators\EmailServiceValidator;

/**
 * Class EmailServiceRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class EmailServiceRepositoryEloquent extends BaseRepository implements EmailServiceRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return EmailService::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return EmailServiceValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
