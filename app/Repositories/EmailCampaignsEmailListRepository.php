<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EmailCampaignsEmailListRepository.
 *
 * @package namespace App\Repositories;
 */
interface EmailCampaignsEmailListRepository extends RepositoryInterface
{
    //
}
