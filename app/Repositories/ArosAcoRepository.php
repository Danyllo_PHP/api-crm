<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ArosAcoRepository.
 *
 * @package namespace App\Repositories;
 */
interface ArosAcoRepository extends RepositoryInterface
{
    //
}
