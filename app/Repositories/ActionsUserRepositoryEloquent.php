<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ActionsUserRepository;
use App\Entities\ActionsUser;
use App\Validators\ActionsUserValidator;

/**
 * Class ActionsUserRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ActionsUserRepositoryEloquent extends BaseRepository implements ActionsUserRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ActionsUser::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return ActionsUserValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
