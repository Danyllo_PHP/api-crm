<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RbruteforceRepository.
 *
 * @package namespace App\Repositories;
 */
interface RbruteforceRepository extends RepositoryInterface
{
    //
}
