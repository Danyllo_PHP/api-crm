<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\EmailEmailNotificationRepository;
use App\Entities\EmailEmailNotification;
use App\Validators\EmailEmailNotificationValidator;

/**
 * Class EmailEmailNotificationRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class EmailEmailNotificationRepositoryEloquent extends BaseRepository implements EmailEmailNotificationRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return EmailEmailNotification::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return EmailEmailNotificationValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
