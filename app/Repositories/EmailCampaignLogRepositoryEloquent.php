<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\EmailCampaignLogRepository;
use App\Entities\EmailCampaignLog;
use App\Validators\EmailCampaignLogValidator;

/**
 * Class EmailCampaignLogRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class EmailCampaignLogRepositoryEloquent extends BaseRepository implements EmailCampaignLogRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return EmailCampaignLog::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return EmailCampaignLogValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
