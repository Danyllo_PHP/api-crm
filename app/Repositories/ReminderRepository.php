<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ReminderRepository.
 *
 * @package namespace App\Repositories;
 */
interface ReminderRepository extends RepositoryInterface
{
    //
}
