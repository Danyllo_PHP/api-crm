<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Repositories\UserRepository::class, \App\Repositories\UserRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ActivityRepository::class, \App\Repositories\ActivityRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AddressRepository::class, \App\Repositories\AddressRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\BankRepository::class, \App\Repositories\BankRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\BanksProvidersSegmentRepository::class, \App\Repositories\BanksProvidersSegmentRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\BlacklistRepository::class, \App\Repositories\BlacklistRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CommentRepository::class, \App\Repositories\CommentRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CommentTypeRepository::class, \App\Repositories\CommentTypeRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\DepartmentRepository::class, \App\Repositories\DepartmentRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\EmailRepository::class, \App\Repositories\EmailRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\EmailCampaignRepository::class, \App\Repositories\EmailCampaignRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\EmailCampaignsEmailListRepository::class, \App\Repositories\EmailCampaignsEmailListRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\EmailEmailsRepository::class, \App\Repositories\EmailEmailsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\EmailListRepository::class, \App\Repositories\EmailListRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\EmailProfileRepository::class, \App\Repositories\EmailProfileRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\EmailServiceRepository::class, \App\Repositories\EmailServiceRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\EmailTemplateRepository::class, \App\Repositories\EmailTemplateRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\EmailTransportRepository::class, \App\Repositories\EmailTransportRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\FidelityRepository::class, \App\Repositories\FidelityRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\FidelityExtractRepository::class, \App\Repositories\FidelityExtractRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\GroupRepository::class, \App\Repositories\GroupRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\InvoiceRepository::class, \App\Repositories\InvoiceRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\InvoiceStatusRepository::class, \App\Repositories\InvoiceStatusRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\MailBoxRepository::class, \App\Repositories\MailBoxRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\MailBoxEmailRepository::class, \App\Repositories\MailBoxEmailRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\MessageRepository::class, \App\Repositories\MessageRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\EmailTransportRepository::class, \App\Repositories\EmailTransportRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\MessagesReplyRepository::class, \App\Repositories\MessagesReplyRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\MessagesStatusRepository::class, \App\Repositories\MessagesStatusRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\MessagesTypeRepository::class, \App\Repositories\MessagesTypeRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\MessagesUserRepository::class, \App\Repositories\MessagesUserRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\NoteRepository::class, \App\Repositories\NoteRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\NoteTypeRepository::class, \App\Repositories\NoteTypeRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\OrderRepository::class, \App\Repositories\OrderRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\OrdersFileRepository::class, \App\Repositories\OrdersFileRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\OrdersNoteRepository::class, \App\Repositories\OrdersNoteRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\OrdersProgramRepository::class, \App\Repositories\OrdersProgramRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\OrdersRemittenceFileRepository::class, \App\Repositories\OrdersRemittenceFileRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\OrdersStatusRepository::class, \App\Repositories\OrdersStatusRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\OrdersStatusProvidersStatus1Repository::class, \App\Repositories\OrdersStatusProvidersStatus1RepositoryEloquent::class);
        $this->app->bind(\App\Repositories\OrdersTypeRepository::class, \App\Repositories\OrdersTypeRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PaymentRepository::class, \App\Repositories\PaymentRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PaymentFormRepository::class, \App\Repositories\PaymentFormRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PaymentsProgramRepository::class, \App\Repositories\PaymentsProgramRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PaymentsStatusRepository::class, \App\Repositories\PaymentsStatusRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PhoneRepository::class, \App\Repositories\PhoneRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PhoneCallRepository::class, \App\Repositories\PhoneCallRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProgramRepository::class, \App\Repositories\ProgramRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProgramsDisplayRepository::class, \App\Repositories\ProgramsDisplayRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProgramsIntervalRepository::class, \App\Repositories\ProgramsIntervalRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProgramsQuotationRepository::class, \App\Repositories\ProgramsQuotationRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProviderRepository::class, \App\Repositories\ProviderRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProvidersFileRepository::class, \App\Repositories\ProvidersFileRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\QuotationRepository::class, \App\Repositories\QuotationRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\QuotationFileRepository::class, \App\Repositories\QuotationFileRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\QuotationStatusRepository::class, \App\Repositories\QuotationStatusRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\RemittenceBankRepository::class, \App\Repositories\RemittenceBankRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\RemittenceConfigRepository::class, \App\Repositories\RemittenceConfigRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\RemittenceFileRepository::class, \App\Repositories\RemittenceFileRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\RemittenceReturnRepository::class, \App\Repositories\RemittenceReturnRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\RemittenceReturnOrderRepository::class, \App\Repositories\RemittenceReturnOrderRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\SmsRepository::class, \App\Repositories\SmsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\StockRepository::class, \App\Repositories\StockRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\TaskRepository::class, \App\Repositories\TaskRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\TaskTypeRepository::class, \App\Repositories\TaskTypeRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProvidersOccupationRepository::class, \App\Repositories\ProvidersOccupationRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProvidersStatusRepository::class, \App\Repositories\ProvidersStatusRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProvidersStatusPaymentFormRepository::class, \App\Repositories\ProvidersStatusPaymentFormRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\EmailListEmailEmailRepository::class, \App\Repositories\EmailListEmailEmailRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\MailBoxAttachmentRepository::class, \App\Repositories\MailBoxAttachmentRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\QuotationObservationsTypeRepository::class, \App\Repositories\QuotationObservationsTypeRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ReminderRepository::class, \App\Repositories\ReminderRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\SegmentRepository::class, \App\Repositories\SegmentRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AuditRepository::class, \App\Repositories\AuditRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AuditDeltaRepository::class, \App\Repositories\AuditDeltaRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AdwordsCampaignRepository::class, \App\Repositories\AdwordsCampaignRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AcoRepository::class, \App\Repositories\AcoRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ActionRepository::class, \App\Repositories\ActionRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ActionsAcoRepository::class, \App\Repositories\ActionsAcoRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ActionsUserRepository::class, \App\Repositories\ActionsUserRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AroRepository::class, \App\Repositories\AroRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CalendarRepository::class, \App\Repositories\CalendarRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PendingEditionRepository::class, \App\Repositories\PendingEditionRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PhoneCallsLogRepository::class, \App\Repositories\PhoneCallsLogRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PreProviderRepository::class, \App\Repositories\PreProviderRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProposalRepository::class, \App\Repositories\ProposalRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\SessionRepository::class, \App\Repositories\SessionRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ArosAcoRepository::class, \App\Repositories\ArosAcoRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AdwordsCampaignRepository::class, \App\Repositories\AdwordsCampaignRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\EmailCampaignLogRepository::class, \App\Repositories\EmailCampaignLogRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\EntitiesReleaseRepository::class, \App\Repositories\EntitiesReleaseRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ReleaseTypeRepository::class, \App\Repositories\ReleaseTypeRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\StockAuditRepository::class, \App\Repositories\StockAuditRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\StockLogRepository::class, \App\Repositories\StockLogRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\RbruteforcelogRepository::class, \App\Repositories\RbruteforcelogRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\RbruteforceRepository::class, \App\Repositories\RbruteforceRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CustomerRepository::class, \App\Repositories\CustomerRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\EmailEmailNotificationRepository::class, \App\Repositories\EmailEmailNotificationRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\StockProviderRepository::class, \App\Repositories\StockProviderRepositoryEloquent::class);
        //:end-bindings:
    }
}
