<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\EmailCampaign;

/**
 * Class EmailCampaignTransformer.
 *
 * @package namespace App\Transformers;
 */
class EmailCampaignTransformer extends TransformerAbstract
{
    /**
     * Transform the EmailCampaign entity.
     *
     * @param \App\Entities\EmailCampaign $model
     *
     * @return array
     */
    public function transform(EmailCampaign $model)
    {
        return [
            'id'                => (int) $model->id,
            'name'              => $model->name,
            'template_id'       => $model->template_id,
            'email_transport_id'=> $model->email_transport_id,
            'email_profile_id'  => $model->email_profile_id,
            'sender'            => $model->sender,
            'paused'            => $model->paused,
            'ready'             => $model->ready,
            'started'           => $model->started,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
