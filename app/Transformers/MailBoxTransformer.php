<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\MailBox;

/**
 * Class MailBoxTransformer.
 *
 * @package namespace App\Transformers;
 */
class MailBoxTransformer extends TransformerAbstract
{
    /**
     * Transform the MailBox entity.
     *
     * @param \App\Entities\MailBox $model
     *
     * @return array
     */
    public function transform(MailBox $model)
    {
        return [
            'id'                => (int) $model->id,
            'message_id'        => $model->message_id,
            'mail_box_email_id' => $model->mail_box_email_id,
            'provider_id'       => $model->provider_id,
            'reference'         => $model->reference,
            'date'              => $model->date->toDateTimeString(),
            'subject'           => $model->subject,
            'from_name'         => $model->from_name,
            'from_address'      => $model->from_address,
            'to'                => $model->to,
            'reply_to'          => $model->reply_to,
            'text_plain'        => $model->text_plain,
            'text_html'         => $model->text_html,
            'email_origin'      => $model->email_origin,
            'attachments_origin'=> $model->attachments_origin,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
