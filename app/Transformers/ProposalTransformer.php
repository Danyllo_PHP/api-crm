<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Proposal;

/**
 * Class ProposalTransformer.
 *
 * @package namespace App\Transformers;
 */
class ProposalTransformer extends TransformerAbstract
{
    /**
     * Transform the Proposal entity.
     *
     * @param \App\Entities\Proposal $model
     *
     * @return array
     */
    public function transform(Proposal $model)
    {
        return [
            'id'                => (int) $model->id,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'quotation_id'      => $model->quotation_id,
            'dir'               => $model->dir,
            'file'              => $model->file,
            'seen'              => $model->seen,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
