<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\NoteType;

/**
 * Class NoteTypeTransformer.
 *
 * @package namespace App\Transformers;
 */
class NoteTypeTransformer extends TransformerAbstract
{
    /**
     * Transform the NoteType entity.
     *
     * @param \App\Entities\NoteType $model
     *
     * @return array
     */
    public function transform(NoteType $model)
    {
        return [
            'id'                => (int) $model->id,
            'title'             => $model->title,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
