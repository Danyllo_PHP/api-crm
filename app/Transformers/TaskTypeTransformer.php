<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\TaskType;

/**
 * Class TaskTypeTransformer.
 *
 * @package namespace App\Transformers;
 */
class TaskTypeTransformer extends TransformerAbstract
{
    /**
     * Transform the TaskType entity.
     *
     * @param \App\Entities\TaskType $model
     *
     * @return array
     */
    public function transform(TaskType $model)
    {
        return [
            'id'                => (int) $model->id,
            'title'             => $model->title,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDataTimeString(),
            'modified'          => $model->modified->toDataTimeString(),
        ];
    }
}
