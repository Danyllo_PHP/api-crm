<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\MailBoxEmail;

/**
 * Class MailBoxEmailTransformer.
 *
 * @package namespace App\Transformers;
 */
class MailBoxEmailTransformer extends TransformerAbstract
{
    /**
     * Transform the MailBoxEmail entity.
     *
     * @param \App\Entities\MailBoxEmail $model
     *
     * @return array
     */
    public function transform(MailBoxEmail $model)
    {
        return [
            'id'                => (int) $model->id,
            'description'       => $model->description,
            'email'             => $model->email,
            'mail_in_use'       => $model->mail_in_use,
            'name'              => $model->name,
            'address'           => $model->address,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
