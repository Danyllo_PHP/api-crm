<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\EntitiesRelease;

/**
 * Class EntitiesReleaseTransformer.
 *
 * @package namespace App\Transformers;
 */
class EntitiesReleaseTransformer extends TransformerAbstract
{
    /**
     * Transform the EntitiesRelease entity.
     *
     * @param \App\Entities\EntitiesRelease $model
     *
     * @return array
     */
    public function transform(EntitiesRelease $model)
    {
        return [
            'id'                => (int) $model->id,
            'parent_id'         => $model->parent_id,
            'parent_name'       => $model->parent_name,
            'user_id'           => $model->user_id,
            'approved'          => $model->approved,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
