<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\OrdersType;

/**
 * Class OrdersTypeTransformer.
 *
 * @package namespace App\Transformers;
 */
class OrdersTypeTransformer extends TransformerAbstract
{
    /**
     * Transform the OrdersType entity.
     *
     * @param \App\Entities\OrdersType $model
     *
     * @return array
     */
    public function transform(OrdersType $model)
    {
        return [
            'id'                => (int) $model->id,
            'title'             => $model->title,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
