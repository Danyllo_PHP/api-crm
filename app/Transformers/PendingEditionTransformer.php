<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\PendingEdition;

/**
 * Class PendingEditionTransformer.
 *
 * @package namespace App\Transformers;
 */
class PendingEditionTransformer extends TransformerAbstract
{
    /**
     * Transform the PendingEdition entity.
     *
     * @param \App\Entities\PendingEdition $model
     *
     * @return array
     */
    public function transform(PendingEdition $model)
    {
        return [
            'id'                => (int) $model->id,
            'model'             => $model->model,
            'primary_key'       => $model->primary_key,
            'field'             => $model->field,
            'value'             => $model->value,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
