<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Bank;

/**
 * Class BankTransformer.
 *
 * @package namespace App\Transformers;
 */
class BankTransformer extends TransformerAbstract
{
    /**
     * Transform the Bank entity.
     *
     * @param \App\Entities\Bank $model
     *
     * @return array
     */
    public function transform(Bank $model)
    {
        return [
            'id'            => (int) $model->id,
            'title'         => $model->title,
            'code'          => $model->code,
            'payment_count' => $model->payment_count,
            'created_by'    => $model->created_by,
            'modified_by'   => $model->modified_by,
            'created'       => $model->created->toDateTimeString(),
            'modified'      => $model->modified->toDateTimeString(),
        ];
    }
}
