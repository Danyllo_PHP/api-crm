<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Aro;

/**
 * Class AroTransformer.
 *
 * @package namespace App\Transformers;
 */
class AroTransformer extends TransformerAbstract
{
    /**
     * Transform the Aro entity.
     *
     * @param \App\Entities\Aro $model
     *
     * @return array
     */
    public function transform(Aro $model)
    {
        return [
            'id'            => (int) $model->id,
            'parent_id'     => $model->parent_id,
            'model'         => $model->model,
            'foreign_key'   => $model->foreign_key,
            'alias'         => $model->alias,
            'lft'           => $model->lft,
            'rght'          => $model->rght,
        ];
    }
}
