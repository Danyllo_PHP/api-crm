<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\AdwordsCampaign;

/**
 * Class AdwordsCampaignTransformer.
 *
 * @package namespace App\Transformers;
 */
class AdwordsCampaignTransformer extends TransformerAbstract
{
    /**
     * Transform the AdwordsCampaign entity.
     *
     * @param \App\Entities\AdwordsCampaign $model
     *
     * @return array
     */
    public function transform(AdwordsCampaign $model)
    {
        return [
            'id'                    => (int) $model->id,
            'name'                  => $model->name,
            'code'                  => $model->code,
            'count_quotations'      => $model->count_quotations,
            'description'           => $model->description,
            'created'               => $model->created->toDataTimeString(),
            'modified'              => $model->modified->toDataTimeString(),
        ];
    }
}
