<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\MessagesUser;

/**
 * Class MessagesUserTransformer.
 *
 * @package namespace App\Transformers;
 */
class MessagesUserTransformer extends TransformerAbstract
{
    /**
     * Transform the MessagesUser entity.
     *
     * @param \App\Entities\MessagesUser $model
     *
     * @return array
     */
    public function transform(MessagesUser $model)
    {
        return [
            'id'                => (int) $model->id,
            'message_id'        => $model->message_id,
            'user_id'           => $model->user_id,
        ];
    }
}
