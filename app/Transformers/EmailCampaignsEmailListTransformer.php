<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\EmailCampaignsEmailList;

/**
 * Class EmailCampaignsEmailListTransformer.
 *
 * @package namespace App\Transformers;
 */
class EmailCampaignsEmailListTransformer extends TransformerAbstract
{
    /**
     * Transform the EmailCampaignsEmailList entity.
     *
     * @param \App\Entities\EmailCampaignsEmailList $model
     *
     * @return array
     */
    public function transform(EmailCampaignsEmailList $model)
    {
        return [
            'id'                => (int) $model->id,
            'sent'              => $model->sent,
            'schedule'          => $model->schedule,
            'email_campaign_id' => $model->email_campaign_id,
            'email_list_id'     => $model->email_list_id,
        ];
    }
}
