<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\EmailEmails;

/**
 * Class EmailEmailsTransformer.
 *
 * @package namespace App\Transformers;
 */
class EmailEmailsTransformer extends TransformerAbstract
{
    /**
     * Transform the EmailEmails entity.
     *
     * @param \App\Entities\EmailEmails $model
     *
     * @return array
     */
    public function transform(EmailEmails $model)
    {
        return [
            'id'                => (int) $model->id,
            'email'             => $model->email,
            'unsubscribed'      => $model->unsubscribed,
            'blacklist_id'      => $model->blacklist_id,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
