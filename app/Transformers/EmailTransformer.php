<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Email;

/**
 * Class EmailTransformer.
 *
 * @package namespace App\Transformers;
 */
class EmailTransformer extends TransformerAbstract
{
    /**
     * Transform the Email entity.
     *
     * @param \App\Entities\Email $model
     *
     * @return array
     */
    public function transform(Email $model)
    {
        return [
            'id'                => (int) $model->id,
            'email'             => $model->email,
            'active'            => $model->active,
            'provider_id'       => $model->provider_id,
            'email_type'        => $model->email_type,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
