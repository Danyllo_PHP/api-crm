<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\EmailListEmailEmail;

/**
 * Class EmailListEmailEmailTransformer.
 *
 * @package namespace App\Transformers;
 */
class EmailListEmailEmailTransformer extends TransformerAbstract
{
    /**
     * Transform the EmailListEmailEmail entity.
     *
     * @param \App\Entities\EmailListEmailEmail $model
     *
     * @return array
     */
    public function transform(EmailListEmailEmail $model)
    {
        return [
            'id'         => (int) $model->id,
            'email_list_id'  => $model->email_list_id,
            'email_email_id'  => $model->email_email_id,
        ];
    }
}
