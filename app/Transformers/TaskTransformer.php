<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Task;

/**
 * Class TaskTransformer.
 *
 * @package namespace App\Transformers;
 */
class TaskTransformer extends TransformerAbstract
{
    /**
     * Transform the Task entity.
     *
     * @param \App\Entities\Task $model
     *
     * @return array
     */
    public function transform(Task $model)
    {
        return [
            'id'                => (int) $model->id,
            'quotation_id'      => $model->quotation_id,
            'task_type_id'      => $model->task_type_id,
            'user_id'           => $model->user_id,
            'due_date'          => $model->due_date->toDateString(),
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDataTimeString(),
            'modified'          => $model->modified->toDataTimeString(),
        ];
    }
}
