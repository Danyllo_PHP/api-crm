<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\RemittenceReturnOrder;

/**
 * Class RemittenceReturnOrderTransformer.
 *
 * @package namespace App\Transformers;
 */
class RemittenceReturnOrderTransformer extends TransformerAbstract
{
    /**
     * Transform the RemittenceReturnOrder entity.
     *
     * @param \App\Entities\RemittenceReturnOrder $model
     *
     * @return array
     */
    public function transform(RemittenceReturnOrder $model)
    {
        return [
            'id'                    => (int) $model->id,
            'order_id'              => $model->order_id,
            'remittence_return_id'  => $model->remittence_return_id,
            'order_status_id'       => $model->order_status_id,
            'provider_name'         => $model->provider_name,
            'provider_cpf'          => $model->provider_cpf,
            'order_price'           => $model->order_price,
            'return_order_id'       => $model->return_order_id,
            'return_codes'          => $model->return_codes,
            'protocol'              => $model->protocol,
            'auth'                  => $model->auth,
            'commitment'            => $model->commitment,
            'payment_date'          => $model->payment_date,
            'payment_due_date'      => $model->payment_due_date,
            'created_by'            => $model->created_by,
            'created'               => $model->created->toDateTimeString(),
            'modified'              => $model->modified->toDateTimeString(),
        ];
    }
}
