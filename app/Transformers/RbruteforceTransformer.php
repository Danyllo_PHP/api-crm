<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Rbruteforce;

/**
 * Class RbruteforceTransformer.
 *
 * @package namespace App\Transformers;
 */
class RbruteforceTransformer extends TransformerAbstract
{
    /**
     * Transform the Rbruteforce entity.
     *
     * @param \App\Entities\Rbruteforce $model
     *
     * @return array
     */
    public function transform(Rbruteforce $model)
    {
        return [
            'id'         => (int) $model->id,
            'url'        => $model->url,
            'expire'     => $model->expire,
        ];
    }
}
