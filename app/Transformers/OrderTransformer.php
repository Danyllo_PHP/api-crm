<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Order;

/**
 * Class OrderTransformer.
 *
 * @package namespace App\Transformers;
 */
class OrderTransformer extends TransformerAbstract
{
    /**
     * Transform the Order entity.
     *
     * @param \App\Entities\Order $model
     *
     * @return array
     */
    public function transform(Order $model)
    {
        return [
            'id'                        => (int)$model->id,
            'user_id'                   => $model->user_id,
            'provider_id'               => $model->provider_id,
            'quotation_id'              => $model->quotation_id,
            'program_id'                => $model->program_id,
            'order_status_id'           => $model->order_status_id,
            'order_type_id'             => $model->order_type_id,
            'value'                     => $model->value,
            'value_paid'                => $model->value_paid,
            'value_payable'             => $model->value_payable,
            'price'                     => $model->price,
            'remittence_file_id'        => $model->remittence_file_id,
            'observation'               => $model->observation,
            'origin'                    => $model->origin,
            'system_creator'            => $model->system_creator,
            'department'                => $model->department,
            'release_type_id'           => $model->release_type_id,
            'banks_providers_segment_id'=> $model->banks_providers_segment_id,
            'payment_form_id'           => $model->payment_form_id,
            'receipt_sent'              => $model->receipt_sent,
            'stocked'                   => $model->stocked,
            'status_modified'           => $model->status_modified->toDateTimeString(),
            'due_date'                  => $model->due_date->toDataString(),
            'session_id'                => $model->session_id,
            'created_by'                => $model->created_by,
            'modified_by'               => $model->modified_by,
            'created'                   => $model->created->toDateTimeString(),
            'modified'                  => $model->modified->toDateTimeString(),
        ];
    }
}
