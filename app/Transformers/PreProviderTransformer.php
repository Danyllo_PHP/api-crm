<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\PreProvider;

/**
 * Class PreProviderTransformer.
 *
 * @package namespace App\Transformers;
 */
class PreProviderTransformer extends TransformerAbstract
{
    /**
     * Transform the PreProvider entity.
     *
     * @param \App\Entities\PreProvider $model
     *
     * @return array
     */
    public function transform(PreProvider $model)
    {
        return [
            'id'                => (int) $model->id,
            'token'             => $model->token,
            'email'             => $model->email,
            'register'          => $model->register,
            'quotation_id'      => $model->quotation_id,
            'expiration'        => $model->expiration,
            'date_register'     => $model->date_register,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
