<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Department;

/**
 * Class DepartmentTransformer.
 *
 * @package namespace App\Transformers;
 */
class DepartmentTransformer extends TransformerAbstract
{
    /**
     * Transform the Department entity.
     *
     * @param \App\Entities\Department $model
     *
     * @return array
     */
    public function transform(Department $model)
    {
        return [
            'id'                => (int) $model->id,
            'title'             => $model->title,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
