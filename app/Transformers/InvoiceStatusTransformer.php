<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\InvoiceStatus;

/**
 * Class InvoiceStatusTransformer.
 *
 * @package namespace App\Transformers;
 */
class InvoiceStatusTransformer extends TransformerAbstract
{
    /**
     * Transform the InvoiceStatus entity.
     *
     * @param \App\Entities\InvoiceStatus $model
     *
     * @return array
     */
    public function transform(InvoiceStatus $model)
    {
        return [
            'id'                => (int) $model->id,
            'title'             => $model->title,
        ];
    }
}
