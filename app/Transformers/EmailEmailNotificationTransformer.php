<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\EmailEmailNotification;

/**
 * Class EmailEmailNotificationTransformer.
 *
 * @package namespace App\Transformers;
 */
class EmailEmailNotificationTransformer extends TransformerAbstract
{
    /**
     * Transform the EmailEmailNotification entity.
     *
     * @param \App\Entities\EmailEmailNotification $model
     *
     * @return array
     */
    public function transform(EmailEmailNotification $model)
    {
        return [
            'id'                => (int) $model->id,
            'email'             => $model->email,
            'event'             => $model->event,
            'source'            => $model->source,
            'created'           => $model->created->toDateTimeString(),
        ];
    }
}
