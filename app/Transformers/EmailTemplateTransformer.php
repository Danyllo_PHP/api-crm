<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\EmailTemplate;

/**
 * Class EmailTemplateTransformer.
 *
 * @package namespace App\Transformers;
 */
class EmailTemplateTransformer extends TransformerAbstract
{
    /**
     * Transform the EmailTemplate entity.
     *
     * @param \App\Entities\EmailTemplate $model
     *
     * @return array
     */
    public function transform(EmailTemplate $model)
    {
        return [
            'id'                => (int) $model->id,
            'name'              => $model->name,
            'body'              => $model->body,
            'type'              => $model->type,
            'user_id'           => $model->user_id,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
