<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Segment;

/**
 * Class SegmentTransformer.
 *
 * @package namespace App\Transformers;
 */
class SegmentTransformer extends TransformerAbstract
{
    /**
     * Transform the Segment entity.
     *
     * @param \App\Entities\Segment $model
     *
     * @return array
     */
    public function transform(Segment $model)
    {
        return [
            'id'         => (int) $model->id,
            'title'      => $model->title,
            'bank_id'    => $model->bank_id,   
            'created_by' => $model->created_by,
            'modified_by'=> $model->modified_by,
            'created'    => $model->created->toDateTimeString(),
            'modified'   => $model->modified->toDateTimeString(),
        ];
    }
}
