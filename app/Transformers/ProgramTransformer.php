<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Program;

/**
 * Class ProgramTransformer.
 *
 * @package namespace App\Transformers;
 */
class ProgramTransformer extends TransformerAbstract
{
    /**
     * Transform the Program entity.
     *
     * @param \App\Entities\Program $model
     *
     * @return array
     */
    public function transform(Program $model)
    {
        return [
            'id'                        => (int) $model->id,
            'title'                     => $model->title,
            'code'                      => $model->code,
            'program_quotation_count'   => $model->program_quotation_count,
            'miles_in_stock'            => $model->miles_in_stock,
            'created_by'                => $model->created_by,
            'modified_by'               => $model->modified_by,
            'created'                   => $model->created->toDateTimeString(),
            'modified'                  => $model->modified->toDateTimeString(),
        ];
    }
}
