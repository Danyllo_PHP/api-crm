<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Message;

/**
 * Class MessageTransformer.
 *
 * @package namespace App\Transformers;
 */
class MessageTransformer extends TransformerAbstract
{
    /**
     * Transform the Message entity.
     *
     * @param \App\Entities\Message $model
     *
     * @return array
     */
    public function transform(Message $model)
    {
        return [
            'id'                => (int) $model->id,
            'parent_id'         => $model->parent_id,
            'parent_name'       => $model->parent_name,
            'message_status_id' => $model->message_status_id,
            'text'              => $model->text,
            'message_type_id'   => $model->message_type_id,
            'schedule'          => $model->schedule,
            'show_for_creator'  => $model->show_for_creator,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
