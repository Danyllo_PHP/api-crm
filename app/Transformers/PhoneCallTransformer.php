<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\PhoneCall;

/**
 * Class PhoneCallTransformer.
 *
 * @package namespace App\Transformers;
 */
class PhoneCallTransformer extends TransformerAbstract
{
    /**
     * Transform the PhoneCall entity.
     *
     * @param \App\Entities\PhoneCall $model
     *
     * @return array
     */
    public function transform(PhoneCall $model)
    {
        return [
            'id'                => (int) $model->id,
            'type'              => $model->type,
            'number'            => $model->number,
            'branch'            => $model->branch,
            'date'              => $model->date->toDateTimeString(),
            'code'              => $model->code,
            'path_to_file'      => $model->path_to_file,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
