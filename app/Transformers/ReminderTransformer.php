<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Reminder;

/**
 * Class ReminderTransformer.
 *
 * @package namespace App\Transformers;
 */
class ReminderTransformer extends TransformerAbstract
{
    /**
     * Transform the Reminder entity.
     *
     * @param \App\Entities\Reminder $model
     *
     * @return array
     */
    public function transform(Reminder $model)
    {
        return [
            'id'            => (int) $model->id,
            'title'         => $model->title,
            'body'          => $model->body,
            'active'        => $model->active,
            'created_by'    => $model->created_by,
            'modified_by'   => $model->modified_by,
            'created'       => $model->created->toDateTimeString(),
            'modified'      => $model->modified->toDateTimeString(),
        ];
    }
}
