<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Activity;

/**
 * Class ActivityTransformer.
 *
 * @package namespace App\Transformers;
 */
class ActivityTransformer extends TransformerAbstract
{
    /**
     * Transform the Activity entity.
     *
     * @param \App\Entities\Activity $model
     *
     * @return array
     */
    public function transform(Activity $model)
    {
        return [
            'id'            => (int) $model->id,
            'quotation_id'  => $model->quotation_id,
            'title'         => $model->title,
            'created_by'    => $model->created_by,
            'modified_by'   => $model->modified_by,
            'created'       => $model->created->toDateTimeString(),
            'modified'      => $model->modified->toDateTimeString(),
        ];
    }
}
