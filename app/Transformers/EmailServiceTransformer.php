<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\EmailService;

/**
 * Class EmailServiceTransformer.
 *
 * @package namespace App\Transformers;
 */
class EmailServiceTransformer extends TransformerAbstract
{
    /**
     * Transform the EmailService entity.
     *
     * @param \App\Entities\EmailService $model
     *
     * @return array
     */
    public function transform(EmailService $model)
    {
        return [
            'id'                => (int) $model->id,
            'email_profile_id'  => $model->email_profile_id,
            'name'              => $model->name,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
