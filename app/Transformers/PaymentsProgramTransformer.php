<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\PaymentsProgram;

/**
 * Class PaymentsProgramTransformer.
 *
 * @package namespace App\Transformers;
 */
class PaymentsProgramTransformer extends TransformerAbstract
{
    /**
     * Transform the PaymentsProgram entity.
     *
     * @param \App\Entities\PaymentsProgram $model
     *
     * @return array
     */
    public function transform(PaymentsProgram $model)
    {
        return [
            'id'                => (int) $model->id,
            'payment_id'        => $model->payment_id,
            'program_id'        => $model->program_id,
            'value'             => $model->value,
            'price'             => $model->price,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
