<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Comment;

/**
 * Class CommentTransformer.
 *
 * @package namespace App\Transformers;
 */
class CommentTransformer extends TransformerAbstract
{
    /**
     * Transform the Comment entity.
     *
     * @param \App\Entities\Comment $model
     *
     * @return array
     */
    public function transform(Comment $model)
    {
        return [
            'id'                => (int) $model->id,
            'quotation_id'      => $model->quotation_id,
            'body'              => $model->body,
            'comment_type_id'   => $model->comment_type_id,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
            'deleted'           => $model->deleted->toDateTimeString(),
        ];
    }
}
