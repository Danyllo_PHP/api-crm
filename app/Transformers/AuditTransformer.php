<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Audit;

/**
 * Class AuditTransformer.
 *
 * @package namespace App\Transformers;
 */
class AuditTransformer extends TransformerAbstract
{
    /**
     * Transform the Audit entity.
     *
     * @param \App\Entities\Audit $model
     *
     * @return array
     */
    public function transform(Audit $model)
    {
        return [
            'id'            => (int) $model->id,
            'event'         => $model->event,
            'model'         => $model->model,
            'entity_id'     => $model->entity_id,
            'json_object'   => $model->json_object,
            'description'   => $model->description,
            'source_id'     => $model->source_id,
            'delta_count'   => $model->delta_count,
            'source_ip'     => $model->source_ip,
            'source_url'    => $model->source_url,
            'source_model'  => $model->source_model,
            'audit_id'      => $model->audit_id,
            'created'       => $model->created->toDataTimeString()
        ];
    }
}
