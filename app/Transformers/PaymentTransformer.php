<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Payment;

/**
 * Class PaymentTransformer.
 *
 * @package namespace App\Transformers;
 */
class PaymentTransformer extends TransformerAbstract
{
    /**
     * Transform the Payment entity.
     *
     * @param \App\Entities\Payment $model
     *
     * @return array
     */
    public function transform(Payment $model)
    {
        return [
            'id'                => (int) $model->id,
            'payment_status_id' => $model->payment_status_id,
            'provider_id'       => $model->provider_id,
            'quotation_id'      => $model->quotation_id,
            'program_id'        => $model->program_id,
            'bank_provider_id'  => $model->bank_provider_id,
            'bank_id'           => $model->bank_id,
            'agency'            => $model->agency,
            'type'              => $model->type,
            'account'           => $model->account,
            'operation'         => $model->operation,
            'date'              => $model->date,
            'price'             => $model->price,
            'value'             => $model->value,
            'comment'           => $model->comment,
            'status_modified'   => $model->status_modified,
            'order_id'          => $model->order_id,
            'invoice_id'        => $model->invoice_id,
            'stocked'           => $model->stocked,
            'archived'          => $model->archived,
            'receipt_sent'      => $model->receipt_sent,
            'info'              => $model->info,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
