<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Phone;

/**
 * Class PhoneTransformer.
 *
 * @package namespace App\Transformers;
 */
class PhoneTransformer extends TransformerAbstract
{
    /**
     * Transform the Phone entity.
     *
     * @param \App\Entities\Phone $model
     *
     * @return array
     */
    public function transform(Phone $model)
    {
        return [
            'id'                => (int) $model->id,
            'number'            => $model->number,
            'branch'            => $model->branch,
            'phone_type'        => $model->phone_type,
            'provider_id'       => $model->provider_id,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
