<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Sms;

/**
 * Class SmsTransformer.
 *
 * @package namespace App\Transformers;
 */
class SmsTransformer extends TransformerAbstract
{
    /**
     * Transform the Sms entity.
     *
     * @param \App\Entities\Sms $model
     *
     * @return array
     */
    public function transform(Sms $model)
    {
        return [
            'id'                => (int) $model->id,
            'dongle'            => $model->dongle,
            'dongle_number'     => $model->dongle_number,
            'number'            => $model->number,
            'provider_id'       => $model->provider_id,
            'msg'               => $model->msg,
            'status'            => $model->status,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
