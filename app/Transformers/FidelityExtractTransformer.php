<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\FidelityExtract;

/**
 * Class FidelityExtractTransformer.
 *
 * @package namespace App\Transformers;
 */
class FidelityExtractTransformer extends TransformerAbstract
{
    /**
     * Transform the FidelityExtract entity.
     *
     * @param \App\Entities\FidelityExtract $model
     *
     * @return array
     */
    public function transform(FidelityExtract $model)
    {
        return [
            'id'                => (int) $model->id,
            'fidelity_id'       => $model->fidelity_id,
            'description'       => $model->description,
            'transaction_type'  => $model->transaction_type,
            'type'              => $model->type,
            'value'             => $model->value,
            'locator'           => $model->locator,
            'stock_id'          => $model->stock_id,
            'date'              => $model->date->toDateTimeString(),
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
