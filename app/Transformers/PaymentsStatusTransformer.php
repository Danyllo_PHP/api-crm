<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\PaymentsStatus;

/**
 * Class PaymentsStatusTransformer.
 *
 * @package namespace App\Transformers;
 */
class PaymentsStatusTransformer extends TransformerAbstract
{
    /**
     * Transform the PaymentsStatus entity.
     *
     * @param \App\Entities\PaymentsStatus $model
     *
     * @return array
     */
    public function transform(PaymentsStatus $model)
    {
        return [
            'id'                => (int) $model->id,
            'title'             => $model->title,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
