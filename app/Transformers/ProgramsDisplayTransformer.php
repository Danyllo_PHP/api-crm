<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\ProgramsDisplay;

/**
 * Class ProgramsDisplayTransformer.
 *
 * @package namespace App\Transformers;
 */
class ProgramsDisplayTransformer extends TransformerAbstract
{
    /**
     * Transform the ProgramsDisplay entity.
     *
     * @param \App\Entities\ProgramsDisplay $model
     *
     * @return array
     */
    public function transform(ProgramsDisplay $model)
    {
        return [
            'id'                => (int) $model->id,
            'program_id'        => $model->program_id,
            'value'             => $model->value,
            'price'             => $model->price,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
