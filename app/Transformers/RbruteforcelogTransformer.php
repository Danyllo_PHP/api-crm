<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Rbruteforcelog;

/**
 * Class RbruteforcelogTransformer.
 *
 * @package namespace App\Transformers;
 */
class RbruteforcelogTransformer extends TransformerAbstract
{
    /**
     * Transform the Rbruteforcelog entity.
     *
     * @param \App\Entities\Rbruteforcelog $model
     *
     * @return array
     */
    public function transform(Rbruteforcelog $model)
    {
        return [
            'id'         => (int) $model->id,
            'data'       => $model->data,
        ];
    }
}
