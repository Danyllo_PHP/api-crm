<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Blacklist;

/**
 * Class BlacklistTransformer.
 *
 * @package namespace App\Transformers;
 */
class BlacklistTransformer extends TransformerAbstract
{
    /**
     * Transform the Blacklist entity.
     *
     * @param \App\Entities\Blacklist $model
     *
     * @return array
     */
    public function transform(Blacklist $model)
    {
        return [
            'id'                => (int) $model->id,
            'email'             => $model->email,
            'comment'           => $model->comment,
            'status'            => $model->status,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
