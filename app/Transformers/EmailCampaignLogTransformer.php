<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\EmailCampaignLog;

/**
 * Class EmailCampaignLogTransformer.
 *
 * @package namespace App\Transformers;
 */
class EmailCampaignLogTransformer extends TransformerAbstract
{
    /**
     * Transform the EmailCampaignLogService entity.
     *
     * @param \App\Entities\EmailCampaignLog $model
     *
     * @return array
     */
    public function transform(EmailCampaignLog $model)
    {
        return [
            'id'                    => (int) $model->id,
            'email_campaign_id'     => $model->email_campaign_id,
            'email_email_id'        => $model->email_email_id,
            'status'                => $model->status,
        ];
    }
}
