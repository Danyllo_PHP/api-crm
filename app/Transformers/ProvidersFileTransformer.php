<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\ProvidersFile;

/**
 * Class ProvidersFileTransformer.
 *
 * @package namespace App\Transformers;
 */
class ProvidersFileTransformer extends TransformerAbstract
{
    /**
     * Transform the ProvidersFile entity.
     *
     * @param \App\Entities\ProvidersFile $model
     *
     * @return array
     */
    public function transform(ProvidersFile $model)
    {
        return [
            'id'                        => (int) $model->id,
            'provider_id'               => $model->provider_id,
            'title'                     => $model->title,
            'dir'                       => $model->dir,
            'file'                      => $model->file,
            'filename'                  => $model->filename,
            'created_by'                => $model->created_by,
            'modified_by'               => $model->modified_by,
            'created'                   => $model->created->toDateTimeString(),
            'modified'                  => $model->modified->toDateTimeString(),
        ];
    }
}
