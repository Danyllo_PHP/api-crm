<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Quotation;

/**
 * Class QuotationTransformer.
 *
 * @package namespace App\Transformers;
 */
class QuotationTransformer extends TransformerAbstract
{
    /**
     * Transform the Quotation entity.
     *
     * @param \App\Entities\Quotation $model
     *
     * @return array
     */
    public function transform(Quotation $model)
    {
        return [
            'id'                        => (int) $model->id,
            'provider_id'               => $model->provider_id,
            'user_id'                   => $model->user_id,
            'quotation_status_id'       => $model->quotation_status_id,
            'name'                      => $model->name,
            'email'                     => $model->email,
            'phone'                     => $model->phone,
            'ip'                        => $model->ip,
            'comment'                   => $model->comment,
            'activity_count'            => $model->activity_count,
            'comment_count'             => $model->comment_count,
            'payment_count'             => $model->payment_count,
            'proposal_count'            => $model->proposal_count,
            'quotation_file_count'      => $model->quotation_file_count,
            'sms_count'                 => $model->sms_count,
            'task_count'                => $model->task_count,
            'archive'                   => $model->archive,
            'email_send'                => $model->email_send,
            'blacklist_id'              => $model->blacklist_id,
            'status_modified'           => $model->status_modified,
            'session_id'                => $model->session_id,
            'observations_type_id'      => $model->observations_type_id,
            'observation'               => $model->observation,
            'device'                    => $model->device,
            'browser'                   => $model->browser,
            'adw_campaign_id'           => $model->adw_campaign_id,
            'source'                    => $model->source,
            'id_reference_campaign'     => $model->id_reference_campaign,
            'created_by'                => $model->created_by,
            'modified_by'               => $model->modified_by,
            'created'                   => $model->created->toDateTimeString(),
            'modified'                  => $model->modified->toDateTimeString(),
        ];
    }
}
