<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\OrdersNote;

/**
 * Class OrdersNoteTransformer.
 *
 * @package namespace App\Transformers;
 */
class OrdersNoteTransformer extends TransformerAbstract
{
    /**
     * Transform the OrdersNote entity.
     *
     * @param \App\Entities\OrdersNote $model
     *
     * @return array
     */
    public function transform(OrdersNote $model)
    {
        return [
            'id'                => (int) $model->id,
            'order_id'          => $model->order_id,
            'body'              => $model->body,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
