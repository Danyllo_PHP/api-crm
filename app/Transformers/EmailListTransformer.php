<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\EmailList;

/**
 * Class EmailListTransformer.
 *
 * @package namespace App\Transformers;
 */
class EmailListTransformer extends TransformerAbstract
{
    /**
     * Transform the EmailList entity.
     *
     * @param \App\Entities\EmailList $model
     *
     * @return array
     */
    public function transform(EmailList $model)
    {
        return [
            'id'                => (int) $model->id,
            'name'              => $model->name,
            'description'       => $model->description,
            'created_by'        => $model->created_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
