<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\CommentType;

/**
 * Class CommentTypeTransformer.
 *
 * @package namespace App\Transformers;
 */
class CommentTypeTransformer extends TransformerAbstract
{
    /**
     * Transform the CommentType entity.
     *
     * @param \App\Entities\CommentType $model
     *
     * @return array
     */
    public function transform(CommentType $model)
    {
        return [
            'id'                => (int) $model->id,
            'title'             => $model->title,
            'created_by'        => $model->created_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
