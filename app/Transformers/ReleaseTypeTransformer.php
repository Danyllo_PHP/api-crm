<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\ReleaseType;

/**
 * Class ReleaseTypeTransformer.
 *
 * @package namespace App\Transformers;
 */
class ReleaseTypeTransformer extends TransformerAbstract
{
    /**
     * Transform the ReleaseType entity.
     *
     * @param \App\Entities\ReleaseType $model
     *
     * @return array
     */
    public function transform(ReleaseType $model)
    {
        return [
            'id'                        => (int) $model->id,
            'title'                     => $model->title,
            'selector_condition_1'      => $model->selector_condition_1,
            'value_condition_1'         => $model->value_condition_1,
            'selector_condition_2'      => $model->selector_condition_2,
            'value_condition_2'         => $model->value_condition_2,
            'created_by'                => $model->created_by,
            'modified_by'               => $model->modified_by,
            'created'                   => $model->created->toDateTimeString(),
            'modified'                  => $model->modified->toDateTimeString(),
        ];
    }
}
