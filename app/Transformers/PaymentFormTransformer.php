<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\PaymentForm;

/**
 * Class PaymentFormTransformer.
 *
 * @package namespace App\Transformers;
 */
class PaymentFormTransformer extends TransformerAbstract
{
    /**
     * Transform the PaymentForm entity.
     *
     * @param \App\Entities\PaymentForm $model
     *
     * @return array
     */
    public function transform(PaymentForm $model)
    {
        return [
            'id'                => (int) $model->id,
            'title'             => $model->title,
            'parent_id'         => $model->parent_id,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
