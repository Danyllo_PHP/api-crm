<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\EmailProfile;

/**
 * Class EmailProfileTransformer.
 *
 * @package namespace App\Transformers;
 */
class EmailProfileTransformer extends TransformerAbstract
{
    /**
     * Transform the EmailProfile entity.
     *
     * @param \App\Entities\EmailProfile $model
     *
     * @return array
     */
    public function transform(EmailProfile $model)
    {
        return [
            'id'                => (int) $model->id,
            'email_transport_id'=> $model->email_transport_id,
            'name'              => $model->name,
            'from'              => $model->from,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
