<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\StockAudit;

/**
 * Class StockAuditTransformer.
 *
 * @package namespace App\Transformers;
 */
class StockAuditTransformer extends TransformerAbstract
{
    /**
     * Transform the StockAudit entity.
     *
     * @param \App\Entities\StockAudit $model
     *
     * @return array
     */
    public function transform(StockAudit $model)
    {
        return [
            'id'                => (int) $model->id,
            'logged_in'         => $model->logged_in,
            'miles_api'         => $model->miles_api,
            'stock_has_more'    => $model->stock_has_more,
            'status'            => $model->status,
            'user_id'           => $model->user_id,
            'is_credit'         => $model->is_credit,
            'company_initials'  => $model->company_initials,
            'stock_id'          => $model->stock_id,
            'miles_stock'       => $model->miles_stock,
        ];
    }
}
