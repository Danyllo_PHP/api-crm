<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Fidelity;

/**
 * Class FidelityTransformer.
 *
 * @package namespace App\Transformers;
 */
class FidelityTransformer extends TransformerAbstract
{
    /**
     * Transform the Fidelity entity.
     *
     * @param \App\Entities\Fidelity $model
     *
     * @return array
     */
    public function transform(Fidelity $model)
    {
        return [
            'id'                => (int) $model->id,
            'card_number'       => $model->card_number,
            'access_password'   => $model->access_password,
            'digital_signature' => $model->digital_signature,
            'validity'          => $model->validity,
            'program_id'        => $model->program_id,
            'provider_id'       => $model->provider_id,
            'last_access'       => $model->last_access,
            'blocked_at'        => $model->blocked_at,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
