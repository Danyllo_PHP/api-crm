<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\RemittenceReturn;

/**
 * Class RemittenceReturnTransformer.
 *
 * @package namespace App\Transformers;
 */
class RemittenceReturnTransformer extends TransformerAbstract
{
    /**
     * Transform the RemittenceReturn entity.
     *
     * @param \App\Entities\RemittenceReturn $model
     *
     * @return array
     */
    public function transform(RemittenceReturn $model)
    {
        return [
            'id'                    => (int) $model->id,
            'path'                  => $model->path,
            'status'                => $model->status,
            'remittence_banks_id'   => $model->remittence_banks_id,
            'created_by'            => $model->created_by,
            'created'               => $model->created->toDateTimeString(),
            'modified'              => $model->modified->toDateTimeString(),
        ];
    }
}
