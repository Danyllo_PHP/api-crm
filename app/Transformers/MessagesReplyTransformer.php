<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\MessagesReply;

/**
 * Class MessagesReplyTransformer.
 *
 * @package namespace App\Transformers;
 */
class MessagesReplyTransformer extends TransformerAbstract
{
    /**
     * Transform the MessagesReply entity.
     *
     * @param \App\Entities\MessagesReply $model
     *
     * @return array
     */
    public function transform(MessagesReply $model)
    {
        return [
            'id'                => (int) $model->id,
            'message_id'        => $model->message_id,
            'text'              => $model->text,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
