<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\OrdersProgram;

/**
 * Class OrdersProgramTransformer.
 *
 * @package namespace App\Transformers;
 */
class OrdersProgramTransformer extends TransformerAbstract
{
    /**
     * Transform the OrdersProgram entity.
     *
     * @param \App\Entities\OrdersProgram $model
     *
     * @return array
     */
    public function transform(OrdersProgram $model)
    {
        return [
            'id'                => (int) $model->id,
            'order_id'          => $model->order_id,
            'program_id'        => $model->program_id,
            'number'            => $model->number,
            'file'              => $model->file,
            'file_dir'          => $model->file_dir,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
