<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\PhoneCallsLog;

/**
 * Class PhoneCallsLogTransformer.
 *
 * @package namespace App\Transformers;
 */
class PhoneCallsLogTransformer extends TransformerAbstract
{
    /**
     * Transform the PhoneCallsLog entity.
     *
     * @param \App\Entities\PhoneCallsLog $model
     *
     * @return array
     */
    public function transform(PhoneCallsLog $model)
    {
        return [
            'id'                => (int) $model->id,
            'phone_call_id'     => $model->phone_call_id,
            'user_id'           => $model->user_id,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
