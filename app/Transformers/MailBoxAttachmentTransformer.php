<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\MailBoxAttachment;

/**
 * Class MailBoxAttachmentTransformer.
 *
 * @package namespace App\Transformers;
 */
class MailBoxAttachmentTransformer extends TransformerAbstract
{
    /**
     * Transform the MailBoxAttachment entity.
     *
     * @param \App\Entities\MailBoxAttachment $model
     *
     * @return array
     */
    public function transform(MailBoxAttachment $model)
    {
        return [
            'id'                => (int) $model->id,
            'mail_box_id'       => $model->mail_box_id,
            'name'              => $model->name,
            'file_path'         => $model->file_path,
            'attachments_origin'=> $model->attachments_origin,
            'path_to_file'      => $model->path_to_file,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
