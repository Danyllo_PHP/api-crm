<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\QuotationObservationsType;

/**
 * Class QuotationObservationsTypeTransformer.
 *
 * @package namespace App\Transformers;
 */
class QuotationObservationsTypeTransformer extends TransformerAbstract
{
    /**
     * Transform the QuotationObservationsType entity.
     *
     * @param \App\Entities\QuotationObservationsType $model
     *
     * @return array
     */
    public function transform(QuotationObservationsType $model)
    {
        return [
            'id'         => (int) $model->id,
            'title'      => $model->title,
            'created'    => $model->created->toDateTimeString(),
            'modified'   => $model->modified->toDateTimeString(),
        ];
    }
}
