<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\OrdersFile;

/**
 * Class OrdersFileTransformer.
 *
 * @package namespace App\Transformers;
 */
class OrdersFileTransformer extends TransformerAbstract
{
    /**
     * Transform the OrdersFile entity.
     *
     * @param \App\Entities\OrdersFile $model
     *
     * @return array
     */
    public function transform(OrdersFile $model)
    {
        return [
            'id'                => (int) $model->id,
            'order_id'          => $model->order_id,
            'title'             => $model->title,
            'dir'               => $model->dir,
            'file'              => $model->file,
            'send'              => $model->send,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
