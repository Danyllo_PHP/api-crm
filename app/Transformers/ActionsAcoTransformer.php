<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\ActionsAco;

/**
 * Class ActionsAcoTransformer.
 *
 * @package namespace App\Transformers;
 */
class ActionsAcoTransformer extends TransformerAbstract
{
    /**
     * Transform the ActionsAco entity.
     *
     * @param \App\Entities\ActionsAco $model
     *
     * @return array
     */
    public function transform(ActionsAco $model)
    {
        return [
            'id'            => (int) $model->id,
            'created_by'    => $model->created_by,
            'modified_by'   => $model->modified_by,
            'action_id'     => $model->action_id,
            'alias'         => $model->alias,
            'created'       => $model->created->toDataTimeString(),
            'modified'      => $model->modified->toDataTimeString(),
        ];
    }
}
