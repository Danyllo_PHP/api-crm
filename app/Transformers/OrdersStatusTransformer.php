<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\OrdersStatus;

/**
 * Class OrdersStatusTransformer.
 *
 * @package namespace App\Transformers;
 */
class OrdersStatusTransformer extends TransformerAbstract
{
    /**
     * Transform the OrdersStatus entity.
     *
     * @param \App\Entities\OrdersStatus $model
     *
     * @return array
     */
    public function transform(OrdersStatus $model)
    {
        return [
            'id'                => (int) $model->id,
            'title'             => $model->title,
            'order_create'      => $model->order_create,
            'parent_id'         => $model->parent_id,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
