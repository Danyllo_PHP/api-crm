<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\RemittenceConfig;

/**
 * Class RemittenceConfigTransformer.
 *
 * @package namespace App\Transformers;
 */
class RemittenceConfigTransformer extends TransformerAbstract
{
    /**
     * Transform the RemittenceConfig entity.
     *
     * @param \App\Entities\RemittenceConfig $model
     *
     * @return array
     */
    public function transform(RemittenceConfig $model)
    {
        return [
            'id'                    => (int) $model->id,
            'name'                  => $model->name,
            'cnpj'                  => $model->cnpj,
            'address'               => $model->address,
            'number'                => $model->number,
            'complement'            => $model->complement,
            'zip_code'              => $model->zip_code,
            'zip_code_complement'   => $model->zip_code_complement,
            'city'                  => $model->city,
            'uf'                    => $model->uf,
            'payment_msg'           => $model->payment_msg,
            'remittence_banks_id'   => $model->remittence_banks_id,
            'created_by'            => $model->created_by,
            'modified_by'           => $model->modified_by,
            'created'               => $model->created->toDateTimeString(),
            'modified'              => $model->modified->toDateTimeString(),
        ];
    }
}
