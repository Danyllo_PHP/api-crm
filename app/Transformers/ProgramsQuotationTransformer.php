<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\ProgramsQuotation;

/**
 * Class ProgramsQuotationTransformer.
 *
 * @package namespace App\Transformers;
 */
class ProgramsQuotationTransformer extends TransformerAbstract
{
    /**
     * Transform the ProgramsQuotation entity.
     *
     * @param \App\Entities\ProgramsQuotation $model
     *
     * @return array
     */
    public function transform(ProgramsQuotation $model)
    {
        return [
            'id'                => (int) $model->id,
            'program_id'        => $model->program_id,
            'quotation_id'      => $model->quotation_id,
            'payment_form_id'   => $model->payment_form_id,
            'username'          => $model->username,
            'value'             => $model->value,
            'price'             => $model->price,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
