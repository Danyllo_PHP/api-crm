<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Address;

/**
 * Class AddressTransformer.
 *
 * @package namespace App\Transformers;
 */
class AddressTransformer extends TransformerAbstract
{
    /**
     * Transform the Address entity.
     *
     * @param \App\Entities\Address $model
     *
     * @return array
     */
    public function transform(Address $model)
    {
        return [
            'id'            => (int) $model->id,
            'parent_id'     => $model->parent_id,
            'model'         => $model->model,
            'zip_code'      => $model->zip_code,
            'address'       => $model->address,
            'number'        => $model->number,
            'complement'    => $model->complement,
            'neighborhood'  => $model->neighborhood,
            'city'          => $model->city,
            'state'         => $model->state,
            'address_type'  => $model->address_type,
            'street_view'   => $model->street_view,
            'extra'         => $model->extra,
            'created_by'    => $model->created_by,
            'modified_by'   => $model->modified_by,
            'created'       => $model->created->toDateTimeString(),
            'modified'      => $model->modified->toDateTimeString(),
        ];
    }
}
