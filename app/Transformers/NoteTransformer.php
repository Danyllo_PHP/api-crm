<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Note;

/**
 * Class NoteTransformer.
 *
 * @package namespace App\Transformers;
 */
class NoteTransformer extends TransformerAbstract
{
    /**
     * Transform the Note entity.
     *
     * @param \App\Entities\Note $model
     *
     * @return array
     */
    public function transform(Note $model)
    {
        return [
            'id'                => (int) $model->id,
            'provider_id'       => $model->provider_id,
            'note_type_id'      => $model->note_type_id,
            'body'              => $model->body,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
