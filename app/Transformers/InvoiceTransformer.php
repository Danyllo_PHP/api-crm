<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Invoice;

/**
 * Class InvoiceTransformer.
 *
 * @package namespace App\Transformers;
 */
class InvoiceTransformer extends TransformerAbstract
{
    /**
     * Transform the Invoice entity.
     *
     * @param \App\Entities\Invoice $model
     *
     * @return array
     */
    public function transform(Invoice $model)
    {
        return [
            'id'                => (int) $model->id,
            'price'             => $model->price,
            'invoice_status_id' => $model->invoice_status_id,
            'archived'          => $model->archived,
            'receipt_sent'      => $model->receipt_sent,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
