<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\AuditDelta;

/**
 * Class AuditDeltaTransformer.
 *
 * @package namespace App\Transformers;
 */
class AuditDeltaTransformer extends TransformerAbstract
{
    /**
     * Transform the AuditDelta entity.
     *
     * @param \App\Entities\AuditDelta $model
     *
     * @return array
     */
    public function transform(AuditDelta $model)
    {
        return [
            'id'                => (int) $model->id,
            'audit_id'          => $model->audit_id,
            'property_name'     => $model->property_name,
            'old_value'         => $model->old_value,
            'new_value'         => $model->new_value,
        ];
    }
}
