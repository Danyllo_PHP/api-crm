<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\OrdersStatusProvidersStatus1;

/**
 * Class OrdersStatusProvidersStatus1Transformer.
 *
 * @package namespace App\Transformers;
 */
class OrdersStatusProvidersStatus1Transformer extends TransformerAbstract
{
    /**
     * Transform the OrdersStatusProvidersStatus1 entity.
     *
     * @param \App\Entities\OrdersStatusProvidersStatus1 $model
     *
     * @return array
     */
    public function transform(OrdersStatusProvidersStatus1 $model)
    {
        return [
            'id'                => (int) $model->id,
            'order_status_id'   => $model->order_status_id,
            'provider_status_id'=> $model->provider_status_id,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
