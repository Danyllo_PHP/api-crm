<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\MessagesStatus;

/**
 * Class MessagesStatusTransformer.
 *
 * @package namespace App\Transformers;
 */
class MessagesStatusTransformer extends TransformerAbstract
{
    /**
     * Transform the MessagesStatus entity.
     *
     * @param \App\Entities\MessagesStatus $model
     *
     * @return array
     */
    public function transform(MessagesStatus $model)
    {
        return [
            'id'                => (int) $model->id,
            'name'              => $model->name,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
