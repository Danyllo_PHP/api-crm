<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Aco;

/**
 * Class AcoTransformer.
 *
 * @package namespace App\Transformers;
 */
class AcoTransformer extends TransformerAbstract
{
    /**
     * Transform the Aco entity.
     *
     * @param \App\Entities\Aco $model
     *
     * @return array
     */
    public function transform(Aco $model)
    {
        return [
            'id'            => (int) $model->id,
            'parent_id'     => $model->parent_id,
            'model'         => $model->model,
            'foreign_key'   => $model->foreign_key,
            'alias'         => $model->alias,
            'lft'           => $model->lft,
            'rght'          => $model->rght,
            '_show'         => $model->_show,
        ];
    }
}
