<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\RemittenceBank;

/**
 * Class RemittenceBankTransformer.
 *
 * @package namespace App\Transformers;
 */
class RemittenceBankTransformer extends TransformerAbstract
{
    /**
     * Transform the RemittenceBank entity.
     *
     * @param \App\Entities\RemittenceBank $model
     *
     * @return array
     */
    public function transform(RemittenceBank $model)
    {
        return [
            'id'                => (int) $model->id,
            'name'              => $model->name,
            'classname'         => $model->classname,
            'code'              => $model->code,
            'agreement'         => $model->agreement   ,
            'agency'            => $model->agency       ,
            'digit'             => $model->digit,
            'account'           => $model->account,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
