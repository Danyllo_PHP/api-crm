<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\ProgramsInterval;

/**
 * Class ProgramsIntervalTransformer.
 *
 * @package namespace App\Transformers;
 */
class ProgramsIntervalTransformer extends TransformerAbstract
{
    /**
     * Transform the ProgramsInterval entity.
     *
     * @param \App\Entities\ProgramsInterval $model
     *
     * @return array
     */
    public function transform(ProgramsInterval $model)
    {
        return [
            'id'                => (int) $model->id,
            'parent_id'         => $model->parent_id,
            'program_id'        => $model->program_id,
            'payment_form_id'   => $model->payment_form_id,
            'display_start'     => $model->display_start,
            'display_end'       => $model->display_end,
            'display_interval'  => $model->display_interval,
            'price'             => $model->price,
            'expire'            => $model->expire,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
