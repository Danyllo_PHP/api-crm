<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\ArosAco;

/**
 * Class ArosAcoTransformer.
 *
 * @package namespace App\Transformers;
 */
class ArosAcoTransformer extends TransformerAbstract
{
    /**
     * Transform the ArosAco entity.
     *
     * @param \App\Entities\ArosAco $model
     *
     * @return array
     */
    public function transform(ArosAco $model)
    {
        return [
            'id'         => (int) $model->id,
            'aro_id'     => $model->aro_id,
            'aco_id'     => $model->aco_id,
            '_create'    => $model->_create,
            '_read'      => $model->_read,
            '_update'    => $model->_update,
            '_delete'    => $model->_delete,
        ];
    }
}
