<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\OrdersRemittenceFile;

/**
 * Class OrdersRemittenceFileTransformer.
 *
 * @package namespace App\Transformers;
 */
class OrdersRemittenceFileTransformer extends TransformerAbstract
{
    /**
     * Transform the OrdersRemittenceFile entity.
     *
     * @param \App\Entities\OrdersRemittenceFile $model
     *
     * @return array
     */
    public function transform(OrdersRemittenceFile $model)
    {
        return [
            'id'                    => (int) $model->id,
            'order_id'              => $model->order_id,
            'remittence_file_id'    => $model->remittence_file_id,
            'observation'           => $model->observation,
            'created_by'            => $model->created_by,
            'modified_by'           => $model->modified_by,
            'created'               => $model->created->toDateTimeString(),
            'modified'              => $model->modified->toDateTimeString(),
        ];
    }
}
