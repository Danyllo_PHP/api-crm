<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\QuotationFile;

/**
 * Class QuotationFileTransformer.
 *
 * @package namespace App\Transformers;
 */
class QuotationFileTransformer extends TransformerAbstract
{
    /**
     * Transform the QuotationFile entity.
     *
     * @param \App\Entities\QuotationFile $model
     *
     * @return array
     */
    public function transform(QuotationFile $model)
    {
        return [
            'id'                => (int) $model->id,
            'quotation_id'      => $model->quotation_id,
            'dir'               => $model->dir,
            'file'              => $model->file,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
