<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\User;

/**
 * Class UserTransformer.
 *
 * @package namespace App\Transformers;
 */
class UserTransformer extends TransformerAbstract
{
    /**
     * Transform the User entity.
     *
     * @param \App\Entities\User $model
     *
     * @return array
     */
    public function transform(User $model)
    {
        return [
            'id'                   => (int) $model->id,
            'name'                 => $model->name,
            'email'                => $model->email,
            'username'             => $model->username,
            'password'             => $model->password,
            'created_by'           => $model->created_by,
            'modified_by'          => $model->modified_by,
            'group_id'             => $model->group_id,
            'branch'               => $model->branch,
            'photo'                => $model->photo,
            'photo_dir'            => $model->photo_dir,
            'active'               => $model->active,
            'activity_count'       => $model->activity_count,
            'comment_count'        => $model->comment_count,
            'quotation_count'      => $model->quotation_count,
            'task_count'           => $model->task_count,
            'group_manager'        => $model->group_manager,
            'last_access'          => $model->last_access,
            'miles_goals'          => $model->miles_goals,
            'email_verified_at'    => $model->email_verified_at,
            'created'              => $model->created->toDateTimeString(),
            'modified'             => $model->modified->toDateTimeString(),
        ];
    }
}
