<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\BanksProvidersSegment;

/**
 * Class BanksProvidersSegmentTransformer.
 *
 * @package namespace App\Transformers;
 */
class BanksProvidersSegmentTransformer extends TransformerAbstract
{
    /**
     * Transform the BanksProvidersSegment entity.
     *
     * @param \App\Entities\BanksProvidersSegment $model
     *
     * @return array
     */
    public function transform(BanksProvidersSegment $model)
    {
        return [
            'id'            => (int) $model->id,
            'bank_id'       => $model->bank_id,
            'segment_id'    => $model->segment_id,
            'provider_id'   => $model->provider_id,
            'agency'        => $model->agency,
            'type'          => $model->type,
            'account'       => $model->account,
            'operation'     => $model->operation,
            'account_digit' => $model->account_digit,
            'agency_digit'  => $model->agency_digit,
            'main'          => $model->main,
            'created_by'    => $model->created_by,
            'modified_by'   => $model->modified_by,
            'created'       => $model->created->toDateTimeString(),
            'modified'      => $model->modified->toDateTimeString(),
        ];
    }
}
