<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\ActionsUser;

/**
 * Class ActionsUserTransformer.
 *
 * @package namespace App\Transformers;
 */
class ActionsUserTransformer extends TransformerAbstract
{
    /**
     * Transform the ActionsUser entity.
     *
     * @param \App\Entities\ActionsUser $model
     *
     * @return array
     */
    public function transform(ActionsUser $model)
    {
        return [
            'id'            => (int) $model->id,
            'action_id'     => $model->action_id,
            'user_id'       => $model->user_id,
            'created_by'    => $model->created_by,
            'modified_by'   => $model->modified_by,
            'created'       => $model->created->toDataTimeString(),
            'modified'      => $model->modified->toDataTimeString(),
        ];
    }
}
