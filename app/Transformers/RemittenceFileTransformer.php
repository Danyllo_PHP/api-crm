<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\RemittenceFile;

/**
 * Class RemittenceFileTransformer.
 *
 * @package namespace App\Transformers;
 */
class RemittenceFileTransformer extends TransformerAbstract
{
    /**
     * Transform the RemittenceFile entity.
     *
     * @param \App\Entities\RemittenceFile $model
     *
     * @return array
     */
    public function transform(RemittenceFile $model)
    {
        return [
            'id'                    => (int) $model->id,
            'url'                   => $model->url,
            'remittence_banks_id'   => $model->remittence_banks_id,
            'created_by'            => $model->created_by,
            'created'               => $model->created->toDateTimeString(),
            'modified'              => $model->modified->toDateTimeString(),
        ];
    }
}
