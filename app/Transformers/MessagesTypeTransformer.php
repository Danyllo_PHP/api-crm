<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\MessagesType;

/**
 * Class MessagesTypeTransformer.
 *
 * @package namespace App\Transformers;
 */
class MessagesTypeTransformer extends TransformerAbstract
{
    /**
     * Transform the MessagesType entity.
     *
     * @param \App\Entities\MessagesType $model
     *
     * @return array
     */
    public function transform(MessagesType $model)
    {
        return [
            'id'                => (int) $model->id,
            'name'              => $model->name,
        ];
    }
}
