<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\ProvidersOccupation;

/**
 * Class ProvidersOccupationTransformer.
 *
 * @package namespace App\Transformers;
 */
class ProvidersOccupationTransformer extends TransformerAbstract
{
    /**
     * Transform the ProvidersOccupation entity.
     *
     * @param \App\Entities\ProvidersOccupation $model
     *
     * @return array
     */
    public function transform(ProvidersOccupation $model)
    {
        return [
            'id'                => (int) $model->id,
            'title'             => $model->title,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
