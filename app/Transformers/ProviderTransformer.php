<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Provider;

/**
 * Class ProviderTransformer.
 *
 * @package namespace App\Transformers;
 */
class ProviderTransformer extends TransformerAbstract
{
    /**
     * Transform the Provider entity.
     *
     * @param \App\Entities\Provider $model
     *
     * @return array
     */
    public function transform(Provider $model)
    {
        return [
            'id'                        => (int) $model->id,
            'provider_status_id'        => $model->provider_status_id,
            'user_id'                   => $model->user_id,
            'parent_id'                 => $model->parent_id,
            'provider_occupation_id'    => $model->provider_occupation_id,
            'name'                      => $model->name,
            'email'                     => $model->email,
            'document'                  => $model->document,
            'cpf'                       => $model->cpf,
            'score_id'                  => $model->score_id,
            'score_value'               => $model->score_value,
            'score_status'              => $model->score_status,
            'score_updated'             => $model->score_updated,
            'phone'                     => $model->phone,
            'cellphone'                 => $model->cellphone,
            'company'                   => $model->company,
            'occupation'                => $model->occupation,
            'company_email'             => $model->company_email,
            'company_phone'             => $model->company_phone,
            'company_phone_branch'      => $model->company_phone_branch,
            'comment'                   => $model->comment,
            'payment_count'             => $model->payment_count,
            'quotation_count'           => $model->quotation_count,
            'provider_file_count'       => $model->provider_file_count,
            'rg'                        => $model->rg,
            'birthday'                  => $model->birthday,
            'gender'                    => $model->gender,
            'street_view'               => $model->street_view,
            'facebook'                  => $model->facebook,
            'linkedin'                  => $model->linkedin,
            'restriction'               => $model->restriction,
            'company_site'              => $model->company_site,
            'orders_count'              => $model->orders_count,
            'order_status_id'           => $model->order_status_id,
            'payment_form_id'           => $model->payment_form_id,
            'cnpj'                      => $model->cnpj,
            'company_extra'             => $model->company_extra,
            'status_modified'           => $model->status_modified,
            'session_id'                => $model->session_id,
            'was_consulted'             => $model->was_consulted,
            'was_consulted_spc'         => $model->was_consulted_spc,
            'last_quotation_id'         => $model->last_quotation_id,
            'created_by'                => $model->created_by,
            'modified_by'               => $model->modified_by,
            'created'                   => $model->created->toDateTimeString(),
            'modified'                  => $model->modified->toDateTimeString(),
        ];
    }
}
