<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\ProvidersStatusPaymentForm;

/**
 * Class ProvidersStatusPaymentFormTransformer.
 *
 * @package namespace App\Transformers;
 */
class ProvidersStatusPaymentFormTransformer extends TransformerAbstract
{
    /**
     * Transform the ProvidersStatusPaymentForm entity.
     *
     * @param \App\Entities\ProvidersStatusPaymentForm $model
     *
     * @return array
     */
    public function transform(ProvidersStatusPaymentForm $model)
    {
        return [
            'id'                => (int) $model->id,
            'provider_status_id'=> $model->provider_status_id,
            'payment_form_id'   => $model->payment_form_id,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
