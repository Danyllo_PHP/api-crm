<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\StockLog;

/**
 * Class StockLogTransformer.
 *
 * @package namespace App\Transformers;
 */
class StockLogTransformer extends TransformerAbstract
{
    /**
     * Transform the StockLog entity.
     *
     * @param \App\Entities\StockLog $model
     *
     * @return array
     */
    public function transform(StockLog $model)
    {
        return [
            'id'                => (int) $model->id,
            'transaction_type'  => $model->transaction_type,
            'price'             => $model->price,
            'miles'             => $model->miles,
            'validity'          => $model->validity,
            'stock_id'          => $model->stock_id,
            'user_id'           => $model->user_id,
            'payment_type'      => $model->payment_type,
            'wintour_sale'      => $model->wintour_sale,
            'refunded'          => $model->refunded,
            'miles_remaining'   => $model->miles_remaining,
            'user_type'         => $model->user_type,
            'emission_id'       => $model->emission_id,
        ];
    }
}
