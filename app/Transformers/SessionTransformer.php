<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Session;

/**
 * Class SessionTransformer.
 *
 * @package namespace App\Transformers;
 */
class SessionTransformer extends TransformerAbstract
{
    /**
     * Transform the Session entity.
     *
     * @param \App\Entities\Session $model
     *
     * @return array
     */
    public function transform(Session $model)
    {
        return [
            'id'         => (int) $model->id,
            'data'       => $model->data,
            'user_id'    => $model->user_id,
            'expires'    => $model->expires,
            'created'    => $model->created->toDateTimeString(),
            'modified'   => $model->modified->toDateTimeString(),
        ];
    }
}
