<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Calendar;

/**
 * Class CalendarTransformer.
 *
 * @package namespace App\Transformers;
 */
class CalendarTransformer extends TransformerAbstract
{
    /**
     * Transform the Calendar entity.
     *
     * @param \App\Entities\Calendar $model
     *
     * @return array
     */
    public function transform(Calendar $model)
    {
        return [
            'id'            => (int) $model->id,
            'date'          => $model->date->toDataString(),
            'created_by'    => $model->created_by,
            'modified_by'   => $model->modified_by,
            'created'       => $model->created->toDateTimeString(),
            'modified'      => $model->modified->toDateTimeString(),
        ];
    }
}
