<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Action;

/**
 * Class ActionTransformer.
 *
 * @package namespace App\Transformers;
 */
class ActionTransformer extends TransformerAbstract
{
    /**
     * Transform the Action entity.
     *
     * @param \App\Entities\Action $model
     *
     * @return array
     */
    public function transform(Action $model)
    {
        return [
            'id'            => (int) $model->id,
            'parent_id'     => $model->parent_id,
            'title'         => $model->title,
            'lft'           => $model->lft,
            'rght'          => $model->rght,
            'created_by'    => $model->created_by,
            'modified_by'   => $model->modified_by,
            'created'       => $model->created->toDataTimeString(),
            'modified'      => $model->modified->toDataTimeString(),
        ];
    }
}
