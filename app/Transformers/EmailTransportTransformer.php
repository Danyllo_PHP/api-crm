<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\EmailTransport;

/**
 * Class EmailTransportTransformer.
 *
 * @package namespace App\Transformers;
 */
class EmailTransportTransformer extends TransformerAbstract
{
    /**
     * Transform the EmailTransport entity.
     *
     * @param \App\Entities\EmailTransport $model
     *
     * @return array
     */
    public function transform(EmailTransport $model)
    {
        return [
            'id'                => (int) $model->id,
            'name'              => $model->name,
            'class_name'        => $model->class_name,
            'host'              => $model->host,
            'port'              => $model->port,
            'username'          => $model->username,
            'client'            => $model->client,
            'tls'               => $model->tls,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
