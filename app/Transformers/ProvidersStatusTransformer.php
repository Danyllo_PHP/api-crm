<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\ProvidersStatus;

/**
 * Class ProvidersStatusTransformer.
 *
 * @package namespace App\Transformers;
 */
class ProvidersStatusTransformer extends TransformerAbstract
{
    /**
     * Transform the ProvidersStatus entity.
     *
     * @param \App\Entities\ProvidersStatus $model
     *
     * @return array
     */
    public function transform(ProvidersStatus $model)
    {
        return [
            'id'                => (int) $model->id,
            'title'             => $model->title,
            'provider_create'   => $model->provider_create,
            'has_comment'       => $model->has_comment,
            'class'             => $model->class,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
