<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\QuotationStatus;

/**
 * Class QuotationStatusTransformer.
 *
 * @package namespace App\Transformers;
 */
class QuotationStatusTransformer extends TransformerAbstract
{
    /**
     * Transform the QuotationStatus entity.
     *
     * @param \App\Entities\QuotationStatus $model
     *
     * @return array
     */
    public function transform(QuotationStatus $model)
    {
        return [
            'id'                => (int) $model->id,
            'title'             => $model->title,
            'quotation_create'  => $model->quotation_create,
            'has_comment'       => $model->has_comment,
            'class'             => $model->class,
            'created_by'        => $model->created_by,
            'modified_by'       => $model->modified_by,
            'created'           => $model->created->toDateTimeString(),
            'modified'          => $model->modified->toDateTimeString(),
        ];
    }
}
