<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Stock;

/**
 * Class StockTransformer.
 *
 * @package namespace App\Transformers;
 */
class StockTransformer extends TransformerAbstract
{
    /**
     * Transform the Stock entity.
     *
     * @param \App\Entities\Stock $model
     *
     * @return array
     */
    public function transform(Stock $model)
    {
        return [
            'id'                => (int) $model->id,
            'company_initials'  => $model->company_initials,
            'miles'             => $model->miles,
            'card_number'       => $model->card_number,
            'digital_signature' => $model->digital_signature,
            'provider_id'       => $model->provider_id,
            'payment_form'      => $model->payment_form,
            'last_lot_value'    => $model->last_lot_value,
            'category'          => $model->category,
            'check_statements'  => $model->check_statements,
            'can_use_stock'     => $model->can_use_stock,
            'access_password'   => $model->access_password,
        ];
    }
}
