<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\TaskService;
use App\Validators\TaskValidator;

/**
 * Class TasksController.
 *
 * @package namespace App\Http\Controllers;
 */
class TasksController extends Controller
{
    use CrudMethods;

    /**
     * @var TaskService
     */
    protected $service;

    /**
     * @var TaskValidator
     */
    protected $validator;

    /**
     * TasksController constructor.
     *
     * @param TaskService $service
     * @param TaskValidator $validator
     */
    public function __construct(TaskService $service, TaskValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
