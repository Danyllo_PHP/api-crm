<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\GroupService;
use App\Validators\GroupValidator;

/**
 * Class GroupsController.
 *
 * @package namespace App\Http\Controllers;
 */
class GroupsController extends Controller
{
    use CrudMethods;

    /**
     * @var GroupService
     */
    protected $service;

    /**
     * @var GroupValidator
     */
    protected $validator;

    /**
     * GroupsController constructor.
     *
     * @param GroupService $service
     * @param GroupValidator $validator
     */
    public function __construct(GroupService $service, GroupValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
