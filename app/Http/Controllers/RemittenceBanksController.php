<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\RemittenceBankService;
use App\Validators\RemittenceBankValidator;

/**
 * Class RemittenceBanksController.
 *
 * @package namespace App\Http\Controllers;
 */
class RemittenceBanksController extends Controller
{

    use CrudMethods;

    /**
     * @var RemittenceBankService
     */
    protected $service;

    /**
     * @var RemittenceBankValidator
     */
    protected $validator;

    /**
     * RemittenceBanksController constructor.
     *
     * @param RemittenceBankService $service
     * @param RemittenceBankValidator $validator
     */
    public function __construct(RemittenceBankService $service, RemittenceBankValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
