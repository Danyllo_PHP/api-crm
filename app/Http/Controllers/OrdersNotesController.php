<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\OrdersNoteService;
use App\Validators\OrdersNoteValidator;

/**
 * Class OrdersNotesController.
 *
 * @package namespace App\Http\Controllers;
 */
class OrdersNotesController extends Controller
{
    use CrudMethods;

    /**
     * @var OrdersNoteService
     */
    protected $service;

    /**
     * @var OrdersNoteValidator
     */
    protected $validator;

    /**
     * OrdersNotesController constructor.
     *
     * @param OrdersNoteService $service
     * @param OrdersNoteValidator $validator
     */
    public function __construct(OrdersNoteService $service, OrdersNoteValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
