<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Traits\CrudMethods;

use App\Services\ActionsUserService;
use App\Validators\ActionsUserValidator;

/**
 * Class ActionsUsersController.
 *
 * @package namespace App\Http\Controllers;
 */
class ActionsUsersController extends Controller
{
    use CrudMethods;

    /**
     * @var ActionsUserService
     */
    protected $service;

    /**
     * @var ActionsUserValidator
     */
    protected $validator;

    /**
     * ActionsUsersController constructor.
     *
     * @param ActionsUserService $service
     * @param ActionsUserValidator $validator
     */
    public function __construct(ActionsUserService $service, ActionsUserValidator $validator)
    {
        $this->service = $service;
        $this->validator = $validator;
    }

}
