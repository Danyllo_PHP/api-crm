<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\PhoneCallService;
use App\Validators\PhoneCallValidator;

/**
 * Class PhoneCallsController.
 *
 * @package namespace App\Http\Controllers;
 */
class PhoneCallsController extends Controller
{
    use CrudMethods;

    /**
     * @var PhoneCallService
     */
    protected $service;

    /**
     * @var PhoneCallValidator
     */
    protected $validator;

    /**
     * PhoneCallsController constructor.
     *
     * @param PhoneCallService $service
     * @param PhoneCallValidator $validator
     */
    public function __construct(PhoneCallService $service, PhoneCallValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
