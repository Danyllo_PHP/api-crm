<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\CommentService;
use App\Validators\CommentValidator;

/**
 * Class CommentsController.
 *
 * @package namespace App\Http\Controllers;
 */
class CommentsController extends Controller
{
    use CrudMethods;

    /**
     * @var CommentService
     */
    protected $service;

    /**
     * @var CommentValidator
     */
    protected $validator;

    /**
     * CommentsController constructor.
     *
     * @param CommentService $service
     * @param CommentValidator $validator
     */
    public function __construct(CommentService $service, CommentValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
