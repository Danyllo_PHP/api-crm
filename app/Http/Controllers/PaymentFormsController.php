<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\PaymentFormService;
use App\Validators\PaymentFormValidator;

/**
 * Class PaymentFormsController.
 *
 * @package namespace App\Http\Controllers;
 */
class PaymentFormsController extends Controller
{
    use CrudMethods;

    /**
     * @var PaymentFormService
     */
    protected $service;

    /**
     * @var PaymentFormValidator
     */
    protected $validator;

    /**
     * PaymentFormsController constructor.
     *
     * @param PaymentFormService $service
     * @param PaymentFormValidator $validator
     */
    public function __construct(PaymentFormService $service, PaymentFormValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
