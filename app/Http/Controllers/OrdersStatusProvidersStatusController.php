<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\OrdersStatusProvidersStatus1Service;
use App\Validators\OrdersStatusProvidersStatus1Validator;

/**
 * Class OrdersStatusProvidersStatusController.
 *
 * @package namespace App\Http\Controllers;
 */
class OrdersStatusProvidersStatusController extends Controller
{
    use CrudMethods;

    /**
     * @var OrdersStatusProvidersStatus1Service
     */
    protected $service;

    /**
     * @var OrdersStatusProvidersStatus1Validator
     */
    protected $validator;

    /**
     * OrdersStatusProvidersStatusController constructor.
     *
     * @param OrdersStatusProvidersStatus1Service $service
     * @param OrdersStatusProvidersStatus1Validator $validator
     */
    public function __construct(OrdersStatusProvidersStatus1Service $service, OrdersStatusProvidersStatus1Validator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
