<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\MessagesReplyService;
use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\MessagesReplyCreateRequest;
use App\Http\Requests\MessagesReplyUpdateRequest;
use App\Repositories\MessagesReplyRepository;
use App\Validators\MessagesReplyValidator;

/**
 * Class MessagesRepliesController.
 *
 * @package namespace App\Http\Controllers;
 */
class MessagesRepliesController extends Controller
{
    use CrudMethods;

    /**
     * @var MessagesReplyService
     */
    protected $service;

    /**
     * @var MessagesReplyValidator
     */
    protected $validator;

    /**
     * MessagesRepliesController constructor.
     *
     * @param MessagesReplyService $service
     * @param MessagesReplyValidator $validator
     */
    public function __construct(MessagesReplyService $service, MessagesReplyValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
