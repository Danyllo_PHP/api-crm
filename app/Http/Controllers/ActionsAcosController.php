<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Traits\CrudMethods;

use App\Validators\ActionsAcoValidator;
use Illuminate\Http\Request;
use App\Services\ActionsAcoService;
/**
 * Class ActionsAcosController.
 *
 * @package namespace App\Http\Controllers;
 */
class ActionsAcosController extends Controller
{

    use CrudMethods;
    /**
     * @var $service
     */
    protected $service;

    /**
     * @var ActionsAcoValidator
     */
    protected $validator;

    /**
     * ActionsAcosController constructor.
     *
     * @param ActionsAcoService $service
     * @param ActionsAcoValidator $validator
     */
    public function __construct(ActionsAcoService $service, ActionsAcoValidator $validator)
    {
        $this->service = $service;
        $this->validator = $validator;
    }

}
