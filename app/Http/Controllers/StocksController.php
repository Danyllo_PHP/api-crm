<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\StockService;
use App\Validators\StockValidator;

/**
 * Class StocksController.
 *
 * @package namespace App\Http\Controllers;
 */
class StocksController extends Controller
{

    use CrudMethods;

    /**
     * @var StockService
     */
    protected $service;

    /**
     * @var StockValidator
     */
    protected $validator;

    /**
     * StocksController constructor.
     *
     * @param StockService $service
     * @param StockValidator $validator
     */
    public function __construct(StockService $service, StockValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
