<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\PaymentsStatusService;
use App\Validators\PaymentsStatusValidator;

/**
 * Class PaymentsStatusController.
 *
 * @package namespace App\Http\Controllers;
 */
class PaymentsStatusController extends Controller
{
    use CrudMethods;

    /**
     * @var PaymentsStatusService
     */
    protected $service;

    /**
     * @var PaymentsStatusValidator
     */
    protected $validator;

    /**
     * PaymentsStatusController constructor.
     *
     * @param PaymentsStatusService $service
     * @param PaymentsStatusValidator $validator
     */
    public function __construct(PaymentsStatusService $service, PaymentsStatusValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
