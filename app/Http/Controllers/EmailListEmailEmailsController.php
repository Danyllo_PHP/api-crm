<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\EmailListEmailEmailService;
use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\EmailListEmailEmailCreateRequest;
use App\Http\Requests\EmailListEmailEmailUpdateRequest;
use App\Repositories\EmailListEmailEmailRepository;
use App\Validators\EmailListEmailEmailValidator;

/**
 * Class EmailListEmailEmailsController.
 *
 * @package namespace App\Http\Controllers;
 */
class EmailListEmailEmailsController extends Controller
{
    use CrudMethods;

    /**
     * @var EmailListEmailEmailService
     */
    protected $service;

    /**
     * @var EmailListEmailEmailValidator
     */
    protected $validator;

    /**
     * EmailListEmailEmailsController constructor.
     *
     * @param EmailListEmailEmailService $service
     * @param EmailListEmailEmailValidator $validator
     */
    public function __construct(EmailListEmailEmailService $service, EmailListEmailEmailValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
