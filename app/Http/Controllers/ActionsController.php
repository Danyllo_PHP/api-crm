<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\ActionService;
use App\Validators\ActionValidator;

/**
 * Class ActionsController.
 *
 * @package namespace App\Http\Controllers;
 */
class ActionsController extends Controller
{

    use CrudMethods;

    /**
     * @var ActionService
     */
    protected $service;

    /**
     * @var ActionValidator
     */
    protected $validator;

    /**
     * ActionsController constructor.
     *
     * @param ActionService $service
     * @param ActionValidator $validator
     */
    public function __construct(ActionService $service,  ActionValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }

}
