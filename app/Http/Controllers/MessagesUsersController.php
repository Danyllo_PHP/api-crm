<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\MessagesUserService;
use App\Validators\MessagesUserValidator;

/**
 * Class MessagesUsersController.
 *
 * @package namespace App\Http\Controllers;
 */
class MessagesUsersController extends Controller
{
    use CrudMethods;

    /**
     * @var MessagesUserService
     */
    protected $service;

    /**
     * @var MessagesUserValidator
     */
    protected $validator;

    /**
     * MessagesUsersController constructor.
     *
     * @param MessagesUserService $service
     * @param MessagesUserValidator $validator
     */
    public function __construct(MessagesUserService $service, MessagesUserValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
