<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\ProvidersFileService;
use App\Validators\ProvidersFileValidator;

/**
 * Class ProvidersFilesController.
 *
 * @package namespace App\Http\Controllers;
 */
class ProvidersFilesController extends Controller
{
    use CrudMethods;

    /**
     * @var ProvidersFileService
     */
    protected $service;

    /**
     * @var ProvidersFileValidator
     */
    protected $validator;

    /**
     * ProvidersFilesController constructor.
     *
     * @param ProvidersFileService $service
     * @param ProvidersFileValidator $validator
     */
    public function __construct(ProvidersFileService $service, ProvidersFileValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
