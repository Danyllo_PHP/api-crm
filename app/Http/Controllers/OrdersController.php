<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\OrderService;
use App\Validators\OrderValidator;

/**
 * Class OrdersController.
 *
 * @package namespace App\Http\Controllers;
 */
class OrdersController extends Controller
{
    use CrudMethods;

    /**
     * @var OrderService
     */
    protected $service;

    /**
     * @var OrderValidator
     */
    protected $validator;

    /**
     * OrdersController constructor.
     *
     * @param OrderService $service
     * @param OrderValidator $validator
     */
    public function __construct(OrderService $service, OrderValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
