<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\EmailCampaignService;
use App\Validators\EmailCampaignValidator;

/**
 * Class EmailCampaignsController.
 *
 * @package namespace App\Http\Controllers;
 */
class EmailCampaignsController extends Controller
{
    use CrudMethods;

    /**
     * @var EmailCampaignService
     */
    protected $service;

    /**
     * @var EmailCampaignValidator
     */
    protected $validator;

    /**
     * EmailCampaignsController constructor.
     *
     * @param EmailCampaignService $service
     * @param EmailCampaignValidator $validator
     */
    public function __construct(EmailCampaignService $service, EmailCampaignValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
