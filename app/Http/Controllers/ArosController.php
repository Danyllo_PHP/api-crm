<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\AroService;
use App\Validators\AroValidator;

/**
 * Class ArosController.
 *
 * @package namespace App\Http\Controllers;
 */
class ArosController extends Controller
{
    use CrudMethods;

    /**
     * @var AroService
     */
    protected $service;

    /**
     * @var AroValidator
     */
    protected $validator;

    /**
     * ArosController constructor.
     *
     * @param AroService $service
     * @param AroValidator $validator
     */
    public function __construct(AroService $service, AroValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }

}
