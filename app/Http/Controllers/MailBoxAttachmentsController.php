<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\MailBoxAttachmentService;
use App\Validators\MailBoxAttachmentValidator;

/**
 * Class MailBoxAttachmentsController.
 *
 * @package namespace App\Http\Controllers;
 */
class MailBoxAttachmentsController extends Controller
{
    use CrudMethods;

    /**
     * @var MailBoxAttachmentService
     */
    protected $service;

    /**
     * @var MailBoxAttachmentValidator
     */
    protected $validator;

    /**
     * MailBoxAttachmentsController constructor.
     *
     * @param MailBoxAttachmentService $service
     * @param MailBoxAttachmentValidator $validator
     */
    public function __construct(MailBoxAttachmentService $service, MailBoxAttachmentValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
