<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\BanksProvidersSegmentService;
use App\Validators\BanksProvidersSegmentValidator;

/**
 * Class BanksProvidersSegmentsController.
 *
 * @package namespace App\Http\Controllers;
 */
class BanksProvidersSegmentsController extends Controller
{

    use CrudMethods;

    /**
     * @var BanksProvidersSegmentService
     */
    protected $service;

    /**
     * @var BanksProvidersSegmentValidator
     */
    protected $validator;

    /**
     * BanksProvidersSegmentsController constructor.
     *
     * @param BanksProvidersSegmentService $service
     * @param BanksProvidersSegmentValidator $validator
     */
    public function __construct(BanksProvidersSegmentService $service, BanksProvidersSegmentValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
