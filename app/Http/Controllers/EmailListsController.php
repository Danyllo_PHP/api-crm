<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\EmailListService;
use App\Validators\EmailListValidator;

/**
 * Class EmailListsController.
 *
 * @package namespace App\Http\Controllers;
 */
class EmailListsController extends Controller
{
    use CrudMethods;

    /**
     * @var EmailListService
     */
    protected $service;

    /**
     * @var EmailListValidator
     */
    protected $validator;

    /**
     * EmailListsController constructor.
     *
     * @param EmailListService $service
     * @param EmailListValidator $validator
     */
    public function __construct(EmailListService $service, EmailListValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
