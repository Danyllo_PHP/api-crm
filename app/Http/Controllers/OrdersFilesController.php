<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\OrdersFileService;
use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\OrdersFileCreateRequest;
use App\Http\Requests\OrdersFileUpdateRequest;
use App\Repositories\OrdersFileRepository;
use App\Validators\OrdersFileValidator;

/**
 * Class OrdersFilesController.
 *
 * @package namespace App\Http\Controllers;
 */
class OrdersFilesController extends Controller
{
    use CrudMethods;

    /**
     * @var OrdersFileService
     */
    protected $service;

    /**
     * @var OrdersFileValidator
     */
    protected $validator;

    /**
     * OrdersFilesController constructor.
     *
     * @param OrdersFileService $repository
     * @param OrdersFileValidator $validator
     */
    public function __construct(OrdersFileService $service, OrdersFileValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
