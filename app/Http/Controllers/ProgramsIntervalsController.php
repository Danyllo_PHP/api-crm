<?php

    namespace App\Http\Controllers;

    use App\Http\Controllers\Traits\CrudMethods;
    use App\Services\ProgramsIntervalService;
    use App\Validators\ProgramsIntervalValidator;

    /**
     * Class ProgramsIntervalsController.
     *
     * @package namespace App\Http\Controllers;
     */
    class ProgramsIntervalsController extends Controller
    {
        use CrudMethods;

        /**
         * @var ProgramsIntervalService
         */
        protected $service;

        /**
         * @var ProgramsIntervalValidator
         */
        protected $validator;

        /**
         * ProgramsIntervalsController constructor.
         *
         * @param ProgramsIntervalService   $service
         * @param ProgramsIntervalValidator $validator
         */
        public function __construct(ProgramsIntervalService $service, ProgramsIntervalValidator $validator)
        {
            $this->service = $service;
            $this->validator = $validator;
        }
    }
