<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\FidelityService;
use App\Validators\FidelityValidator;

/**
 * Class FidelitiesController.
 *
 * @package namespace App\Http\Controllers;
 */
class FidelitiesController extends Controller
{
    use CrudMethods;

    /**
     * @var FidelityService
     */
    protected $service;

    /**
     * @var FidelityValidator
     */
    protected $validator;

    /**
     * FidelitiesController constructor.
     *
     * @param FidelityService $service
     * @param FidelityValidator $validator
     */
    public function __construct(FidelityService $service, FidelityValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
