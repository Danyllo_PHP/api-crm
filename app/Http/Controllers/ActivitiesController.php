<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Traits\CrudMethods;


use App\Services\ActivityService;
use App\Validators\ActivityValidator;

/**
 * Class ActivitiesController.
 *
 * @package namespace App\Http\Controllers;
 */
class ActivitiesController extends Controller
{
    use CrudMethods;

    /**
     * @var ActivityService
     */
    protected $service;

    /**
     * @var ActivityValidator
     */
    protected $validator;

    /**
     * ActivitiesController constructor.
     *
     * @param ActivityService $service
     * @param ActivityValidator $validator
     */
    public function __construct(ActivityService $service, ActivityValidator $validator)
    {
        $this->service = $service;
        $this->validator = $validator;
    }
}
