<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\MessagesTypeService;
use App\Validators\MessagesTypeValidator;

/**
 * Class MessagesTypesController.
 *
 * @package namespace App\Http\Controllers;
 */
class MessagesTypesController extends Controller
{
    use CrudMethods;

    /**
     * @var MessagesTypeService
     */
    protected $service;

    /**
     * @var MessagesTypeValidator
     */
    protected $validator;

    /**
     * MessagesTypesController constructor.
     *
     * @param MessagesTypeService $service
     * @param MessagesTypeValidator $validator
     */
    public function __construct(MessagesTypeService $service, MessagesTypeValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
