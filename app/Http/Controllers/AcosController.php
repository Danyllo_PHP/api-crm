<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\AcoService ;
use Illuminate\Http\Request;
use App\Validators\AcoValidator;

/**
 * Class AcosController.
 *
 * @package namespace App\Http\Controllers;
 */
class AcosController extends Controller
{
    use CrudMethods;
    /**
     * @var AcoService
     */
    protected $service;

    /**
     * @var AcoValidator
     */
    protected $validator;


    /**
     * AcosController constructor.
     *
     * @param AcoService $service
     * @param AcoValidator $validator
     */
    public function __construct(AcoService $service, AcoValidator $validator)
    {
        $this->service = $service;
        $this->validator = $validator;
    }

}
