<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\ArosAcoService;
use App\Validators\ArosAcoValidator;

/**
 * Class ArosAcosController.
 *
 * @package namespace App\Http\Controllers;
 */
class ArosAcosController extends Controller
{
    use CrudMethods;

    /**
     * @var ArosAcoService
     */
    protected $service;

    /**
     * @var ArosAcoValidator
     */
    protected $validator;

    /**
     * ArosAcosController constructor.
     *
     * @param ArosAcoService $service
     * @param ArosAcoValidator $validator
     */
    public function __construct(ArosAcoService $service, ArosAcoValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }

}
