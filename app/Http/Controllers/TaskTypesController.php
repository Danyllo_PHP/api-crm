<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\TaskTypeService;
use App\Validators\TaskTypeValidator;

/**
 * Class TaskTypesController.
 *
 * @package namespace App\Http\Controllers;
 */
class TaskTypesController extends Controller
{

    use CrudMethods;

    /**
     * @var TaskTypeService
     */
    protected $service;

    /**
     * @var TaskTypeValidator
     */
    protected $validator;

    /**
     * TaskTypesController constructor.
     *
     * @param TaskTypeService $service
     * @param TaskTypeValidator $validator
     */
    public function __construct(TaskTypeService $service, TaskTypeValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
