<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\PaymentsProgramService;
use App\Validators\PaymentsProgramValidator;

/**
 * Class PaymentsProgramsController.
 *
 * @package namespace App\Http\Controllers;
 */
class PaymentsProgramsController extends Controller
{
    use CrudMethods;

    /**
     * @var PaymentsProgramService
     */
    protected $service;

    /**
     * @var PaymentsProgramValidator
     */
    protected $validator;

    /**
     * PaymentsProgramsController constructor.
     *
     * @param PaymentsProgramService $service
     * @param PaymentsProgramValidator $validator
     */
    public function __construct(PaymentsProgramService $service, PaymentsProgramValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
