<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\StockLogService;
use App\Validators\StockLogValidator;

/**
 * Class StockLogsController.
 *
 * @package namespace App\Http\Controllers;
 */
class StockLogsController extends Controller
{

    use CrudMethods;

    /**
     * @var StockLogService
     */
    protected $service;

    /**
     * @var StockLogValidator
     */
    protected $validator;

    /**
     * StockLogsController constructor.
     *
     * @param StockLogService $service
     * @param StockLogValidator $validator
     */
    public function __construct(StockLogService $service, StockLogValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
