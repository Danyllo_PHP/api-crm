<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\ProvidersOccupationService;
use App\Validators\ProvidersOccupationValidator;

/**
 * Class ProvidersOccupationsController.
 *
 * @package namespace App\Http\Controllers;
 */
class ProvidersOccupationsController extends Controller
{

    use CrudMethods;

    /**
     * @var ProvidersOccupationService
     */
    protected $service;

    /**
     * @var ProvidersOccupationValidator
     */
    protected $validator;

    /**
     * ProvidersOccupationsController constructor.
     *
     * @param ProvidersOccupationService $service
     * @param ProvidersOccupationValidator $validator
     */
    public function __construct(ProvidersOccupationService $service, ProvidersOccupationValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
