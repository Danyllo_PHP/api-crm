<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\InvoiceStatusService;
use App\Validators\InvoiceStatusValidator;

/**
 * Class InvoiceStatusesController.
 *
 * @package namespace App\Http\Controllers;
 */
class InvoiceStatusesController extends Controller
{
    use CrudMethods;

    /**
     * @var InvoiceStatusService
     */
    protected $service;

    /**
     * @var InvoiceStatusValidator
     */
    protected $validator;

    /**
     * InvoiceStatusesController constructor.
     *
     * @param InvoiceStatusService $service
     * @param InvoiceStatusValidator $validator
     */
    public function __construct(InvoiceStatusService $service, InvoiceStatusValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
