<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\PendingEditionService;
use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\PendingEditionCreateRequest;
use App\Http\Requests\PendingEditionUpdateRequest;
use App\Repositories\PendingEditionRepository;
use App\Validators\PendingEditionValidator;

/**
 * Class PendingEditionsController.
 *
 * @package namespace App\Http\Controllers;
 */
class PendingEditionsController extends Controller
{
    use CrudMethods;

    /**
     * @var PendingEditionService
     */
    protected $service;

    /**
     * @var PendingEditionValidator
     */
    protected $validator;

    /**
     * PendingEditionsController constructor.
     *
     * @param PendingEditionService $service
     * @param PendingEditionValidator $validator
     */
    public function __construct(PendingEditionService $service, PendingEditionValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
