<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\RbruteforceService;
use App\Validators\RbruteforceValidator;

/**
 * Class RbruteforcesController.
 *
 * @package namespace App\Http\Controllers;
 */
class RbruteforcesController extends Controller
{

    use CrudMethods;

    /**
     * @var RbruteforceService
     */
    protected $service;

    /**
     * @var RbruteforceValidator
     */
    protected $validator;

    /**
     * RbruteforcesController constructor.
     *
     * @param RbruteforceService $service
     * @param RbruteforceValidator $validator
     */
    public function __construct(RbruteforceService $service, RbruteforceValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
