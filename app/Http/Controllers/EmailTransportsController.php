<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\EmailTransportService;
use App\Validators\EmailTransportValidator;

/**
 * Class EmailTransportsController.
 *
 * @package namespace App\Http\Controllers;
 */
class EmailTransportsController extends Controller
{
    use CrudMethods;

    /**
     * @var EmailTransportService
     */
    protected $service;

    /**
     * @var EmailTransportValidator
     */
    protected $validator;

    /**
     * EmailTransportsController constructor.
     *
     * @param EmailTransportService $service
     * @param EmailTransportValidator $validator
     */
    public function __construct(EmailTransportService $service, EmailTransportValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
