<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\ProviderStatusService;
use App\Validators\ProvidersStatusValidator;

/**
 * Class ProvidersStatusController.
 *
 * @package namespace App\Http\Controllers;
 */
class ProvidersStatusController extends Controller
{

    use CrudMethods;

    /**
     * @var ProviderStatusService
     */
    protected $service;

    /**
     * @var ProvidersStatusValidator
     */
    protected $validator;

    /**
     * ProvidersStatusController constructor.
     *
     * @param ProviderStatusService $service
     * @param ProvidersStatusValidator $validator
     */
    public function __construct(ProviderStatusService $service, ProvidersStatusValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
