<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\CalendarService;
use App\Validators\CalendarValidator;

/**
 * Class CalendarController.
 *
 * @package namespace App\Http\Controllers;
 */
class CalendarController extends Controller
{
    use CrudMethods;

    /**
     * @var CalendarService
     */
    protected $service;

    /**
     * @var CalendarValidator
     */
    protected $validator;

    /**
     * CalendarController constructor.
     *
     * @param CalendarService $service
     * @param CalendarValidator $validator
     */
    public function __construct(CalendarService $service, CalendarValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
