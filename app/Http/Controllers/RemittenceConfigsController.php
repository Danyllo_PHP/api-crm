<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\RemittenceConfigService;
use App\Validators\RemittenceConfigValidator;

/**
 * Class RemittenceConfigsController.
 *
 * @package namespace App\Http\Controllers;
 */
class RemittenceConfigsController extends Controller
{

    use CrudMethods;

    /**
     * @var RemittenceConfigService
     */
    protected $service;

    /**
     * @var RemittenceConfigValidator
     */
    protected $validator;

    /**
     * RemittenceConfigsController constructor.
     *
     * @param RemittenceConfigService $service
     * @param RemittenceConfigValidator $validator
     */
    public function __construct(RemittenceConfigService $service, RemittenceConfigValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
