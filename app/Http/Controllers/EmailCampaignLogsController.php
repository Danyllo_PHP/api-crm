<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Traits\CrudMethods;
use App\Services\EmailCampaignLogService;
use App\Validators\EmailCampaignLogValidator;

/**
 * Class EmailCampaignLogsController.
 *
 * @package namespace App\Http\Controllers;
 */
class EmailCampaignLogsController extends Controller
{
    use CrudMethods;

    /**
     * @var EmailCampaignLogService
     */
    protected $service;

    /**
     * @var EmailCampaignLogValidator
     */
    protected $validator;

    /**
     * EmailCampaignLogsController constructor.
     *
     * @param EmailCampaignLogService $repository
     * @param EmailCampaignLogValidator $validator
     */
    public function __construct(EmailCampaignLogService $service, EmailCampaignLogValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
