<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\RemittenceReturnService;
use App\Validators\RemittenceReturnValidator;

/**
 * Class RemittenceReturnsController.
 *
 * @package namespace App\Http\Controllers;
 */
class RemittenceReturnsController extends Controller
{

    use CrudMethods;

    /**
     * @var RemittenceReturnService
     */
    protected $service;

    /**
     * @var RemittenceReturnValidator
     */
    protected $validator;

    /**
     * RemittenceReturnsController constructor.
     *
     * @param RemittenceReturnService $service
     * @param RemittenceReturnValidator $validator
     */
    public function __construct(RemittenceReturnService $service, RemittenceReturnValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }

}
