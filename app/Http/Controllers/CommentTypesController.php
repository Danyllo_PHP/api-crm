<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\CommentTypeService;
use App\Validators\CommentTypeValidator;

/**
 * Class CommentTypesController.
 *
 * @package namespace App\Http\Controllers;
 */
class CommentTypesController extends Controller
{
    use CrudMethods;

    /**
     * @var CommentTypeService
     */
    protected $service;

    /**
     * @var CommentTypeValidator
     */
    protected $validator;

    /**
     * CommentTypesController constructor.
     *
     * @param CommentTypeService $service
     * @param CommentTypeValidator $validator
     */
    public function __construct(CommentTypeService $service, CommentTypeValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
