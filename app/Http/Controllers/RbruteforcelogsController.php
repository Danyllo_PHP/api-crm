<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\RbruteforcelogService;
use App\Validators\RbruteforcelogValidator;

/**
 * Class RbruteforcelogsController.
 *
 * @package namespace App\Http\Controllers;
 */
class RbruteforcelogsController extends Controller
{

    use CrudMethods;

    /**
     * @var RbruteforcelogService
     */
    protected $service;

    /**
     * @var RbruteforcelogValidator
     */
    protected $validator;

    /**
     * RbruteforcelogsController constructor.
     *
     * @param RbruteforcelogService $service
     * @param RbruteforcelogValidator $validator
     */
    public function __construct(RbruteforcelogService $service, RbruteforcelogValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
