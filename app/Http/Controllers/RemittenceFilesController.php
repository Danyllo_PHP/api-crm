<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\RemittenceFileService;
use App\Validators\RemittenceFileValidator;

/**
 * Class RemittenceFilesController.
 *
 * @package namespace App\Http\Controllers;
 */
class RemittenceFilesController extends Controller
{

    use CrudMethods;

    /**
     * @var RemittenceFileService
     */
    protected $service;

    /**
     * @var RemittenceFileValidator
     */
    protected $validator;

    /**
     * RemittenceFilesController constructor.
     *
     * @param RemittenceFileService $service
     * @param RemittenceFileValidator $validator
     */
    public function __construct(RemittenceFileService $service, RemittenceFileValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
