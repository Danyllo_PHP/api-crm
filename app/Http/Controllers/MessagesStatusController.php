<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\MessagesStatusService;
use App\Validators\MessagesStatusValidator;

/**
 * Class MessagesStatusesController.
 *
 * @package namespace App\Http\Controllers;
 */
class MessagesStatusController extends Controller
{
    use CrudMethods;

    /**
     * @var MessagesStatusService
     */
    protected $service;

    /**
     * @var MessagesStatusValidator
     */
    protected $validator;

    /**
     * MessagesStatusesController constructor.
     *
     * @param MessagesStatusService $service
     * @param MessagesStatusValidator $validator
     */
    public function __construct(MessagesStatusService $service, MessagesStatusValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
