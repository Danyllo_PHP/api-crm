<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\EmailEmailService;
use App\Validators\EmailEmailsValidator;

/**
 * Class EmailEmailsController.
 *
 * @package namespace App\Http\Controllers;
 */
class EmailEmailsController extends Controller
{
    use CrudMethods;

    /**
     * @var EmailEmailService
     */
    protected $service;

    /**
     * @var EmailEmailsValidator
     */
    protected $validator;

    /**
     * EmailEmailsController constructor.
     *
     * @param EmailEmailService $service
     * @param EmailEmailsValidator $validator
     */
    public function __construct(EmailEmailService $service, EmailEmailsValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
