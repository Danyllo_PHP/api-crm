<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\SmsService;
use App\Validators\SmsValidator;

/**
 * Class SmsController.
 *
 * @package namespace App\Http\Controllers;
 */
class SmsController extends Controller
{

    use CrudMethods;

    /**
     * @var SmsService
     */
    protected $service;

    /**
     * @var SmsValidator
     */
    protected $validator;

    /**
     * SmsController constructor.
     *
     * @param SmsService $service
     * @param SmsValidator $validator
     */
    public function __construct(SmsService $service, SmsValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
