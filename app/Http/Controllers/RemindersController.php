<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\ReminderService;
use App\Validators\ReminderValidator;

/**
 * Class RemindersController.
 *
 * @package namespace App\Http\Controllers;
 */
class RemindersController extends Controller
{

    use CrudMethods;

    /**
     * @var ReminderService
     */
    protected $service;

    /**
     * @var ReminderValidator
     */
    protected $validator;

    /**
     * RemindersController constructor.
     *
     * @param ReminderService $service
     * @param ReminderValidator $validator
     */
    public function __construct(ReminderService $service, ReminderValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
