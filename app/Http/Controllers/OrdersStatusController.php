<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\OrdersStatusService;
use App\Validators\OrdersStatusValidator;

/**
 * Class OrdersStatusController.
 *
 * @package namespace App\Http\Controllers;
 */
class OrdersStatusController extends Controller
{
    use CrudMethods;

    /**
     * @var OrdersStatusService
     */
    protected $service;

    /**
     * @var OrdersStatusValidator
     */
    protected $validator;

    /**
     * OrdersStatusController constructor.
     *
     * @param OrdersStatusService $service
     * @param OrdersStatusValidator $validator
     */
    public function __construct(OrdersStatusService $service, OrdersStatusValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
