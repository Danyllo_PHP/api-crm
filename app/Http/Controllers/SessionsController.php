<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\SessionService;
use App\Validators\SessionValidator;

/**
 * Class SessionsController.
 *
 * @package namespace App\Http\Controllers;
 */
class SessionsController extends Controller
{

    use CrudMethods;

    /**
     * @var SessionService
     */
    protected $service;

    /**
     * @var SessionValidator
     */
    protected $validator;

    /**
     * SessionsController constructor.
     *
     * @param SessionService $service
     * @param SessionValidator $validator
     */
    public function __construct(SessionService $service, SessionValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
