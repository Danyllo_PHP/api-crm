<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\EmailEmailNotificationService;
use App\Validators\EmailEmailNotificationValidator;

/**
 * Class EmailEmailNotificationsController.
 *
 * @package namespace App\Http\Controllers;
 */
class EmailEmailNotificationsController extends Controller
{
    use CrudMethods;

    /**
     * @var EmailEmailNotificationService
     */
    protected $service;

    /**
     * @var EmailEmailNotificationValidator
     */
    protected $validator;

    /**
     * EmailEmailNotificationsController constructor.
     *
     * @param EmailEmailNotificationService $service
     * @param EmailEmailNotificationValidator $validator
     */
    public function __construct(EmailEmailNotificationService $service, EmailEmailNotificationValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
