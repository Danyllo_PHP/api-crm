<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;

use App\Repositories\AdwordsCampaignRepository;
use App\Services\AdwordsCampaignService;
use App\Validators\AdwordsCampaignValidator;

/**
 * Class AdwordsCampaignsController.
 *
 * @package namespace App\Http\Controllers;
 */
class AdwordsCampaignsController extends Controller
{
    use CrudMethods;

    /**
     * @var AdwordsCampaignValidator
     */
    protected $validator;

    /**
     * @var AdwordsCampaignService
     */

    protected $service;
    /**
     * AdwordsCampaignsController constructor.
     *
     * @param AdwordsCampaignService $service.
     * @param AdwordsCampaignValidator $validator.
     */
    public function __construct(AdwordsCampaignService $service, AdwordsCampaignValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }

}
