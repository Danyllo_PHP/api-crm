<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\QuotationObservationsTypeService;
use App\Validators\QuotationObservationsTypeValidator;

/**
 * Class QuotationObservationsTypesController.
 *
 * @package namespace App\Http\Controllers;
 */
class QuotationObservationsTypesController extends Controller
{
    use CrudMethods;

    /**
     * @var QuotationObservationsTypeService
     */
    protected $service;

    /**
     * @var QuotationObservationsTypeValidator
     */
    protected $validator;

    /**
     * QuotationObservationsTypesController constructor.
     *
     * @param QuotationObservationsTypeService $service
     * @param QuotationObservationsTypeValidator $validator
     */
    public function __construct(QuotationObservationsTypeService $service, QuotationObservationsTypeValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
