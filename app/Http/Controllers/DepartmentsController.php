<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\DepartmentService;
use App\Validators\DepartmentValidator;

/**
 * Class DepartmentsController.
 *
 * @package namespace App\Http\Controllers;
 */
class DepartmentsController extends Controller
{
    use CrudMethods;

    /**
     * @var DepartmentService
     */
    protected $service;

    /**
     * @var DepartmentValidator
     */
    protected $validator;

    /**
     * DepartmentsController constructor.
     *
     * @param DepartmentService $service
     * @param DepartmentValidator $validator
     */
    public function __construct(DepartmentService $service, DepartmentValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
