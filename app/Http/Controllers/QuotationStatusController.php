<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\QuotationStatusService;
use App\Validators\QuotationStatusValidator;

/**
 * Class QuotationStatusController.
 *
 * @package namespace App\Http\Controllers;
 */
class QuotationStatusController extends Controller
{

    use CrudMethods;

    /**
     * @var QuotationStatusService
     */
    protected $service;

    /**
     * @var QuotationStatusValidator
     */
    protected $validator;

    /**
     * QuotationStatusController constructor.
     *
     * @param QuotationStatusService $service
     * @param QuotationStatusValidator $validator
     */
    public function __construct(QuotationStatusService $service, QuotationStatusValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
