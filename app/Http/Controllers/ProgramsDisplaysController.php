<?php

    namespace App\Http\Controllers;

    use App\Http\Controllers\Traits\CrudMethods;
    use App\Services\ProgramsDisplayService;
    use App\Validators\ProgramsDisplayValidator;

    /**
     * Class ProgramsDisplaysController.
     *
     * @package namespace App\Http\Controllers;
     */
    class ProgramsDisplaysController extends Controller
    {
        use CrudMethods;

        /**
         * @var ProgramsDisplayService
         */
        protected $service;

        /**
         * @var ProgramsDisplayValidator
         */
        protected $validator;

        /**
         * ProgramsDisplaysController constructor.
         *
         * @param ProgramsDisplayService   $service
         * @param ProgramsDisplayValidator $validator
         */
        public function __construct(ProgramsDisplayService $service, ProgramsDisplayValidator $validator)
        {
            $this->service = $service;
            $this->validator = $validator;
        }
    }
