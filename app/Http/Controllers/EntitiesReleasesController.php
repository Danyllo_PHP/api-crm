<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\EntitiesReleaseService;
use App\Validators\EntitiesReleaseValidator;

/**
 * Class EntitiesReleasesController.
 *
 * @package namespace App\Http\Controllers;
 */
class EntitiesReleasesController extends Controller
{
    use CrudMethods;
    /**
     * @var EntitiesReleaseService
     */
    protected $service;

    /**
     * @var EntitiesReleaseValidator
     */
    protected $validator;

    /**
     * EntitiesReleasesController constructor.
     *
     * @param EntitiesReleaseService $service
     * @param EntitiesReleaseValidator $validator
     */
    public function __construct(EntitiesReleaseService $service, EntitiesReleaseValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
