<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\NoteService;
use App\Validators\NoteValidator;

/**
 * Class NotesController.
 *
 * @package namespace App\Http\Controllers;
 */
class NotesController extends Controller
{
    use CrudMethods;

    /**
     * @var NoteService
     */
    protected $service;

    /**
     * @var NoteValidator
     */
    protected $validator;

    /**
     * NotesController constructor.
     *
     * @param NoteService $service
     * @param NoteValidator $validator
     */
    public function __construct(NoteService $service, NoteValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
