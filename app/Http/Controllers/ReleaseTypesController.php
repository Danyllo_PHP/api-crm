<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\ReleaseTypeService;
use App\Validators\ReleaseTypeValidator;

/**
 * Class ReleaseTypesController.
 *
 * @package namespace App\Http\Controllers;
 */
class ReleaseTypesController extends Controller
{

    use CrudMethods;

    /**
     * @var ReleaseTypeService
     */
    protected $service;

    /**
     * @var ReleaseTypeValidator
     */
    protected $validator;

    /**
     * ReleaseTypesController constructor.
     *
     * @param ReleaseTypeService $repository
     * @param ReleaseTypeValidator $validator
     */
    public function __construct(ReleaseTypeService $service, ReleaseTypeValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
