<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\AuditService;
use App\Validators\AuditValidator;

/**
 * Class AuditsController.
 *
 * @package namespace App\Http\Controllers;
 */
class AuditsController extends Controller
{
    use CrudMethods;

    /**
     * @var AuditService
     */
    protected $service;

    /**
     * @var AuditValidator
     */
    protected $validator;

    /**
     * AuditsController constructor.
     *
     * @param AuditService $service
     * @param AuditValidator $validator
     */
    public function __construct(AuditService $service, AuditValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }

}
