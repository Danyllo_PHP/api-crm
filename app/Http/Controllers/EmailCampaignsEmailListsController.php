<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\EmailCampaignsEmailListService;
use App\Validators\EmailCampaignsEmailListValidator;

/**
 * Class EmailCampaignsEmailListsController.
 *
 * @package namespace App\Http\Controllers;
 */
class EmailCampaignsEmailListsController extends Controller
{
    use CrudMethods;

    /**
     * @var EmailCampaignsEmailListService
     */
    protected $service;

    /**
     * @var EmailCampaignsEmailListValidator
     */
    protected $validator;

    /**
     * EmailCampaignsEmailListsController constructor.
     *
     * @param EmailCampaignsEmailListService $service
     * @param EmailCampaignsEmailListValidator $validator
     */
    public function __construct(EmailCampaignsEmailListService $service, EmailCampaignsEmailListValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
