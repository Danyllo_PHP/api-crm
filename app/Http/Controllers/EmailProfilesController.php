<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\EmailProfileService;
use App\Validators\EmailProfileValidator;

/**
 * Class EmailProfilesController.
 *
 * @package namespace App\Http\Controllers;
 */
class EmailProfilesController extends Controller
{
    use CrudMethods;

    /**
     * @var EmailProfileService
     */
    protected $service;

    /**
     * @var EmailProfileValidator
     */
    protected $validator;

    /**
     * EmailProfilesController constructor.
     *
     * @param EmailProfileService $service
     * @param EmailProfileValidator $validator
     */
    public function __construct(EmailProfileService $service, EmailProfileValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
