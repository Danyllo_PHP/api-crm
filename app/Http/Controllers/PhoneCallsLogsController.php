<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\PhoneCallsLogService;
use App\Validators\PhoneCallsLogValidator;

/**
 * Class PhoneCallsLogsController.
 *
 * @package namespace App\Http\Controllers;
 */
class PhoneCallsLogsController extends Controller
{
    use CrudMethods;

    /**
     * @var PhoneCallsLogService
     */
    protected $service;

    /**
     * @var PhoneCallsLogValidator
     */
    protected $validator;

    /**
     * PhoneCallsLogsController constructor.
     *
     * @param PhoneCallsLogService $service
     * @param PhoneCallsLogValidator $validator
     */
    public function __construct(PhoneCallsLogService $service, PhoneCallsLogValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
