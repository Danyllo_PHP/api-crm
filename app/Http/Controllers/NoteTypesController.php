<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\NoteTypeService;
use App\Validators\NoteTypeValidator;

/**
 * Class NoteTypesController.
 *
 * @package namespace App\Http\Controllers;
 */
class NoteTypesController extends Controller
{
    use CrudMethods;

    /**
     * @var NoteTypeService
     */
    protected $service;

    /**
     * @var NoteTypeValidator
     */
    protected $validator;

    /**
     * NoteTypesController constructor.
     *
     * @param NoteTypeService $service
     * @param NoteTypeValidator $validator
     */
    public function __construct(NoteTypeService $service, NoteTypeValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
