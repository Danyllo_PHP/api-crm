<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\EmailService;
use App\Validators\EmailValidator;

/**
 * Class EmailsController.
 *
 * @package namespace App\Http\Controllers;
 */
class EmailsController extends Controller
{

    use CrudMethods;

    /**
     * @var EmailService
     */
    protected $service;

    /**
     * @var EmailValidator
     */
    protected $validator;

    /**
     * EmailsController constructor.
     *
     * @param EmailService $service
     * @param EmailValidator $validator
     */
    public function __construct(EmailService $service, EmailValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
