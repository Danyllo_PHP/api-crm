<?php

    namespace App\Http\Controllers;

    use App\Http\Controllers\Traits\CrudMethods;
    use App\Services\ProgramsQuotationService;
    use App\Validators\ProgramsQuotationValidator;

    /**
     * Class ProgramsQuotationsController.
     *
     * @package namespace App\Http\Controllers;
     */
    class ProgramsQuotationsController extends Controller
    {
        use CrudMethods;

        /**
         * @var ProgramsQuotationService
         */
        protected $service;

        /**
         * @var ProgramsQuotationValidator
         */
        protected $validator;

        /**
         * ProgramsQuotationsController constructor.
         *
         * @param ProgramsQuotationService   $service
         * @param ProgramsQuotationValidator $validator
         */
        public function __construct(ProgramsQuotationService $service, ProgramsQuotationValidator $validator)
        {
            $this->service = $service;
            $this->validator = $validator;
        }
    }
