<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\EmailServiceService;
use App\Validators\EmailServiceValidator;

/**
 * Class EmailServicesController.
 *
 * @package namespace App\Http\Controllers;
 */
class EmailServicesController extends Controller
{
    use CrudMethods;

    /**
     * @var EmailServiceService
     */
    protected $service;

    /**
     * @var EmailServiceValidator
     */
    protected $validator;

    /**
     * EmailServicesController constructor.
     *
     * @param EmailServiceService $service
     * @param EmailServiceValidator $validator
     */
    public function __construct(EmailServiceService $service, EmailServiceValidator $validator)
    {
        $this->service = $service;
        $this->validator = $validator;
    }
}
