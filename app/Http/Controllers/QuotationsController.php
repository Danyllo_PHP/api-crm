<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\QuotationService;
use App\Validators\QuotationValidator;

/**
 * Class QuotationsController.
 *
 * @package namespace App\Http\Controllers;
 */
class QuotationsController extends Controller
{

    use CrudMethods;

    /**
     * @var QuotationService
     */
    protected $service;

    /**
     * @var QuotationValidator
     */
    protected $validator;

    /**
     * QuotationsController constructor.
     *
     * @param QuotationService $service
     * @param QuotationValidator $validator
     */
    public function __construct(QuotationService $service, QuotationValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
