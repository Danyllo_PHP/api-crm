<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\MailBoxService;
use App\Validators\MailBoxValidator;

/**
 * Class MailBoxesController.
 *
 * @package namespace App\Http\Controllers;
 */
class MailBoxesController extends Controller
{
    use CrudMethods;

    /**
     * @var MailBoxService
     */
    protected $service;

    /**
     * @var MailBoxValidator
     */
    protected $validator;

    /**
     * MailBoxesController constructor.
     *
     * @param MailBoxService $repository
     * @param MailBoxValidator $validator
     */
    public function __construct(MailBoxService $service, MailBoxValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
