<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\BlacklistService;
use App\Validators\BlacklistValidator;

/**
 * Class BlacklistsController.
 *
 * @package namespace App\Http\Controllers;
 */
class BlacklistsController extends Controller
{

    use CrudMethods;

    /**
     * @var BlacklistService
     */
    protected $service;

    /**
     * @var BlacklistValidator
     */
    protected $validator;

    /**
     * BlacklistsController constructor.
     *
     * @param BlacklistService $service
     * @param BlacklistValidator $validator
     */
    public function __construct(BlacklistService $service, BlacklistValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
