<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\StockAuditService;
use App\Validators\StockAuditValidator;

/**
 * Class StockAuditsController.
 *
 * @package namespace App\Http\Controllers;
 */
class StockAuditsController extends Controller
{

    use CrudMethods;

    /**
     * @var StockAuditService
     */
    protected $service;

    /**
     * @var StockAuditValidator
     */
    protected $validator;

    /**
     * StockAuditsController constructor.
     *
     * @param StockAuditService $service
     * @param StockAuditValidator $validator
     */
    public function __construct(StockAuditService $service, StockAuditValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
