<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Repositories\QuotationFileRepository;
use App\Services\QuotationFileService;
use App\Validators\QuotationFileValidator;

/**
 * Class QuotationFilesController.
 *
 * @package namespace App\Http\Controllers;
 */
class QuotationFilesController extends Controller
{
    use CrudMethods;

    /**
     * @var QuotationFileService
     */
    protected $service;

    /**
     * @var QuotationFileValidator
     */
    protected $validator;

    /**
     * QuotationFilesController constructor.
     *
     * @param QuotationFileService $service
     * @param QuotationFileValidator $validator
     */
    public function __construct(QuotationFileService $service, QuotationFileValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
