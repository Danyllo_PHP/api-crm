<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\InvoiceService;
use App\Validators\InvoiceValidator;

/**
 * Class InvoicesController.
 *
 * @package namespace App\Http\Controllers;
 */
class InvoicesController extends Controller
{
    use CrudMethods;

    /**
     * @var InvoiceService
     */
    protected $service;

    /**
     * @var InvoiceValidator
     */
    protected $validator;

    /**
     * InvoicesController constructor.
     *
     * @param InvoiceService $service
     * @param InvoiceValidator $validator
     */
    public function __construct(InvoiceService $service, InvoiceValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
