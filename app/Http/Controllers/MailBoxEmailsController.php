<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\MailBoxEmailService;
use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\MailBoxEmailCreateRequest;
use App\Http\Requests\MailBoxEmailUpdateRequest;
use App\Repositories\MailBoxEmailRepository;
use App\Validators\MailBoxEmailValidator;

/**
 * Class MailBoxEmailsController.
 *
 * @package namespace App\Http\Controllers;
 */
class MailBoxEmailsController extends Controller
{
    use CrudMethods;

    /**
     * @var MailBoxEmailService
     */
    protected $service;

    /**
     * @var MailBoxEmailValidator
     */
    protected $validator;

    /**
     * MailBoxEmailsController constructor.
     *
    $service     * @param MailBoxEmailValidator $validator
     */
    public function __construct(MailBoxEmailService $service, MailBoxEmailValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
