<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\PhoneService;
use App\Validators\PhoneValidator;

/**
 * Class PhonesController.
 *
 * @package namespace App\Http\Controllers;
 */
class PhonesController extends Controller
{
    use CrudMethods;

    /**
     * @var PhoneService
     */
    protected $service;

    /**
     * @var PhoneValidator
     */
    protected $validator;

    /**
     * PhonesController constructor.
     *
     * @param PhoneService $service
     * @param PhoneValidator $validator
     */
    public function __construct(PhoneService $service, PhoneValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
