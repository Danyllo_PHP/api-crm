<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\EmailTemplateService;
use App\Validators\EmailTemplateValidator;

/**
 * Class EmailTemplatesController.
 *
 * @package namespace App\Http\Controllers;
 */
class EmailTemplatesController extends Controller
{
    use CrudMethods;

    /**
     * @var EmailTemplateService
     */
    protected $service;

    /**
     * @var EmailTemplateValidator
     */
    protected $validator;

    /**
     * EmailTemplatesController constructor.
     *
     * @param EmailTemplateService $service
     * @param EmailTemplateValidator $validator
     */
    public function __construct(EmailTemplateService $service, EmailTemplateValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
