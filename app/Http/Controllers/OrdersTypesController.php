<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\OrdersTypeService;
use App\Validators\OrdersTypeValidator;

/**
 * Class OrdersTypesController.
 *
 * @package namespace App\Http\Controllers;
 */
class OrdersTypesController extends Controller
{
    use CrudMethods;

    /**
     * @var OrdersTypeService
     */
    protected $service;

    /**
     * @var OrdersTypeValidator
     */
    protected $validator;

    /**
     * OrdersTypesController constructor.
     *
     * @param OrdersTypeService $service
     * @param OrdersTypeValidator $validator
     */
    public function __construct(OrdersTypeService $service, OrdersTypeValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
