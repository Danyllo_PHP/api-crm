<?php

    namespace App\Http\Controllers;

    use App\Http\Controllers\Traits\CrudMethods;
    use App\Services\ProvidersStatusPaymentFormService;
    use App\Validators\ProvidersStatusPaymentFormValidator;

    /**
     * Class ProvidersStatusPaymentFormsController.
     *
     * @package namespace App\Http\Controllers;
     */
    class ProvidersStatusPaymentFormsController extends Controller
    {

        use CrudMethods;

        /**
         * @var ProvidersStatusPaymentFormService
         */
        protected $service;

        /**
         * @var ProvidersStatusPaymentFormValidator
         */
        protected $validator;

        /**
         * ProvidersStatusPaymentFormsController constructor.
         *
         * @param ProvidersStatusPaymentFormService   $service
         * @param ProvidersStatusPaymentFormValidator $validator
         */
        public function __construct(ProvidersStatusPaymentFormService $service, ProvidersStatusPaymentFormValidator $validator)
        {
            $this->service = $service;
            $this->validator = $validator;
        }
    }
