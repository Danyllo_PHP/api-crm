<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\OrdersRemittenceFileService;
use App\Validators\OrdersRemittenceFileValidator;

/**
 * Class OrdersRemittenceFilesController.
 *
 * @package namespace App\Http\Controllers;
 */
class OrdersRemittenceFilesController extends Controller
{
    use CrudMethods;

    /**
     * @var OrdersRemittenceFileService
     */
    protected $service;

    /**
     * @var OrdersRemittenceFileValidator
     */
    protected $validator;

    /**
     * OrdersRemittenceFilesController constructor.
     *
     * @param OrdersRemittenceFileService $service
     * @param OrdersRemittenceFileValidator $validator
     */
    public function __construct(OrdersRemittenceFileService $service, OrdersRemittenceFileValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
