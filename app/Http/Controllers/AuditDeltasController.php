<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\AuditDeltaService;
use App\Validators\AuditDeltaValidator;

/**
 * Class AuditDeltasController.
 *
 * @package namespace App\Http\Controllers;
 */
class AuditDeltasController extends Controller
{
    use CrudMethods;

    /**
     * @var AuditDeltaService
     */
    protected $repository;

    /**
     * @var AuditDeltaValidator
     */
    protected $validator;

    /**
     * AuditDeltasController constructor.
     *
     * @param AuditDeltaService $service
     * @param AuditDeltaValidator $validator
     */
    public function __construct(AuditDeltaService $service, AuditDeltaValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
