<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\RemittenceReturnOrderService;
use App\Validators\RemittenceReturnOrderValidator;

/**
 * Class RemittenceReturnOrdersController.
 *
 * @package namespace App\Http\Controllers;
 */
class RemittenceReturnOrdersController extends Controller
{

    use CrudMethods;

    /**
     * @var RemittenceReturnOrderService
     */
    protected $service;

    /**
     * @var RemittenceReturnOrderValidator
     */
    protected $validator;

    /**
     * RemittenceReturnOrdersController constructor.
     *
     * @param RemittenceReturnOrderService $service
     * @param RemittenceReturnOrderValidator $validator
     */
    public function __construct(RemittenceReturnOrderService $service, RemittenceReturnOrderValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
