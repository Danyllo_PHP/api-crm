<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\AppController;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;

/**
 * Class ForgotPasswordController
 * @package App\Http\Controllers\Auth
 */
class ForgotPasswordController extends AppController
{

    use SendsPasswordResetEmails;

    /**
     * ForgotPasswordController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get the response for a successful password reset link.
     *
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkResponse($response)
    {
        return response()->json([
            'error'   => false,
            'message' => trans($response, [], 'pt-br')
        ], 200);
    }

    /**
     * @param $response
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkFailedResponse( $response)
    {
        return response()->json([
            'error'   => true,
            'message' => trans($response, [], 'pt-br')
        ], 200);
    }
}
