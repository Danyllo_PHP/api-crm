<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\FidelityExtractService;
use App\Validators\FidelityExtractValidator;

/**
 * Class FidelityExtractsController.
 *
 * @package namespace App\Http\Controllers;
 */
class FidelityExtractsController extends Controller
{
    use CrudMethods;

    /**
     * @var FidelityExtractService
     */
    protected $service;

    /**
     * @var FidelityExtractValidator
     */
    protected $validator;

    /**
     * FidelityExtractsController constructor.
     *
     * @param FidelityExtractService $service
     * @param FidelityExtractValidator $validator
     */
    public function __construct(FidelityExtractService $service, FidelityExtractValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
