<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\PaymentService;
use App\Validators\PaymentValidator;

/**
 * Class PaymentsController.
 *
 * @package namespace App\Http\Controllers;
 */
class PaymentsController extends Controller
{
    use CrudMethods;
    /**
     * @var PaymentService
     */
    protected $service;

    /**
     * @var PaymentValidator
     */
    protected $validator;

    /**
     * PaymentsController constructor.
     *
     * @param PaymentService $service
     * @param PaymentValidator $validator
     */
    public function __construct(PaymentService $service, PaymentValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
