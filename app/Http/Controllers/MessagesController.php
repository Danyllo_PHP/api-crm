<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\MessageService;
use App\Validators\MessageValidator;

/**
 * Class MessagesController.
 *
 * @package namespace App\Http\Controllers;
 */
class MessagesController extends Controller
{
    use CrudMethods;

    /**
     * @var MessageService
     */
    protected $service;

    /**
     * @var MessageValidator
     */
    protected $validator;

    /**
     * MessagesController constructor.
     *
     * @param MessageService $service
     * @param MessageValidator $validator
     */
    public function __construct(MessageService $service, MessageValidator $validator)
    {
        $this->service = $service;
    }
}
