<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CrudMethods;
use App\Services\OrdersProgramService;
use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\OrdersProgramCreateRequest;
use App\Http\Requests\OrdersProgramUpdateRequest;
use App\Repositories\OrdersProgramRepository;
use App\Validators\OrdersProgramValidator;

/**
 * Class OrdersProgramsController.
 *
 * @package namespace App\Http\Controllers;
 */
class OrdersProgramsController extends Controller
{
    use CrudMethods;

    /**
     * @var OrdersProgramService
     */
    protected $service;

    /**
     * @var OrdersProgramValidator
     */
    protected $validator;

    /**
     * OrdersProgramsController constructor.
     *
     * @param OrdersProgramService $service
     * @param OrdersProgramValidator $validator
     */
    public function __construct(OrdersProgramService $service, OrdersProgramValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
}
