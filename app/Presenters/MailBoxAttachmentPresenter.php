<?php

namespace App\Presenters;

use App\Transformers\MailBoxAttachmentTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class MailBoxAttachmentPresenter.
 *
 * @package namespace App\Presenters;
 */
class MailBoxAttachmentPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new MailBoxAttachmentTransformer();
    }
}
