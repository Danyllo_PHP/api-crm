<?php

namespace App\Presenters;

use App\Transformers\RemittenceReturnOrderTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class RemittenceReturnOrderPresenter.
 *
 * @package namespace App\Presenters;
 */
class RemittenceReturnOrderPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new RemittenceReturnOrderTransformer();
    }
}
