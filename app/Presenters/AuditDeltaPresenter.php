<?php

namespace App\Presenters;

use App\Transformers\AuditDeltaTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AuditDeltaPresenter.
 *
 * @package namespace App\Presenters;
 */
class AuditDeltaPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AuditDeltaTransformer();
    }
}
