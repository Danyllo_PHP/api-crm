<?php

namespace App\Presenters;

use App\Transformers\QuotationFileTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class QuotationFilePresenter.
 *
 * @package namespace App\Presenters;
 */
class QuotationFilePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new QuotationFileTransformer();
    }
}
