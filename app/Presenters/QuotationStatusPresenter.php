<?php

namespace App\Presenters;

use App\Transformers\QuotationStatusTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class QuotationStatusPresenter.
 *
 * @package namespace App\Presenters;
 */
class QuotationStatusPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new QuotationStatusTransformer();
    }
}
