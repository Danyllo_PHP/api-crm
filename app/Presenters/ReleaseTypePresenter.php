<?php

namespace App\Presenters;

use App\Transformers\ReleaseTypeTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ReleaseTypePresenter.
 *
 * @package namespace App\Presenters;
 */
class ReleaseTypePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ReleaseTypeTransformer();
    }
}
