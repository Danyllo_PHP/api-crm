<?php

namespace App\Presenters;

use App\Transformers\ProvidersOccupationTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ProvidersOccupationPresenter.
 *
 * @package namespace App\Presenters;
 */
class ProvidersOccupationPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ProvidersOccupationTransformer();
    }
}
