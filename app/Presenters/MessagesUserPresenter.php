<?php

namespace App\Presenters;

use App\Transformers\MessagesUserTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class MessagesUserPresenter.
 *
 * @package namespace App\Presenters;
 */
class MessagesUserPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new MessagesUserTransformer();
    }
}
