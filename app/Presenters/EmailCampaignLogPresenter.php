<?php

namespace App\Presenters;

use App\Transformers\EmailCampaignLogTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class EmailCampaignLogPresenter.
 *
 * @package namespace App\Presenters;
 */
class EmailCampaignLogPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new EmailCampaignLogTransformer();
    }
}
