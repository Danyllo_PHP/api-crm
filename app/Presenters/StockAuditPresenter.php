<?php

namespace App\Presenters;

use App\Transformers\StockAuditTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class StockAuditPresenter.
 *
 * @package namespace App\Presenters;
 */
class StockAuditPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new StockAuditTransformer();
    }
}
