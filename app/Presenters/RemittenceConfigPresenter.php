<?php

namespace App\Presenters;

use App\Transformers\RemittenceConfigTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class RemittenceConfigPresenter.
 *
 * @package namespace App\Presenters;
 */
class RemittenceConfigPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new RemittenceConfigTransformer();
    }
}
