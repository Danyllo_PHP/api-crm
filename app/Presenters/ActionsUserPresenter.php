<?php

namespace App\Presenters;

use App\Transformers\ActionsUserTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ActionsUserPresenter.
 *
 * @package namespace App\Presenters;
 */
class ActionsUserPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ActionsUserTransformer();
    }
}
