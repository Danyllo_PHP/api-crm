<?php

namespace App\Presenters;

use App\Transformers\PaymentsProgramTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class PaymentsProgramPresenter.
 *
 * @package namespace App\Presenters;
 */
class PaymentsProgramPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PaymentsProgramTransformer();
    }
}
