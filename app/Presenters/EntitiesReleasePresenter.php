<?php

namespace App\Presenters;

use App\Transformers\EntitiesReleaseTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class EntitiesReleasePresenter.
 *
 * @package namespace App\Presenters;
 */
class EntitiesReleasePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new EntitiesReleaseTransformer();
    }
}
