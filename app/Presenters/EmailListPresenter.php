<?php

namespace App\Presenters;

use App\Transformers\EmailListTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class EmailListPresenter.
 *
 * @package namespace App\Presenters;
 */
class EmailListPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new EmailListTransformer();
    }
}
