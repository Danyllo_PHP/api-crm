<?php

namespace App\Presenters;

use App\Transformers\MessagesReplyTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class MessagesReplyPresenter.
 *
 * @package namespace App\Presenters;
 */
class MessagesReplyPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new MessagesReplyTransformer();
    }
}
