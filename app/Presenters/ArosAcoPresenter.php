<?php

namespace App\Presenters;

use App\Transformers\ArosAcoTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ArosAcoPresenter.
 *
 * @package namespace App\Presenters;
 */
class ArosAcoPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ArosAcoTransformer();
    }
}
