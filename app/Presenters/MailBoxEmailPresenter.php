<?php

namespace App\Presenters;

use App\Transformers\MailBoxEmailTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class MailBoxEmailPresenter.
 *
 * @package namespace App\Presenters;
 */
class MailBoxEmailPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new MailBoxEmailTransformer();
    }
}
