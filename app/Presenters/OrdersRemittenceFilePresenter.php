<?php

namespace App\Presenters;

use App\Transformers\OrdersRemittenceFileTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class OrdersRemittenceFilePresenter.
 *
 * @package namespace App\Presenters;
 */
class OrdersRemittenceFilePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new OrdersRemittenceFileTransformer();
    }
}
