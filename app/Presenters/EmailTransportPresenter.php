<?php

namespace App\Presenters;

use App\Transformers\EmailTransportTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class EmailTransportPresenter.
 *
 * @package namespace App\Presenters;
 */
class EmailTransportPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new EmailTransportTransformer();
    }
}
