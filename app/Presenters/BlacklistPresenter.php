<?php

namespace App\Presenters;

use App\Transformers\BlacklistTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class BlacklistPresenter.
 *
 * @package namespace App\Presenters;
 */
class BlacklistPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new BlacklistTransformer();
    }
}
