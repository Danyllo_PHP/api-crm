<?php

namespace App\Presenters;

use App\Transformers\EmailListEmailEmailTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class EmailListEmailEmailPresenter.
 *
 * @package namespace App\Presenters;
 */
class EmailListEmailEmailPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new EmailListEmailEmailTransformer();
    }
}
