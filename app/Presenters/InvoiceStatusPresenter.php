<?php

namespace App\Presenters;

use App\Transformers\InvoiceStatusTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class InvoiceStatusPresenter.
 *
 * @package namespace App\Presenters;
 */
class InvoiceStatusPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new InvoiceStatusTransformer();
    }
}
