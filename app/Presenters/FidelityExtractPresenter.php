<?php

namespace App\Presenters;

use App\Transformers\FidelityExtractTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class FidelityExtractPresenter.
 *
 * @package namespace App\Presenters;
 */
class FidelityExtractPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new FidelityExtractTransformer();
    }
}
