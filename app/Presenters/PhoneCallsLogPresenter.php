<?php

namespace App\Presenters;

use App\Transformers\PhoneCallsLogTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class PhoneCallsLogPresenter.
 *
 * @package namespace App\Presenters;
 */
class PhoneCallsLogPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PhoneCallsLogTransformer();
    }
}
