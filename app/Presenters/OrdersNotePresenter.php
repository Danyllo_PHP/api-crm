<?php

namespace App\Presenters;

use App\Transformers\OrdersNoteTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class OrdersNotePresenter.
 *
 * @package namespace App\Presenters;
 */
class OrdersNotePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new OrdersNoteTransformer();
    }
}
