<?php

namespace App\Presenters;

use App\Transformers\RemittenceBankTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class RemittenceBankPresenter.
 *
 * @package namespace App\Presenters;
 */
class RemittenceBankPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new RemittenceBankTransformer();
    }
}
