<?php

namespace App\Presenters;

use App\Transformers\ProgramsDisplayTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ProgramsDisplayPresenter.
 *
 * @package namespace App\Presenters;
 */
class ProgramsDisplayPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ProgramsDisplayTransformer();
    }
}
