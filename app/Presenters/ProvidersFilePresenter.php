<?php

namespace App\Presenters;

use App\Transformers\ProvidersFileTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ProvidersFilePresenter.
 *
 * @package namespace App\Presenters;
 */
class ProvidersFilePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ProvidersFileTransformer();
    }
}
