<?php

namespace App\Presenters;

use App\Transformers\ProvidersStatusPaymentFormTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ProvidersStatusPaymentFormPresenter.
 *
 * @package namespace App\Presenters;
 */
class ProvidersStatusPaymentFormPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ProvidersStatusPaymentFormTransformer();
    }
}
