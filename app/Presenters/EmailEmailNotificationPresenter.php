<?php

namespace App\Presenters;

use App\Transformers\EmailEmailNotificationTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class EmailEmailNotificationPresenter.
 *
 * @package namespace App\Presenters;
 */
class EmailEmailNotificationPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new EmailEmailNotificationTransformer();
    }
}
