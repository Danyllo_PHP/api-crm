<?php

namespace App\Presenters;

use App\Transformers\ProvidersStatusTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ProvidersStatusPresenter.
 *
 * @package namespace App\Presenters;
 */
class ProvidersStatusPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ProvidersStatusTransformer();
    }
}
