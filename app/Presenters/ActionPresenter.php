<?php

namespace App\Presenters;

use App\Transformers\ActionTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ActionPresenter.
 *
 * @package namespace App\Presenters;
 */
class ActionPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ActionTransformer();
    }
}
