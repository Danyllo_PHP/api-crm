<?php

namespace App\Presenters;

use App\Transformers\AroTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AroPresenter.
 *
 * @package namespace App\Presenters;
 */
class AroPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AroTransformer();
    }
}
