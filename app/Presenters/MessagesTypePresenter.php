<?php

namespace App\Presenters;

use App\Transformers\MessagesTypeTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class MessagesTypePresenter.
 *
 * @package namespace App\Presenters;
 */
class MessagesTypePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new MessagesTypeTransformer();
    }
}
