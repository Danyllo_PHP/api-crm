<?php

namespace App\Presenters;

use App\Transformers\NoteTypeTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class NoteTypePresenter.
 *
 * @package namespace App\Presenters;
 */
class NoteTypePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new NoteTypeTransformer();
    }
}
