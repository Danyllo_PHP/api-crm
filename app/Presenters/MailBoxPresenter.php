<?php

namespace App\Presenters;

use App\Transformers\MailBoxTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class MailBoxPresenter.
 *
 * @package namespace App\Presenters;
 */
class MailBoxPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new MailBoxTransformer();
    }
}
