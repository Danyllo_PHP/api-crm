<?php

namespace App\Presenters;

use App\Transformers\EmailServiceTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class EmailServicePresenter.
 *
 * @package namespace App\Presenters;
 */
class EmailServicePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new EmailServiceTransformer();
    }
}
