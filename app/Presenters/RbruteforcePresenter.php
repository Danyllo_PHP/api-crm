<?php

namespace App\Presenters;

use App\Transformers\RbruteforceTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class RbruteforcePresenter.
 *
 * @package namespace App\Presenters;
 */
class RbruteforcePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new RbruteforceTransformer();
    }
}
