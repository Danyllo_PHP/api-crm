<?php

namespace App\Presenters;

use App\Transformers\PhoneCallTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class PhoneCallPresenter.
 *
 * @package namespace App\Presenters;
 */
class PhoneCallPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PhoneCallTransformer();
    }
}
