<?php

namespace App\Presenters;

use App\Transformers\MessagesStatusTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class MessagesStatusPresenter.
 *
 * @package namespace App\Presenters;
 */
class MessagesStatusPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new MessagesStatusTransformer();
    }
}
