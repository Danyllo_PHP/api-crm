<?php

namespace App\Presenters;

use App\Transformers\EmailCampaignTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class EmailCampaignPresenter.
 *
 * @package namespace App\Presenters;
 */
class EmailCampaignPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new EmailCampaignTransformer();
    }
}
