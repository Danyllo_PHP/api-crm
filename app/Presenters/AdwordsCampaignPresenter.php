<?php

namespace App\Presenters;

use App\Transformers\AdwordsCampaignTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AdwordsCampaignPresenter.
 *
 * @package namespace App\Presenters;
 */
class AdwordsCampaignPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AdwordsCampaignTransformer();
    }
}
