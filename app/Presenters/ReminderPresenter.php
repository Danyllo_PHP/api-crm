<?php

namespace App\Presenters;

use App\Transformers\ReminderTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ReminderPresenter.
 *
 * @package namespace App\Presenters;
 */
class ReminderPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ReminderTransformer();
    }
}
