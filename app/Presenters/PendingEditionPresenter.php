<?php

namespace App\Presenters;

use App\Transformers\PendingEditionTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class PendingEditionPresenter.
 *
 * @package namespace App\Presenters;
 */
class PendingEditionPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PendingEditionTransformer();
    }
}
