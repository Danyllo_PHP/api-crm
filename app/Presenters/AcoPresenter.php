<?php

namespace App\Presenters;

use App\Transformers\AcoTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AcoPresenter.
 *
 * @package namespace App\Presenters;
 */
class AcoPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AcoTransformer();
    }
}
