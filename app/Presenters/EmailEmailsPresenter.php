<?php

namespace App\Presenters;

use App\Transformers\EmailEmailsTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class EmailEmailsPresenter.
 *
 * @package namespace App\Presenters;
 */
class EmailEmailsPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new EmailEmailsTransformer();
    }
}
