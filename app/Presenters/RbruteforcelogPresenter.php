<?php

namespace App\Presenters;

use App\Transformers\RbruteforcelogTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class RbruteforcelogPresenter.
 *
 * @package namespace App\Presenters;
 */
class RbruteforcelogPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new RbruteforcelogTransformer();
    }
}
