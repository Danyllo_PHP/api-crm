<?php

namespace App\Presenters;

use App\Transformers\TaskTypeTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class TaskTypePresenter.
 *
 * @package namespace App\Presenters;
 */
class TaskTypePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new TaskTypeTransformer();
    }
}
