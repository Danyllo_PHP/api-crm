<?php

namespace App\Presenters;

use App\Transformers\StockLogTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class StockLogPresenter.
 *
 * @package namespace App\Presenters;
 */
class StockLogPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new StockLogTransformer();
    }
}
