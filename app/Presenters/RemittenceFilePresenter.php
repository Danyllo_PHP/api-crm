<?php

namespace App\Presenters;

use App\Transformers\RemittenceFileTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class RemittenceFilePresenter.
 *
 * @package namespace App\Presenters;
 */
class RemittenceFilePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new RemittenceFileTransformer();
    }
}
