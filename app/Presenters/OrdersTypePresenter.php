<?php

namespace App\Presenters;

use App\Transformers\OrdersTypeTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class OrdersTypePresenter.
 *
 * @package namespace App\Presenters;
 */
class OrdersTypePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new OrdersTypeTransformer();
    }
}
