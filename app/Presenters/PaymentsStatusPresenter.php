<?php

namespace App\Presenters;

use App\Transformers\PaymentsStatusTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class PaymentsStatusPresenter.
 *
 * @package namespace App\Presenters;
 */
class PaymentsStatusPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PaymentsStatusTransformer();
    }
}
