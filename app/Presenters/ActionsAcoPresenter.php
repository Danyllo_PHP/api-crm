<?php

namespace App\Presenters;

use App\Transformers\ActionsAcoTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ActionsAcoPresenter.
 *
 * @package namespace App\Presenters;
 */
class ActionsAcoPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ActionsAcoTransformer();
    }
}
