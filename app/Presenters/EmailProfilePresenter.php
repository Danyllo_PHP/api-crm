<?php

namespace App\Presenters;

use App\Transformers\EmailProfileTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class EmailProfilePresenter.
 *
 * @package namespace App\Presenters;
 */
class EmailProfilePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new EmailProfileTransformer();
    }
}
