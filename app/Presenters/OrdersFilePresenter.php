<?php

namespace App\Presenters;

use App\Transformers\OrdersFileTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class OrdersFilePresenter.
 *
 * @package namespace App\Presenters;
 */
class OrdersFilePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new OrdersFileTransformer();
    }
}
