<?php

namespace App\Presenters;

use App\Transformers\SessionTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class SessionPresenter.
 *
 * @package namespace App\Presenters;
 */
class SessionPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new SessionTransformer();
    }
}
