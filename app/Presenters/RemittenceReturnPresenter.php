<?php

namespace App\Presenters;

use App\Transformers\RemittenceReturnTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class RemittenceReturnPresenter.
 *
 * @package namespace App\Presenters;
 */
class RemittenceReturnPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new RemittenceReturnTransformer();
    }
}
