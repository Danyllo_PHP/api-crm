<?php

namespace App\Presenters;

use App\Transformers\CommentTypeTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class CommentTypePresenter.
 *
 * @package namespace App\Presenters;
 */
class CommentTypePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new CommentTypeTransformer();
    }
}
