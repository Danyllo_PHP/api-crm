<?php

namespace App\Presenters;

use App\Transformers\ProgramsIntervalTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ProgramsIntervalPresenter.
 *
 * @package namespace App\Presenters;
 */
class ProgramsIntervalPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ProgramsIntervalTransformer();
    }
}
