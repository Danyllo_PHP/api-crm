<?php

namespace App\Presenters;

use App\Transformers\QuotationObservationsTypeTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class QuotationObservationsTypePresenter.
 *
 * @package namespace App\Presenters;
 */
class QuotationObservationsTypePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new QuotationObservationsTypeTransformer();
    }
}
