<?php

namespace App\Presenters;

use App\Transformers\OrdersStatusProvidersStatus1Transformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class OrdersStatusProvidersStatus1Presenter.
 *
 * @package namespace App\Presenters;
 */
class OrdersStatusProvidersStatus1Presenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new OrdersStatusProvidersStatus1Transformer();
    }
}
