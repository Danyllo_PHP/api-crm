<?php

namespace App\Presenters;

use App\Transformers\EmailCampaignsEmailListTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class EmailCampaignsEmailListPresenter.
 *
 * @package namespace App\Presenters;
 */
class EmailCampaignsEmailListPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new EmailCampaignsEmailListTransformer();
    }
}
