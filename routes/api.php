<?php

Route::middleware(['auth:api'])->group(function(){
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@postReset');
    Route::post('users/create-person','UsersController@store');
    Route::get('providers/report-status','ProvidersController@reportProvidersStatus');
});
