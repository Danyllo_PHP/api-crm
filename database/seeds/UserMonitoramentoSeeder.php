<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserMonitoramentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Monitoramento',
            'email' => 'monitoramento@mangue3.com',
            'username' => 'monitoramento@mangue3.com',
            'password' => bcrypt('monitor_mangue321'),
            'group_id' => 1,
            'active' => 1,
            'activity_count' => 0,
            'quotation_count' => 0,
            'comment_count' => 0,
            'task_count' => 0,
            'group_manager' => 1,
            'created' => (new DateTime('now')),
            'modified' => (new DateTime('now')),
        ]);
    }
}
